package com.ai.common.webApi.openCnofig;

import com.ai.entity.SysOpenConfigModel;

public interface OpenConfigWebApi {

    /**
     * 通过key获取单个数据
     *
     * @param key
     * @return
     */
    SysOpenConfigModel queryOpenConfigByUniqueKey(String key);


}
