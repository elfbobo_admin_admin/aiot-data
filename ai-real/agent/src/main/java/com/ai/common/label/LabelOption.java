package com.ai.common.label;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 标签下拉实体
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LabelOption<ValueType, LabelType> {

    private ValueType value;

    private LabelType label;

}
