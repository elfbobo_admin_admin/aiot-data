package com.ai.common.support.sms.strategy.plugins;

import com.ai.common.support.sms.entity.SmsBaseParam;
import com.ai.common.support.sms.enums.SmsStrategy;
import com.ai.common.support.sms.strategy.SendServiceCommon;
import com.ai.common.utils.RedisUtil;
import com.ai.common.webApi.baseResource.BaseResourceWebApi;
import com.ai.common.webApi.log.SysLogSmsWebApi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.stereotype.Component;

@Component
public class SendTestStrategy extends SendServiceCommon {

    @Autowired
    public SendTestStrategy(RedisUtil redisUtils, HashOperations<String, Object, SmsBaseParam> hashOperations, SysLogSmsWebApi logSmsWebApi, BaseResourceWebApi resourceConfigWebApi) {
        super(redisUtils, hashOperations, logSmsWebApi, resourceConfigWebApi);
    }

    @Override
    protected void sendExecute(SmsBaseParam param) {
        log.info("[短信发送 - 测试短信] uniqueKey: {},text: {}", param.getUniqueKey(), this.buildSendCodeText(param.getCode()));
    }

    @Override
    public SmsStrategy type() {
        return SmsStrategy.test;
    }

}
