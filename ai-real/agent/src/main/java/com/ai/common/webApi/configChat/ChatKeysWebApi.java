package com.ai.common.webApi.configChat;

import com.ai.entity.ChatKeysModel;

public interface ChatKeysWebApi {

    ChatKeysModel queryByApiToken(String apiToken);

}
