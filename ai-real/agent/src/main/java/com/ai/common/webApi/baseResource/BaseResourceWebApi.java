package com.ai.common.webApi.baseResource;

import com.ai.vo.ResourceDrawVO;
import com.ai.vo.ResourceMainVO;
import com.ai.vo.ResourceChatConfigVO;
import com.ai.vo.ResourceSmsConfigVO;

public interface BaseResourceWebApi {


    /**
     * ResourceMainVO 配置
     *
     * @return
     */
    ResourceMainVO getResourceMain();

    /**
     * ResourceChatConfigVO
     *
     * @return
     */
    ResourceChatConfigVO getResourceChatConfigVO();

    /**
     * ResourceDrawVO 配置
     *
     * @return
     */
    ResourceDrawVO getResourceDraw();

    ResourceSmsConfigVO getResourceSmsConfig();


}
