package com.ai.common.webApi.openCnofig;

public interface OpenConfigCacheFlush {

    void flushCache(String uniqueKey);

}
