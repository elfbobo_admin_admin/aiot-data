package com.ai.common.support.sms.strategy.plugins;

import com.ai.common.exception.BusinessException;
import com.ai.common.support.sms.entity.SmsBaseParam;
import com.ai.common.support.sms.enums.SmsStrategy;
import com.ai.common.support.sms.strategy.SendServiceCommon;
import com.ai.common.utils.RedisUtil;
import com.ai.common.webApi.baseResource.BaseResourceWebApi;
import com.ai.common.webApi.log.SysLogSmsWebApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;


@Component
public class SendEmailStrategy extends SendServiceCommon {

    @Resource
    private JavaMailSender mailSender;
    @Value("${spring.mail.username}")
    private String sender;

    @Autowired
    public SendEmailStrategy(RedisUtil redisUtils, HashOperations<String, Object, SmsBaseParam> hashOperations, SysLogSmsWebApi logSmsWebApi, BaseResourceWebApi resourceConfigWebApi) {
        super(redisUtils, hashOperations, logSmsWebApi, resourceConfigWebApi);
    }

    @Override
    public SmsStrategy type() {
        return SmsStrategy.mail;
    }

    @Override
    protected void sendExecute(SmsBaseParam param) {

        String uniqueKey = param.getUniqueKey();
        String code = param.getCode();
        String sendText = this.buildSendCodeText(code);

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
        try {
            helper.setFrom(sender);
            helper.setTo(uniqueKey.split(","));
            helper.setSubject("HugAi");
            helper.setText(sendText);
            helper.setSentDate(new Date());
        } catch (MessagingException e) {
            e.printStackTrace();
            throw new BusinessException("邮件发送失败");
        }
        mailSender.send(mimeMessage);
    }

}
