package com.ai.common.support.sms.entity;

import com.ai.common.support.sms.enums.SmsTypeEnum;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
public class SmsBaseParam implements Serializable {


    /**
     * 短信类型 {@link SmsTypeEnum}
     */
    @NotNull(message = "短信类型不能为空")
    private String type;
    /**
     * 唯一标识
     */
    private String uniqueKey;
    /**
     * 短信内容
     */
    private String code;
    /**
     * 验证码
     */
    private String originalText;
    /**
     * 发送次数
     */
    private Integer count = 0;
    /**
     * 是否已使用 0 否 1 是
     */
    private Boolean status = Boolean.FALSE;
    /**
     * 创建时间
     */
    private Date createTime = new Date();
    /**
     * 到期时间
     */
    private Date expireTime;


}
