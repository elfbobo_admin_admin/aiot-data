package com.ai.convert;

import com.ai.dto.DomainDTO;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.ai.entity.DomainModel;

@Mapper(builder = @Builder(disableBuilder = true))
public interface DomainConvert extends BaseConvert<DomainModel, DomainDTO> {

    DomainConvert INSTANCE = Mappers.getMapper(DomainConvert.class);

}
