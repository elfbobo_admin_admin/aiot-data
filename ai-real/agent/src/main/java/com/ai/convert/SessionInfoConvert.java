package com.ai.convert;

import com.ai.dto.SessionInfoDTO;
import com.ai.entity.SessionInfoModel;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * 会话表 Convert
 */
@Mapper(builder = @Builder(disableBuilder = true))
public interface SessionInfoConvert extends BaseConvert<SessionInfoModel, SessionInfoDTO> {

    SessionInfoConvert INSTANCE = Mappers.getMapper(SessionInfoConvert.class);

}
