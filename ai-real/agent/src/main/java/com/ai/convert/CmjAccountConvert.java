package com.ai.convert;

import com.ai.dto.CmjAccountDTO;
import com.ai.entity.CmjAccountModel;
import com.ai.vo.CmjAccountDetailVO;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


@Mapper(builder = @Builder(disableBuilder = true))
public interface CmjAccountConvert extends BaseConvert<CmjAccountModel, CmjAccountDTO> {

    CmjAccountConvert INSTANCE = Mappers.getMapper(CmjAccountConvert.class);

    CmjAccountDetailVO convertDetailVo(CmjAccountModel model);

}
