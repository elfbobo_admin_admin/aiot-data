package com.ai.convert;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(builder = @Builder(disableBuilder = true))
public class GalleryModelConvert {
    GalleryModelConvert INSTANCE = Mappers.getMapper(GalleryModelConvert.class);
}
