package com.ai.convert;

import com.ai.dto.SessionInfoDrawDTO;
import com.ai.entity.SessionInfoDrawModel;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * 会话表 Convert
 */
@Mapper(builder = @Builder(disableBuilder = true))
public interface SessionInfoDrawConvert extends BaseConvert<SessionInfoDrawModel, SessionInfoDrawDTO> {

    SessionInfoDrawConvert INSTANCE = Mappers.getMapper(SessionInfoDrawConvert.class);

}
