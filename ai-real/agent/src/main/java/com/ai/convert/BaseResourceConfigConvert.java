package com.ai.convert;

import com.ai.entity.BaseResourceConfigEntity;
import com.ai.vo.BaseResourceConfigVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface BaseResourceConfigConvert {
    BaseResourceConfigConvert INSTANCE = Mappers.getMapper(BaseResourceConfigConvert.class);

    BaseResourceConfigEntity convert(BaseResourceConfigVo vo);

    BaseResourceConfigVo convert(BaseResourceConfigEntity entity);

    List<BaseResourceConfigVo> convertList(List<BaseResourceConfigEntity> list);

}
