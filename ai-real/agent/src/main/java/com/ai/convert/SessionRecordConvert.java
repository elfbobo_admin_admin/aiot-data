package com.ai.convert;

import com.ai.dto.SessionRecordDTO;
import com.ai.entity.SessionRecordModel;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * 会话详情 Convert
 */
@Mapper(builder = @Builder(disableBuilder = true))
public interface SessionRecordConvert extends BaseConvert<SessionRecordModel, SessionRecordDTO> {

    SessionRecordConvert INSTANCE = Mappers.getMapper(SessionRecordConvert.class);

}
