package com.ai.convert;

import com.ai.vo.draw.openai.OpenaiTxt2ImgRequest;
import com.theokanning.openai.image.CreateImageRequest;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(builder = @Builder(disableBuilder = true))
public interface DrawOpenaiConvert {

    DrawOpenaiConvert INSTANCE = Mappers.getMapper(DrawOpenaiConvert.class);

    CreateImageRequest convertApiParam(OpenaiTxt2ImgRequest param);

}
