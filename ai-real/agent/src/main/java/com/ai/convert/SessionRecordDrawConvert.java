package com.ai.convert;

import com.ai.dto.SessionRecordDrawDTO;
import com.ai.entity.SessionRecordDrawModel;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(builder = @Builder(disableBuilder = true))
public interface SessionRecordDrawConvert extends BaseConvert<SessionRecordDrawModel, SessionRecordDrawDTO> {

    SessionRecordDrawConvert INSTANCE = Mappers.getMapper(SessionRecordDrawConvert.class);

}
