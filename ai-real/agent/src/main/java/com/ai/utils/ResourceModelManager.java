package com.ai.utils;


import cn.hutool.core.util.StrUtil;
import com.ai.entity.BaseResourceConfigEntity;


import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 系统配置缓存管理
 */
public class ResourceModelManager {

    private static final ConcurrentHashMap<String, BaseResourceConfigEntity> CACHE_MAP = new ConcurrentHashMap<>();

    public static void add(String key, BaseResourceConfigEntity model) {
        if (StrUtil.isEmpty(key) || Objects.isNull(model))
            return;
        CACHE_MAP.put(key, model);
    }

    public static BaseResourceConfigEntity get(String key) {
        if (StrUtil.isEmpty(key))
            return null;
        return CACHE_MAP.get(key);
    }

    public static void remove(String key) {
        if (StrUtil.isEmpty(key))
            return;
        CACHE_MAP.remove(key);
    }

    public static void removeAll() {
        CACHE_MAP.keySet().forEach(CACHE_MAP::remove);
    }

}
