package com.ai.vo.draw;

import com.ai.entity.SessionInfoDrawModel;
import com.ai.entity.SessionRecordDrawModel;
import lombok.Data;

import java.util.List;

@Data
public class DrawPersistenceCollection {

    private SessionInfoDrawModel sessionInfoDrawModelInsert;

    private List<SessionRecordDrawModel> sessionRecordDrawModelListInsert;

}
