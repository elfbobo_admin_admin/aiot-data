package com.ai.vo;

import com.ai.entity.CmjAccountModel;
import com.ai.entity.CmjChannelConfigModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
public class CmjAccountDetailVO extends CmjAccountModel {

    private List<CmjChannelConfigModel> channelConfigList;

    private List<String> channelIds;

}
