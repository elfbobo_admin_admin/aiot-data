package com.ai.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 模型配置
 */
@Data
public class ResourceChatConfigVO implements Serializable {

    /**
     * 绘图优化默认使用配置
     */
    private Long drawPromptOptimizeChatModelId;
    /**
     * 绘图优化默认prompt
     */
    private String drawPromptOptimizeContent;

}
