package com.ai.core.chat.account.strategy;

import com.ai.common.enums.flow.ChatSdkType;
import com.ai.common.webApi.baseResource.BaseResourceWebApi;
import com.ai.core.chat.account.strategy.common.SdkAccountBuildCommon;
import com.ai.service.IChatKeysService;
import com.ai.service.IChatModelService;
import com.ai.service.IChatSdkHostService;
import com.ai.service.IChatSdkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OpenaiAccountBuilderStrategy extends SdkAccountBuildCommon {

    @Autowired
    public OpenaiAccountBuilderStrategy(BaseResourceWebApi baseResourceWebApi, IChatKeysService chatKeysService, IChatSdkHostService chatSdkHostService, IChatModelService chatModelService, IChatSdkService chatSdkService) {
        super(baseResourceWebApi, chatKeysService, chatSdkHostService, chatModelService, chatSdkService);
    }

    @Override
    public ChatSdkType type() {
        return ChatSdkType.openai;
    }


}
