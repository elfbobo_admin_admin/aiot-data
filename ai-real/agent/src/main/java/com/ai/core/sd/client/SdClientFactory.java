package com.ai.core.sd.client;

import com.ai.common.utils.okhttp.OkhttpClientUtil;
import com.ai.common.webApi.baseResource.BaseResourceWebApi;
import com.ai.core.sd.client.api.Api;
import net.srt.framework.common.utils.SpringUtils;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;

import java.util.concurrent.TimeUnit;


public class SdClientFactory {

    public static OkHttpClient getClient() {

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.MINUTES)
                .writeTimeout(10, TimeUnit.MINUTES)
                .readTimeout(10, TimeUnit.MINUTES)
                .addInterceptor(chain -> {
                    // 设置请求头
                    String token = "";
                    Request request = chain.request()
                            .newBuilder()
                            .header("Authorization" , "Bearer " + token)
                            .build();
                    return chain.proceed(request);
                })
                .connectionPool(new ConnectionPool(20, 10L, TimeUnit.MINUTES)).build();

        return client;
    }

    public static SdApiClientService createService() {
        OkHttpClient client = getClient();

        String sdHostUrl = SpringUtils.getBean(BaseResourceWebApi.class).getResourceDraw().getSdHostUrl();

        Retrofit retrofit = OkhttpClientUtil.defaultRetrofit(client, sdHostUrl);

        return new SdApiClientService(retrofit.create(Api.class));

    }

}
