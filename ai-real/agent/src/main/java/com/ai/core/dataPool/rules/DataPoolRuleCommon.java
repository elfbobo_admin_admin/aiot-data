package com.ai.core.dataPool.rules;

import cn.hutool.core.lang.Assert;
import com.ai.core.dataPool.model.DataElement;
import com.ai.core.dataPool.sign.DataExecute;
import com.ai.common.exception.BusinessException;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public abstract class DataPoolRuleCommon implements DataExecute {

    protected List<DataElement> dataElements;

    protected DataElement e;

    protected List<Integer> cumulativeWeights = new ArrayList<>();

    protected int totalWeight = 0;

    public DataPoolRuleCommon(Supplier<List<DataElement>> getDataElementFunction) {
        this.dataElements = getDataElementFunction.get();

        for (DataElement item : this.dataElements) {
            this.totalWeight += item.getWeightValue();
            this.cumulativeWeights.add(totalWeight);
        }
    }

    @Override
    public DataElement getElement() {
        this.execute();
        Assert.notNull(this.e, () -> new BusinessException("数据池获取数据异常"));
        return this.e;
    }
}
