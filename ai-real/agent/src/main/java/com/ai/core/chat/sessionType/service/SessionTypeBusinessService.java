package com.ai.core.chat.sessionType.service;

import com.ai.common.enums.SessionType;
import com.ai.entity.DomainModel;
import com.ai.entity.SessionRecordModel;

import java.util.List;

/**
 * 会话类型业务接口
 */
public interface SessionTypeBusinessService {

    /**
     * 标识
     *
     * @return
     */
    SessionType type();

    /**
     * 创建会话前前置操作
     *
     * @param domainModel
     * @return
     */
    List<SessionRecordModel> createSessionBeforeAddRecord(DomainModel domainModel);

}
