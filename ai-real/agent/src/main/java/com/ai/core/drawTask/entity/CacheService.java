package com.ai.core.drawTask.entity;

import com.ai.chatsdk.common.context.ChatBusinessServiceContext;
import com.ai.core.chat.account.SdkAccountBuildContext;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CacheService {

    private SdkAccountBuildContext sdkAccountBuildContext;

    private ChatBusinessServiceContext chatBusinessServiceContext;

}
