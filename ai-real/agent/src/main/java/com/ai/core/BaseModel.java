package com.ai.core;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.ai.common.utils.uuid.SFIDWorker;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 公共实体model
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseModel implements Serializable {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间", type = "String")
    private String createTime;
    /**
     * 创建者
     */
    @Schema(description = "创建者", type = "String")
    private String createOper;
    /**
     * 操作时间
     */
    @Schema(description = "操作时间", type = "String")
    private String updateTime;
    /**
     * 操作人
     */
    @Schema(description = "操作人", type = "String")
    private String updateOper;
    /**
     * 删除标识 0 未删除 1 已删除
     */
    @Schema(description = "删除标识 0 未删除 1 已删除", type = "int")
    private int delFlag;

    @TableField(exist = false)
    @Schema(description = "创建时间-第二参数", type = "String")
    private String createTime_;
    @TableField(exist = false)
    @Schema(description = "修改时间-第二参数", type = "String")
    private String updateTime_;

    /**
     * 扩展参数
     */
    @TableField(exist = false)
    @Schema(description = "扩展参数", type = "Map")
    private Map<String, Object> paramExtMap = new HashMap<>();

    /**
     * key 字段名
     * value 查询条件 eq like
     */
    @TableField(exist = false)
    @Schema(description = "自定义查询参数", type = "Map")
    private Map<String, String> queryCondition = new HashMap<>();
    /**
     * key 字段名
     * value 排序条件
     * true asc  false desc
     */
    @TableField(exist = false)
    @Schema(description = "自定义排序参数", type = "Map")
    private Map<String, Boolean> sortCondition = new HashMap<>();

    @TableField(exist = false)
    @Schema(description = "分页页码", type = "int")
    private long page = 1;

    @TableField(exist = false)
    @Schema(description = "分页页数", type = "int")
    private long size = 10;

    /**
     * 获取唯一id
     */
    public static Long uniqueId() {
        return SFIDWorker.nextId();
    }

    /**
     * 初始化前操作
     */
    public void afterInitModel() {
    }

}

