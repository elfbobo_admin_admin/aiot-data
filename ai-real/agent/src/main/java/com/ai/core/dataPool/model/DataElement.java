package com.ai.core.dataPool.model;

import lombok.Data;

@Data
public class DataElement {

    /**
     * 值
     */
    private String value;
    /**
     * 权重
     */
    private Integer weightValue;

    private Object originalElement;

}
