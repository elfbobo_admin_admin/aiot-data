package com.ai.core.drawTask.strategy;

import com.ai.common.webApi.mjParam.MjParamWebApi;
import com.ai.common.websocket.constants.ResultCode;
import com.ai.core.drawTask.entity.CacheService;
import com.ai.core.midjourney.common.entity.TaskObj;
import com.ai.core.midjourney.service.MidjourneyTaskEventListener;
import com.ai.entity.draw.TaskDrawModel;
import com.ai.websocket.user.socket.UserMessagePushUtil;
import com.ai.service.SessionRecordDrawService;
import net.srt.framework.common.utils.SpringUtils;
import com.ai.common.exception.BusinessException;

public abstract class DrawMJAbstractStrategy<MappingCls> extends DrawAbstractStrategy<MappingCls> {

    protected MjParamWebApi paramWebApi;

    protected TaskObj taskObj;

    protected SessionRecordDrawService sessionRecordDrawService;

    public DrawMJAbstractStrategy(CacheService cacheService, TaskDrawModel drawData) {
        super(cacheService,drawData);
        this.paramWebApi = SpringUtils.getBean(MjParamWebApi.class);
        this.sessionRecordDrawService = SpringUtils.getBean(SessionRecordDrawService.class);
    }

    /**
     * executeApi 策略器完整处理
     *
     * @return
     */
    @Override
    protected void executeApiHandle() {
        try {
            this.mjApiExecute();
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof BusinessException){
                UserMessagePushUtil.pushMessageString(String.valueOf(this.drawData.getUserId()), ResultCode.S_MESSAGE_ERROR,e.getMessage());
            }
            SpringUtils.getBean(MidjourneyTaskEventListener.class).errorTask(this.taskObj.getNonce());
        }
    }

    /**
     * mj api 策略完整处理
     *
     * @return
     */
    protected abstract void mjApiExecute();

}
