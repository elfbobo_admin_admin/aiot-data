package com.ai.core.midjourney.common.utils;

import lombok.Data;

@Data
public class ContentParseData {
	protected String prompt;
	protected String status;
}
