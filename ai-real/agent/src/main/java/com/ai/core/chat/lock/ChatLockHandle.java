package com.ai.core.chat.lock;

import com.ai.common.constants.RedisConstant;
import com.ai.common.exception.BusinessException;
import com.ai.common.function.SingleFunction;
import com.ai.common.utils.uuid.UUID;
import lombok.extern.slf4j.Slf4j;

import net.srt.framework.common.utils.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * 会话锁任务处理
 * 每次使用一定要先调用 init 方法。否则容易造成冲突。
 */
@Slf4j
public class ChatLockHandle {

    // 默认超时时间 秒
    private final static long DEFAULT_TIME_OUT = 60 * 2;

    private RedisTemplate<String, String> redisTemplate;

    private ChatLockHandle() {
    }

    private ChatLockHandle(String groupName) {
        this.groupName = groupName;
    }

    private ChatLockHandle(String groupName, RedisTemplate<String, String> redisTemplate) {
        this.groupName = groupName;
        this.redisTemplate = redisTemplate;
    }

    /**
     * 分组名称
     */
    private String groupName;

    private String lockKey;
    private String lockValue;
    private String domainLockKey;
    private String domainLockValue;

    /**
     * 初始化
     *
     * @param groupName
     * @return
     */
    public static ChatLockHandle init(String groupName) {
        return new ChatLockHandle(groupName);
    }

    public static ChatLockHandle init(String groupName, RedisTemplate<String, String> redisTemplate) {
        return new ChatLockHandle(groupName, redisTemplate);
    }

    /**
     * 获取会话redis锁
     *
     * @param sessionType
     * @param sessionId
     * @return
     */
//    public RLock getLock(String sessionType, Long sessionId) {
//        final String KEY = RedisConstant.REDIS_LOCK + groupName + ":" + sessionType + ":" + sessionId;
//        return redissonClient.getLock(KEY);
//    }
    public boolean getLock(String sessionType, Long sessionId) {
//        lockValue = UUID.randomUUID().toString();
        lockKey = RedisConstant.REDIS_LOCK + groupName + ":" + sessionType + ":" + sessionId;
        Boolean lockResult = redisTemplate.opsForValue().setIfAbsent(lockKey, lockKey);
        return Boolean.TRUE.equals(lockResult);
    }

    public boolean getLock(String sessionType, Long sessionId, long timeout, TimeUnit uimeUnit) {
//        lockValue = UUID.randomUUID().toString();
        lockKey = RedisConstant.REDIS_LOCK + groupName + ":" + sessionType + ":" + sessionId;
        Boolean lockResult = redisTemplate.opsForValue().setIfAbsent(lockKey, lockKey, timeout, uimeUnit);
        return Boolean.TRUE.equals(lockResult);
    }

    /**
     * 获取领域会话redis锁
     *
     * @param sessionType
     * @param sessionId
     * @param domainUniqueKey
     * @return
     */
//    public RLock getLock(String sessionType, Long sessionId, String domainUniqueKey) {
//        final String KEY = RedisConstant.REDIS_LOCK + groupName + ":" + sessionType + ":" + domainUniqueKey + ":" + sessionId;
//        return redissonClient.getLock(KEY);
//    }

    public boolean getLock(String sessionType, Long sessionId, String domainUniqueKey) {
//        domainLockValue = UUID.randomUUID().toString();
        domainLockKey = RedisConstant.REDIS_LOCK + groupName + ":" + sessionType + ":" + domainUniqueKey + ":" + sessionId;
        Boolean lockResult = redisTemplate.opsForValue().setIfAbsent(domainLockKey, domainLockKey);
        return Boolean.TRUE.equals(lockResult);
    }

    public boolean getLock(String sessionType, Long sessionId, String domainUniqueKey, long timeout, TimeUnit uimeUnit) {
//        domainLockValue = UUID.randomUUID().toString();
        domainLockKey = RedisConstant.REDIS_LOCK + groupName + ":" + sessionType + ":" + domainUniqueKey + ":" + sessionId;
        Boolean lockResult = redisTemplate.opsForValue().setIfAbsent(domainLockKey, domainLockKey, timeout, uimeUnit);
        return Boolean.TRUE.equals(lockResult);
    }

    public boolean unlock(String sessionType, Long sessionId) {
        String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
        DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>();
        redisScript.setScriptText(script);
        redisScript.setResultType(Long.class);

        lockKey = RedisConstant.REDIS_LOCK + groupName + ":" + sessionType + ":" + sessionId;

        Long result = redisTemplate.execute(redisScript, Collections.singletonList(lockKey), lockKey);
        if (result != null && result.equals(1L)) {
            // 解锁成功
            return true;
        } else {
            // 解锁失败
            return false;
        }
    }

    public boolean unlock(String sessionType, Long sessionId, String domainUniqueKey) {
        String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
        DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>();
        redisScript.setScriptText(script);
        redisScript.setResultType(Long.class);

        domainLockKey = RedisConstant.REDIS_LOCK + groupName + ":" + sessionType + ":" + domainUniqueKey + ":" + sessionId;

        Long result = redisTemplate.execute(redisScript, Collections.singletonList(domainLockKey), domainLockKey);
        if (result != null && result.equals(1L)) {
            // 解锁成功
            return true;
        } else {
            // 解锁失败
            return false;
        }
    }


    /**
     * chat handle
     *
     * @param sessionType
     * @param sessionId
     * @param singleFunction
     * @param errorMessage
     */
    public void handle(String sessionType, Long sessionId, SingleFunction singleFunction, String errorMessage) {
        final String KEY = RedisConstant.REDIS_LOCK + groupName + ":" + sessionType + ":" + sessionId;

        boolean locked = getLock(sessionType, sessionId);

        if (!locked) {
            locked = getLock(sessionType, sessionId, DEFAULT_TIME_OUT, TimeUnit.SECONDS);
            log.info("[Redis Lock {}] - 加锁: {}", groupName, KEY);
        } else {
            log.info("[Redis Lock {}] - 占用中: {}", groupName, KEY);
            throw new BusinessException(errorMessage);
        }

        try {
            if (locked) {
                log.info("[Redis Lock {}] - 锁正在执行: {}", groupName, KEY);
                singleFunction.run();
            } else {
                throw new BusinessException(errorMessage);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(e.getMessage());
        } finally {
            if (locked) {
                log.info("[Redis Lock {}] - 释放锁: {}", groupName, KEY);
                unlock(sessionType, sessionId);
            }
        }
    }

    /**
     * domain handle
     *
     * @param sessionType
     * @param sessionId
     * @param domainUniqueKey
     * @param singleFunction
     * @param errorMessage
     */
    public void handle(String sessionType, Long sessionId, String domainUniqueKey, SingleFunction singleFunction, String errorMessage) {
        final String KEY = RedisConstant.REDIS_LOCK + groupName + ":" + sessionType + ":" + domainUniqueKey + ":" + sessionId;

        boolean locked = getLock(sessionType, sessionId, domainUniqueKey);

        if (!locked) {
            locked = getLock(sessionType, sessionId, domainUniqueKey, DEFAULT_TIME_OUT, TimeUnit.SECONDS);
            log.info("[Redis Lock {}] - 加锁: {}", groupName, KEY);
        } else {
            log.info("[Redis Lock {}] - 占用中: {}", groupName, KEY);
            throw new BusinessException(errorMessage);
        }

        try {
            if (locked) {
                log.info("[Redis Lock {}] - 锁正在执行: {}", groupName, KEY);
                singleFunction.run();
            } else {
                throw new BusinessException(errorMessage);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(e.getMessage());
        } finally {
            if (locked) {
                log.info("[Redis Lock {}] - 释放锁: {}", groupName, KEY);
                unlock(sessionType, sessionId, domainUniqueKey);
            }
        }
    }

}
