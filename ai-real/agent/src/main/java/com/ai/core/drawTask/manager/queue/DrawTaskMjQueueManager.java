package com.ai.core.drawTask.manager.queue;

import com.ai.core.drawTask.manager.DrawTaskDataManager;
import com.ai.core.midjourney.service.MidjourneyTaskEventListener;
import com.ai.service.TaskDrawService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class DrawTaskMjQueueManager extends DrawTaskDataManager {

    @Resource
    private MidjourneyTaskEventListener taskEventListener;
    @Resource
    private TaskDrawService taskDrawService;

    public DrawTaskMjQueueManager() {
        super(3, 350, 10, 300);
    }

    /**
     * 任务超时回调
     *
     * @param id
     */
    @Override
    protected void timeOutCallBack(String id) {
        taskEventListener.errorTask(id, "任务超时");
    }

    /**
     * 执行异常回调
     *
     * @param id
     * @param exception
     */
    @Override
    protected void runBeforeException(String id, Exception exception) {
        taskEventListener.errorTask(id, "超过最大任务数");
    }

}
