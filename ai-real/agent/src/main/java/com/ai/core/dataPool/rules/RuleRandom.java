package com.ai.core.dataPool.rules;

import com.ai.core.dataPool.model.DataElement;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;


public class RuleRandom extends DataPoolRuleCommon {

    public RuleRandom(Supplier<List<DataElement>> getDataElementFunction) {
        super(getDataElementFunction);
    }

    @Override
    public void execute() {
        Random random = new Random();
        if (totalWeight == 0) {
            this.e = this.dataElements.get(random.nextInt(super.dataElements.size()));
        } else {
            int randomValue = random.nextInt(totalWeight);
            for (int i = 0; i < cumulativeWeights.size(); i++) {
                if (randomValue < cumulativeWeights.get(i)) {
                    this.e = super.dataElements.get(i);
                    return;
                }
            }
        }
    }

}
