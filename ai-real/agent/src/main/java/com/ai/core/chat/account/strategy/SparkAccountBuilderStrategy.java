package com.ai.core.chat.account.strategy;

import cn.hutool.core.lang.Assert;
import com.ai.common.exception.BusinessException;
import com.ai.core.context.UserThreadLocal;
import com.ai.entity.ChatKeysModel;
import com.ai.entity.ChatModelModel;
import com.ai.entity.ChatSdkModel;
import com.ai.vo.ResourceMainVO;
import com.alibaba.fastjson2.JSON;
import com.ai.chatsdk.common.entity.account.ChatSdkAccount;
import com.ai.chatsdk.common.entity.account.ClientParam;

import com.ai.common.enums.flow.ChatSdkType;

import com.ai.common.webApi.baseResource.BaseResourceWebApi;
import com.ai.core.chat.account.strategy.common.SdkAccountBuildCommon;
import com.ai.core.chat.entity.ChatRequestParam;
import com.ai.core.dataPool.DataRuleEnum;
import com.ai.core.dataPool.factory.DataFactoryChatKey;

import com.ai.service.IChatKeysService;
import com.ai.service.IChatModelService;
import com.ai.service.IChatSdkHostService;
import com.ai.service.IChatSdkService;
import com.ai.common.function.OR;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class SparkAccountBuilderStrategy extends SdkAccountBuildCommon {

    @Autowired
    public SparkAccountBuilderStrategy(BaseResourceWebApi baseResourceWebApi, IChatKeysService chatKeysService, IChatSdkHostService chatSdkHostService, IChatModelService chatModelService, IChatSdkService chatSdkService) {
        super(baseResourceWebApi, chatKeysService, chatSdkHostService, chatModelService, chatSdkService);
    }

    @Override
    public ChatSdkType type() {
        return ChatSdkType.spark;
    }

    @Override
    public ChatSdkAccount buildSdkAccount(ChatModelModel chatModel, ChatSdkModel chatSdk, ChatRequestParam chatRequestParam) {
        Assert.notNull(chatModel, () -> new BusinessException("构建对话参数失败，chatMode不能为空"));
        Long chatSdkId = chatSdk.getId();

        ResourceMainVO resourceMain = baseResourceWebApi.getResourceMain();
        List<ChatKeysModel> chatKeys = chatKeysService.getAbleByChatSdkId(chatSdkId);

        String rulesName = DataRuleEnum.getByName(chatSdk.getKeysRules()).name();
        ChatKeysModel chatKeysModel = DataFactoryChatKey.poolGetKey(rulesName, chatKeys);

        ChatSdkAccount account = new ChatSdkAccount();
        account.setUserId(UserThreadLocal.getUserId());
        account.setSdkUniqueKey(this.type().getKey());
        account.setApiToken(chatKeysModel.getApiToken());
        account.setAppId(chatKeysModel.getAppId());
        account.setApiSecret(chatKeysModel.getApiSecret());
        OR.run(chatModel, Objects::nonNull, e -> {
            account.setModelValue(e.getModelValue());
            account.setOnceToken(e.getOnceToken());
        });
        OR.run(chatRequestParam, Objects::nonNull, e -> {
            account.setConnectId(e.getConnectId());
            account.setExtendParam(e.getExtendParam());
            account.setChatRequestParamJson(JSON.toJSONString(e));
        });

        // 客户端配置
        ClientParam clientParam = new ClientParam();
        clientParam.setBaseUrl(chatModel.getRequestUrl());
        clientParam.setIfProxy(false);
        clientParam.setProxyHost(resourceMain.getProxyHost());
        clientParam.setProxyPort(resourceMain.getProxyPort());

        account.setClientParam(clientParam);
        return account;
    }
}
