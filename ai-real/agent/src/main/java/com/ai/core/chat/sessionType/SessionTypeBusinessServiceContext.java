package com.ai.core.chat.sessionType;

import com.ai.common.support.strategy.StrategyServiceContext;
import com.ai.core.chat.sessionType.service.SessionTypeBusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;

@Component
public class SessionTypeBusinessServiceContext extends StrategyServiceContext<SessionTypeBusinessService> {

    @Autowired
    public SessionTypeBusinessServiceContext(List<SessionTypeBusinessService> sessionTypeBusinessServices) {
        super(sessionTypeBusinessServices);
    }

    @Override
    protected Function<SessionTypeBusinessService, String> serviceKey() {
        return s -> s.type().name();
    }
}
