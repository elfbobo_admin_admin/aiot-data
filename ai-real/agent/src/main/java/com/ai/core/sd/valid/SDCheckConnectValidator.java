package com.ai.core.sd.valid;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.ai.common.webApi.baseResource.BaseResourceWebApi;
import com.ai.core.sd.valid.annotation.CheckSDHostConnect;
import com.ai.vo.ResourceDrawVO;
import net.srt.framework.common.utils.SpringUtils;
import com.ai.common.exception.BusinessException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class SDCheckConnectValidator implements ConstraintValidator<CheckSDHostConnect, Object> {

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        ResourceDrawVO resourceDraw = SpringUtils.getBean(BaseResourceWebApi.class).getResourceDraw();
        String sdHostUrl = resourceDraw.getSdHostUrl();
        Assert.notEmpty(sdHostUrl, () -> new BusinessException("暂未开启SD绘图功能"));
        // 验证连接
        if (!verifyUrlConnect(sdHostUrl + "/user", 1000 * 3)) {
            return false;
        }
        return true;
    }

    public static boolean verifyUrlConnect(String urlString, int timeOutMillSeconds) {
        String response = HttpUtil.get(urlString, timeOutMillSeconds);
        if (StrUtil.NULL.equals(response)) {
            return true;
        }
        return false;
    }

}
