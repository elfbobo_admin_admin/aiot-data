package com.ai.core.chat.account.service;

import com.ai.chatsdk.common.entity.account.ChatSdkAccount;
import com.ai.common.enums.flow.ChatSdkType;
import com.ai.core.chat.entity.ChatRequestParam;
import com.ai.entity.ChatModelModel;
import com.ai.entity.ChatSdkModel;

public interface SdkAccountBuildService{

    /**
     * sdk 标识
     *
     * @return
     */
    ChatSdkType type();

    /**
     * 构建sdk账户参数
     *
     * @param chatModel
     * @param chatSdk
     * @param chatRequestParam
     * @return
     */
    ChatSdkAccount buildSdkAccount(ChatModelModel chatModel, ChatSdkModel chatSdk, ChatRequestParam chatRequestParam);


    ChatSdkAccount buildSdkAccount(String modelUnique);

    ChatSdkAccount buildSdkAccountBySdkUnique(String sdkUnique);

}
