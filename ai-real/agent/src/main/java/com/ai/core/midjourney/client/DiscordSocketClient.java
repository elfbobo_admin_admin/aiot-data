package com.ai.core.midjourney.client;

import com.ai.common.constants.Constants;
import com.ai.entity.CmjChannelConfigModel;
import com.ai.vo.CmjAccountDetailVO;
import com.ai.common.webApi.baseResource.BaseResourceWebApi;
import com.ai.core.midjourney.common.constants.DiscordConstant;
import com.ai.core.midjourney.common.constants.SocketClientParamConstants;
import com.ai.core.midjourney.common.entity.DiscordAccount;
import com.ai.core.midjourney.listener.DiscordSocketListener;
import com.ai.vo.ResourceMainVO;
import net.srt.framework.common.utils.SpringUtils;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;
import reactor.util.function.Tuples;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


public class DiscordSocketClient {

    /**
     * 全局帐号存储
     */
    private static Map<String, DiscordAccount> discordAccountMap = new ConcurrentHashMap<>();

    /**
     * 简历Discord ws连接，根据配置帐号
     *
     * @param discordAccount
     * @return
     */
    public static WebSocket connection(DiscordAccount discordAccount) {
        if (Objects.isNull(discordAccount)) {
            return null;
        }
        Request request = getRequest(discordAccount);
        ResourceMainVO resourceMain = SpringUtils.getBean(BaseResourceWebApi.class).getResourceMain();

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder().connectTimeout(2, TimeUnit.MINUTES);
        if (Constants.BOOLEAN.TRUE.equals(discordAccount.getIfProxy())){
            clientBuilder.proxy(
                    new Proxy(Proxy.Type.HTTP, new InetSocketAddress(resourceMain.getProxyHost(), resourceMain.getProxyPort()))
            );
        }
        OkHttpClient client = clientBuilder.build();
        WebSocket webSocket = client.newWebSocket(request, new DiscordSocketListener(discordAccount));

        DiscordSocketClient.getDiscordAccountMap().put(discordAccount.getUserName(), discordAccount);
        return webSocket;
    }

    private static Request getRequest(DiscordAccount discordAccount) {
        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(DiscordConstant.DISCORD_WSS_URL + "/?encoding=json&v=9&compress=zlib-stream");
        SocketClientParamConstants.getDefaultBrowserHeaders(discordAccount).forEach(requestBuilder::header);
        return requestBuilder.build();
    }

    public static DiscordAccount buildAccount(CmjAccountDetailVO param) {
        DiscordAccount discordAccount = new DiscordAccount(param.getUserName(), param.getUserToken(), param.getUserAgent());
        List<CmjChannelConfigModel> channelConfigList = param.getChannelConfigList();
        discordAccount.setChannelConfigList(
                channelConfigList.stream().map(channelConfig -> Tuples.of(channelConfig.getGuildId(), channelConfig.getChannelId())).collect(Collectors.toList())
        );
        discordAccount.setChannelIds(param.getChannelIds());
        discordAccount.setAutoData(SocketClientParamConstants.getDefaultAutoData(discordAccount));
        discordAccount.setIfProxy(param.getIfProxy());
        return discordAccount;
    }

    public static Map<String, DiscordAccount> getDiscordAccountMap() {
        return discordAccountMap;
    }
}
