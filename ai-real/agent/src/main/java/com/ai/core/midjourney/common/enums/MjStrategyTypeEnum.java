package com.ai.core.midjourney.common.enums;


public enum MjStrategyTypeEnum {

    Blend,

    Describe,

    ErrorMessage,

    Imagine,

    RerollSuccess,

    StartAndProgress,

    UpscaleSuccess,

    VariationSuccess,

    ;

}
