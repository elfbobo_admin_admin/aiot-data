package com.ai.core.midjourney.common.entity;

import lombok.Data;

/**
 * mj messageHandle响应实体存储
 */
@Data
public class MessageResponseData {

    private String imgUrl;

    private String messageId;

    private String messageHash;

}
