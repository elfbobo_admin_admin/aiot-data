package com.ai.core.drawTask.strategy;

/**
 * 绘图业务接口
 */
public interface DrawApiService extends DrawStrategySign {

    /**
     * 请求外部接口返回持久化集合实体
     *
     * @return
     */
    void executeApi();

}
