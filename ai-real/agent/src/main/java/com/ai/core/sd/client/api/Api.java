package com.ai.core.sd.client.api;

import com.ai.core.sd.entity.request.Img2ImgRequest;
import com.ai.core.sd.entity.request.TxtImgRequest;
import com.ai.core.sd.entity.response.Img2ImgResponse;
import com.ai.core.sd.entity.response.TxtImgResponse;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface Api {

    @POST("/sdapi/v1/txt2img")
    Single<TxtImgResponse> txt2img(@Body TxtImgRequest request);

    @POST("/sdapi/v1/img2img")
    Single<Img2ImgResponse> img2img(@Body Img2ImgRequest request);

    @GET("/user")
    Single<String> user();

}
