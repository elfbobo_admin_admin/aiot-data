package com.ai.core.drawTask.strategy.impl;

import com.alibaba.fastjson2.JSON;
import com.ai.chatsdk.common.entity.account.ChatSdkAccount;
import com.ai.chatsdk.openai.client.OpenaiClientFactory;
import com.ai.common.enums.flow.ChatSdkType;
import com.ai.core.drawTask.entity.CacheService;
import com.ai.core.drawTask.enums.DrawType;
import com.ai.entity.draw.TaskDrawModel;
import com.ai.vo.draw.openai.OpenaiTxt2ImgRequest;
import com.ai.core.chat.account.service.SdkAccountBuildService;
import com.ai.core.drawTask.entity.SessionCacheDrawData;
import com.ai.core.drawTask.manager.DrawTaskDataManager;
import com.ai.core.drawTask.manager.queue.DrawTaskOpenaiQueueManager;
import com.ai.core.drawTask.manager.service.DrawOpenaiResponseService;
import com.ai.core.drawTask.strategy.DrawAbstractStrategy;
import com.ai.convert.DrawOpenaiConvert;
import com.ai.common.constants.HttpStatus;
import net.srt.framework.common.utils.SpringUtils;
import com.ai.common.exception.BusinessException;
import com.theokanning.openai.OpenAiHttpException;
import com.theokanning.openai.image.CreateImageRequest;
import com.theokanning.openai.image.ImageResult;
import com.theokanning.openai.service.OpenAiService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ApiStrategyOpenaiTxtImg extends DrawAbstractStrategy<OpenaiTxt2ImgRequest> {

    public ApiStrategyOpenaiTxtImg(CacheService cacheService, TaskDrawModel drawData, SessionCacheDrawData cacheData) {
        super(cacheService,drawData, cacheData);
    }

    @Override
    protected Class<OpenaiTxt2ImgRequest> getMappingCls() {
        return OpenaiTxt2ImgRequest.class;
    }

    @Override
    public DrawType.ApiKey apiKey() {
        return DrawType.ApiKey.openai_txt2img;
    }

    @Override
    public void executeApiHandle() {
        String requestParam = this.drawData.getRequestParam();

        OpenaiTxt2ImgRequest apiRequestParam = JSON.parseObject(requestParam, this.getMappingCls());
        apiRequestParam.setSize(apiRequestParam.getSizeWidth() + "x" + apiRequestParam.getSizeHeight());

        final Long taskId = this.drawData.getId();

        SdkAccountBuildService accountBuildService = sdkAccountBuildContext.getService(ChatSdkType.openai.getKey(), BusinessException::new);
        ChatSdkAccount chatSdkAccount = accountBuildService.buildSdkAccountBySdkUnique(ChatSdkType.openai.getKey());

        OpenAiService openAiService = OpenaiClientFactory.getService(chatSdkAccount);

        CreateImageRequest apiParam = DrawOpenaiConvert.INSTANCE.convertApiParam(apiRequestParam);

        // 添加任务至队列管理器
        DrawTaskDataManager queueManager = SpringUtils.getBean(DrawTaskOpenaiQueueManager.class);
        queueManager.startSync(String.valueOf(taskId), () -> {
            ImageResult apiResponse;
            try {
                apiResponse = openAiService.createImage(apiParam);
                log.info("openai 文生图响应：{}", JSON.toJSONString(apiResponse));
            } catch (OpenAiHttpException e) {
                e.printStackTrace();
                int statusCode = e.statusCode;
                String code = e.code;
                if (HttpStatus.UNAUTHORIZED == statusCode || "insufficient_quota".equals(code)) {
                    // todo 停用账号
                }
                throw e;
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
            DrawOpenaiResponseService responseService = SpringUtils.getBean(DrawOpenaiResponseService.class);
            responseService.handleTxt2img(String.valueOf(taskId), apiParam, apiResponse);
        });

    }

}
