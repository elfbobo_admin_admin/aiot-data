package com.ai.core.midjourney;

import cn.hutool.core.collection.CollUtil;
import com.ai.common.enums.flow.AccountStatus;
import com.ai.vo.CmjAccountDetailVO;
import com.ai.core.midjourney.client.DiscordSocketClient;
import com.ai.core.midjourney.common.entity.DiscordAccount;
import com.ai.service.ICmjAccountService;
import com.ai.common.function.OR;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 初始化连接
 * @since https://github.com/novicezk/midjourney-proxy
 */
@Component
public class DiscordInitializer implements ApplicationRunner {

    @Resource
    private ICmjAccountService accountService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        accountService.flushCache();
        OR.run(accountService.getAccountAll(), CollUtil::isNotEmpty, accountAll -> {
            for (CmjAccountDetailVO accountVo : accountAll) {
                if (AccountStatus.NORMAL.getKey().equals(accountVo.getAccountStatus())){
                    DiscordAccount discordAccount = DiscordSocketClient.buildAccount(accountVo);
                    // 建立discord ws连接
                    DiscordSocketClient.connection(discordAccount);
                }
            }
        });
    }

}
