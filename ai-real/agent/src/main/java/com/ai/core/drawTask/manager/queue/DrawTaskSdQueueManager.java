package com.ai.core.drawTask.manager.queue;

import com.ai.core.drawTask.manager.DrawTaskDataManager;
import com.ai.entity.draw.TaskDrawModel;
import com.ai.service.TaskDrawService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


@Component
public class DrawTaskSdQueueManager extends DrawTaskDataManager {

    @Resource
    private TaskDrawService taskDrawService;

    public DrawTaskSdQueueManager() {
        super(1, 300, 20, 5);
    }

    /**
     * 任务超时回调
     *
     * @param id
     */
    @Override
    protected void timeOutCallBack(String id) {

    }

    /**
     * 执行异常回调
     *
     * @param id
     * @param exception
     */
    @Override
    protected void runBeforeException(String id, Exception exception) {
        taskDrawService.failTask(id, wrapper -> {
            wrapper.set(TaskDrawModel::getRemark, exception.getMessage());
        });
        this.overQueue(id);
    }

}
