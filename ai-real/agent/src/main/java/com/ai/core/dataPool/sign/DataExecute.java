package com.ai.core.dataPool.sign;

import com.ai.core.dataPool.model.DataElement;


public interface DataExecute {

    /**
     * 获取数据值
     *
     * @return
     */
    DataElement getElement();

    /**
     * 执行器
     */
    void execute();

}
