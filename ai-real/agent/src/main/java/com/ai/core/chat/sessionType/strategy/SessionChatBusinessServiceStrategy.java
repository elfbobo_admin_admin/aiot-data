package com.ai.core.chat.sessionType.strategy;

import com.ai.common.enums.SessionType;
import org.springframework.stereotype.Service;

/**
 * CHAT 会话类型策略实现
 */
@Service
public class SessionChatBusinessServiceStrategy extends SessionBusinessCommon {

    @Override
    public SessionType type() {
        return SessionType.chat;
    }

}
