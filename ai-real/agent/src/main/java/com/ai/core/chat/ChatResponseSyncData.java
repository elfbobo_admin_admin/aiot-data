package com.ai.core.chat;

import cn.hutool.core.collection.CollUtil;
import com.ai.entity.SessionRecordModel;
import com.alibaba.fastjson2.JSON;
import com.ai.chatsdk.common.entity.ChatSdkStorageResponse;
import com.ai.chatsdk.common.entity.account.ChatSdkAccount;
import com.ai.chatsdk.common.entity.session.RecordData;
import com.ai.chatsdk.common.service.ChatResponseSyncDataService;
import com.ai.core.chat.entity.ChatRequestParam;
import com.ai.service.IChatKeysService;
import com.ai.service.SessionRecordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ChatResponseSyncData implements ChatResponseSyncDataService {

    @Resource
    private IChatKeysService chatKeysService;
    @Resource
    private SessionRecordService sessionRecordService;

    @Transactional
    @Override
    public List<SessionRecordModel> syncChatResponse(ChatSdkAccount requestParam, ChatSdkStorageResponse response, List<RecordData> recordList) {
        // 帐号异常停用key
        if (response.getAccountError()) {
            String apiToken = response.getAccount().getApiToken();
            chatKeysService.removeByApiToken(apiToken);
            return null;
        }
        ChatRequestParam chatRequestParam = JSON.parseObject(requestParam.getChatRequestParamJson(), ChatRequestParam.class);
        if (CollUtil.isNotEmpty(response.getResponseRecordData())) {
            return sessionRecordService.responseInsertHandle(recordList, chatRequestParam, response);
        }
        return null;
    }

}
