package com.ai.core.drawTask.strategy;

import com.ai.core.drawTask.enums.DrawType;

/**
 * 绘图策略器标识
 */
public interface DrawStrategySign {

    /**
     * 获取绘图api唯一标识
     *
     * @return
     */
    DrawType.ApiKey apiKey();

}
