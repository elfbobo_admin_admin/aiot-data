package com.ai.core.dataPool.factory;

import cn.hutool.core.lang.Assert;

import com.ai.core.dataPool.DataRuleEnum;
import com.ai.core.dataPool.model.DataElement;
import com.ai.core.dataPool.rules.RuleRandom;
import com.ai.entity.ChatKeysModel;
import com.ai.common.exception.BusinessException;

import java.util.List;
import java.util.Locale;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class DataFactoryChatKey {

    /**
     * @param ruleKey
     * @param chatKeysModelList
     * @return
     */
    public static ChatKeysModel poolGetKey(String ruleKey, List<ChatKeysModel> chatKeysModelList) {
        Assert.notNull(ruleKey, () -> new BusinessException("获取SDK秘钥失败，请指定规则"));

        Supplier<List<DataElement>> convertDataElement = () -> chatKeysModelList.stream().map(item -> {
            DataElement dataElement = new DataElement();
            dataElement.setValue(item.getApiToken());
            dataElement.setWeightValue(item.getWeightValue());
            dataElement.setOriginalElement(item);
            return dataElement;
        }).collect(Collectors.toList());

        if (DataRuleEnum.random.name().toUpperCase(Locale.ROOT).equals(ruleKey.toUpperCase(Locale.ROOT))) {
            return (ChatKeysModel) new RuleRandom(convertDataElement).getElement().getOriginalElement();
        }

        return (ChatKeysModel) new RuleRandom(convertDataElement).getElement().getOriginalElement();
    }

}
