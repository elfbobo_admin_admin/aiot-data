package com.ai.core.midjourney.common.entity.request;

import com.ai.entity.draw.valid.CreateTask;
import com.ai.core.midjourney.valid.annotation.FilterSenWord;
import com.ai.core.midjourney.valid.annotation.NotSocketConnect;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;

@NotSocketConnect(message = "Midjourney无法连接，请稍后重试或联系管理员", groups = {CreateTask.class})
@EqualsAndHashCode(callSuper = true)
@Data
public class MjTxt2ImgRequest extends MjBaseRequest {

    /**
     * 提示词
     */
    @FilterSenWord(message = "存在敏感词汇请重试", groups = {CreateTask.class})
    @NotEmpty(message = "提示词不能为空", groups = {CreateTask.class})
    private String prompt;

}
