package com.ai.core.chat.account;

import com.ai.common.support.strategy.StrategyServiceContext;
import com.ai.core.chat.account.service.SdkAccountBuildService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;

@Component
public class SdkAccountBuildContext extends StrategyServiceContext<SdkAccountBuildService> {

    @Autowired
    public SdkAccountBuildContext(List<SdkAccountBuildService> sdkAccountBuildServices) {
        super(sdkAccountBuildServices);
    }


    @Override
    protected Function<SdkAccountBuildService, String> serviceKey() {
        return s -> s.type().getKey();
    }
}
