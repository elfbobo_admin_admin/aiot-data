package com.ai.core.drawTask.context;

import com.ai.chatsdk.common.context.ChatBusinessServiceContext;
import com.ai.entity.draw.TaskDrawModel;
import com.ai.core.chat.account.SdkAccountBuildContext;
import com.ai.core.drawTask.entity.CacheService;
import com.ai.core.drawTask.entity.SessionCacheDrawData;
import com.ai.core.drawTask.enums.DrawType;
import com.ai.core.drawTask.strategy.DrawApiService;
import com.ai.core.drawTask.strategy.impl.ApiStrategyOpenaiImg2img;
import com.ai.core.drawTask.strategy.impl.ApiStrategyOpenaiTxtImg;
import com.ai.core.drawTask.strategy.impl.ApiStrategySdImg2Img;
import com.ai.core.drawTask.strategy.impl.ApiStrategySdTxtImg;
import com.ai.core.drawTask.strategy.impl.mj.ApiStrategyMjTxt2Img;
import com.ai.core.drawTask.strategy.impl.mj.ApiStrategyMjU;
import com.ai.core.drawTask.strategy.impl.mj.ApiStrategyMjV;
import com.ai.common.exception.BusinessException;

/**
 * 绘图业务上下文入口
 */
public class DrawApiServiceContext {

    private DrawApiServiceContext() {
    }

    private TaskDrawModel drawData;

    private SessionCacheDrawData cacheData;

    private CacheService cacheService;

    public static DrawApiServiceContext init(TaskDrawModel drawData) {
        return init(drawData, null);
    }

    public static DrawApiServiceContext init(TaskDrawModel drawData, SessionCacheDrawData cacheData) {
        return init(drawData, cacheData, null, null);
    }

    public static DrawApiServiceContext init(TaskDrawModel drawData, SessionCacheDrawData cacheData, SdkAccountBuildContext sdkAccountBuildContext, ChatBusinessServiceContext chatBusinessServiceContext) {
        DrawApiServiceContext context = new DrawApiServiceContext();
        context.drawData = drawData;
        context.cacheData = cacheData;
        context.cacheService = CacheService.builder()
                .chatBusinessServiceContext(chatBusinessServiceContext)
                .sdkAccountBuildContext(sdkAccountBuildContext)
                .build();
        return context;
    }

    public DrawApiService getDrawApiService() {
        DrawType.ApiKey drawApiKey = DrawType.ApiKey.getByName(drawData.getDrawApiKey());

        DrawApiService drawApiService;

        switch (drawApiKey) {
            case openai_txt2img -> drawApiService = new ApiStrategyOpenaiTxtImg(this.cacheService,this.drawData, this.cacheData);
            case openai_img2img -> drawApiService = new ApiStrategyOpenaiImg2img(this.cacheService,this.drawData, this.cacheData);
            case sd_txt2img -> drawApiService = new ApiStrategySdTxtImg(this.cacheService,this.drawData, this.cacheData);
            case sd_img2img -> drawApiService = new ApiStrategySdImg2Img(this.cacheService,this.drawData, this.cacheData);
            case mj_txt2img -> drawApiService = new ApiStrategyMjTxt2Img(this.cacheService,this.drawData);
            case mj_u -> drawApiService = new ApiStrategyMjU(this.cacheService,this.drawData);
            case mj_v -> drawApiService = new ApiStrategyMjV(this.cacheService,this.drawData);
            default -> throw new BusinessException("没有找到绘图策略服务");
        }
        return drawApiService;
    }

}
