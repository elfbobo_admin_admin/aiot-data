package com.ai.core.chat.account.strategy.common;

import com.ai.core.context.UserThreadLocal;
import com.ai.entity.ChatKeysModel;
import com.ai.entity.ChatSdkHostModel;
import com.ai.vo.ResourceMainVO;
import com.alibaba.fastjson2.JSON;
import com.ai.chatsdk.common.entity.account.ChatSdkAccount;
import com.ai.chatsdk.common.entity.account.ClientParam;
import com.ai.common.constants.Constants;

import com.ai.common.webApi.baseResource.BaseResourceWebApi;
import com.ai.core.chat.account.service.SdkAccountBuildService;
import com.ai.core.chat.entity.ChatRequestParam;
import com.ai.core.dataPool.DataRuleEnum;
import com.ai.core.dataPool.factory.DataFactoryChatHost;
import com.ai.core.dataPool.factory.DataFactoryChatKey;
import com.ai.service.IChatKeysService;
import com.ai.service.IChatModelService;
import com.ai.service.IChatSdkHostService;
import com.ai.service.IChatSdkService;
import com.ai.common.function.OR;
import com.ai.entity.ChatModelModel;
import com.ai.entity.ChatSdkModel;

import java.util.List;
import java.util.Objects;

public abstract class SdkAccountBuildCommon implements SdkAccountBuildService {

    protected BaseResourceWebApi baseResourceWebApi;
    protected IChatKeysService chatKeysService;
    protected IChatSdkHostService chatSdkHostService;
    protected IChatModelService chatModelService;
    protected IChatSdkService chatSdkService;

    public SdkAccountBuildCommon(BaseResourceWebApi baseResourceWebApi, IChatKeysService chatKeysService, IChatSdkHostService chatSdkHostService, IChatModelService chatModelService, IChatSdkService chatSdkService) {
        this.baseResourceWebApi = baseResourceWebApi;
        this.chatKeysService = chatKeysService;
        this.chatSdkHostService = chatSdkHostService;
        this.chatModelService = chatModelService;
        this.chatSdkService = chatSdkService;
    }

    @Override
    public ChatSdkAccount buildSdkAccount(ChatModelModel chatModel, ChatSdkModel chatSdk, ChatRequestParam chatRequestParam) {
        Long chatSdkId = chatSdk.getId();

        ResourceMainVO resourceMain = baseResourceWebApi.getResourceMain();
        List<ChatKeysModel> chatKeys = chatKeysService.getAbleByChatSdkId(chatSdkId);
        List<ChatSdkHostModel> hostList = chatSdkHostService.getUsableByChatSdkId(chatSdkId);

        String rulesName = DataRuleEnum.getByName(chatSdk.getKeysRules()).name();
        ChatKeysModel chatKeysModel = DataFactoryChatKey.poolGetKey(rulesName, chatKeys);

        ChatSdkAccount account = new ChatSdkAccount();
        account.setUserId(UserThreadLocal.getUserId());
        account.setSdkUniqueKey(this.type().getKey());
        account.setApiToken(chatKeysModel.getApiToken());
        account.setAppId(chatKeysModel.getAppId());
        OR.run(chatModel, Objects::nonNull, e -> {
            account.setModelValue(e.getModelValue());
            account.setOnceToken(e.getOnceToken());
        });
        OR.run(chatRequestParam, Objects::nonNull, e -> {
            account.setConnectId(e.getConnectId());
            account.setExtendParam(e.getExtendParam());
            account.setChatRequestParamJson(JSON.toJSONString(e));
        });

        // 客户端配置
        ChatSdkHostModel chatSdkHostModel = DataFactoryChatHost.poolGetKey(rulesName, hostList);
        ClientParam clientParam = new ClientParam();
        clientParam.setBaseUrl(chatSdkHostModel.getHostUrl());
        clientParam.setIfProxy(Constants.BOOLEAN.TRUE.equals(chatSdkHostModel.getIfProxy()));
        clientParam.setProxyHost(resourceMain.getProxyHost());
        clientParam.setProxyPort(resourceMain.getProxyPort());
        clientParam.setTimeoutValue(chatSdkHostModel.getTimeoutValue());
        clientParam.setMaxConnect(chatSdkHostModel.getMaxConnect());

        account.setClientParam(clientParam);
        return account;
    }

    @Override
    public ChatSdkAccount buildSdkAccount(String modelUnique) {
        ChatModelModel chatModel = chatModelService.getByUniqueKey(modelUnique);

        ChatSdkModel chatSdkModel = chatSdkService.getById(chatModel.getChatSdkId());

        return this.buildSdkAccount(chatModel, chatSdkModel, null);
    }

    @Override
    public ChatSdkAccount buildSdkAccountBySdkUnique(String sdkUnique) {

        ChatSdkModel chatSdkModel = chatSdkService.getByUniqueKey(sdkUnique);

        return this.buildSdkAccount(null, chatSdkModel, null);
    }
}
