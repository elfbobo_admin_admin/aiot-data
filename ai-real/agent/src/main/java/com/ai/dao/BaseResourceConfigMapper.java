package com.ai.dao;

import net.srt.framework.mybatis.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import com.ai.entity.BaseResourceConfigEntity;

@Mapper
public interface BaseResourceConfigMapper extends BaseDao<BaseResourceConfigEntity> {
}