package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.SessionRecordDrawModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * 图像会话详情 持久层接口
 */
@Mapper
public interface SessionRecordDrawMapper extends BaseMapper<SessionRecordDrawModel> {


}
