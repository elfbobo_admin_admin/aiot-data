package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.ChatModelModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * 对话模型管理Model 持久层接口
 */
@Mapper
public interface ChatModelMapper extends BaseMapper<ChatModelModel> {


}
