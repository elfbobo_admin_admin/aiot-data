package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.NoticeClientModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * NoticeClient 持久层接口
 */
@Mapper
public interface NoticeClientMapper extends BaseMapper<NoticeClientModel> {


}
