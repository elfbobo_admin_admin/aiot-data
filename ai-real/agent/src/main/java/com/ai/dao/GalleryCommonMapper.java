package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ai.entity.draw.GalleryCommonModel;
import com.ai.entity.SessionRecordDrawModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface GalleryCommonMapper extends BaseMapper<GalleryCommonModel> {

    IPage<SessionRecordDrawModel> queryCommonSessionRecord(IPage<SessionRecordDrawModel> page, @Param("param") SessionRecordDrawModel param);

}
