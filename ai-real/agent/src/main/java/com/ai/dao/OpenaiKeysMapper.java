package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.OpenaiKeysModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * apikeys 持久层接口
 */
@Mapper
public interface OpenaiKeysMapper extends BaseMapper<OpenaiKeysModel> {


}
