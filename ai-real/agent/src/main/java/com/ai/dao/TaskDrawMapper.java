package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.draw.TaskDrawModel;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TaskDrawMapper extends BaseMapper<TaskDrawModel> {
}
