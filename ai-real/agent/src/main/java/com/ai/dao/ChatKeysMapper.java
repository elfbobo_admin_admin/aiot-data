package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.ChatKeysModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * 对话秘钥池Model 持久层接口
 */
@Mapper
public interface ChatKeysMapper extends BaseMapper<ChatKeysModel> {


}
