package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.CmjAccountModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * mj账户配置 持久层接口
 */
@Mapper
public interface CmjAccountMapper extends BaseMapper<CmjAccountModel> {


}
