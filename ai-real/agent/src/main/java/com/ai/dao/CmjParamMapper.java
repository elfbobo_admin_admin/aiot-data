package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.CmjParamModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * mj参数配置 持久层接口
 */
@Mapper
public interface CmjParamMapper extends BaseMapper<CmjParamModel> {


}
