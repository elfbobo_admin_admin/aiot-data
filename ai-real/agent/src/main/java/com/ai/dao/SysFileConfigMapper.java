package com.ai.dao;

import com.ai.entity.SysFileConfigModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件上传配置 持久层接口
 */
@Mapper
public interface SysFileConfigMapper extends BaseMapper<SysFileConfigModel> {


}
