package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.DomainModel;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DomainMapper extends BaseMapper<DomainModel> {


}
