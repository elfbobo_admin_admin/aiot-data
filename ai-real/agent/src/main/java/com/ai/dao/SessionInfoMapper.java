package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.SessionInfoModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会话表 持久层接口
 */
@Mapper
public interface SessionInfoMapper extends BaseMapper<SessionInfoModel> {


}
