package com.ai.dao;

import com.ai.entity.SysLogSmsModel;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 验证码日志 持久层接口
 */
@Mapper
public interface SysLogSmsMapper extends BaseMapper<SysLogSmsModel> {


}
