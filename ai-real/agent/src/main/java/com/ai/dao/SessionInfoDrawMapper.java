package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.SessionInfoDrawModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * 绘图会话 持久层接口
 */
@Mapper
public interface SessionInfoDrawMapper extends BaseMapper<SessionInfoDrawModel> {


}
