package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.CmjChannelConfigModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * mj频道配置 持久层接口
 */
@Mapper
public interface CmjChannelConfigMapper extends BaseMapper<CmjChannelConfigModel> {


}
