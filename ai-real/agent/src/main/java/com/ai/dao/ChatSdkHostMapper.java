package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.ChatSdkHostModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * 对话镜像地址管理Model 持久层接口
 */
@Mapper
public interface ChatSdkHostMapper extends BaseMapper<ChatSdkHostModel> {


}
