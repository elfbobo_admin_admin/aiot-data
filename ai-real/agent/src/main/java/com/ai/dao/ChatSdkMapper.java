package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.ChatSdkModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * 对话第三方平台管理Model 持久层接口
 */
@Mapper
public interface ChatSdkMapper extends BaseMapper<ChatSdkModel> {


}
