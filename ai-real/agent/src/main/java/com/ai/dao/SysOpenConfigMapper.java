package com.ai.dao;

import com.ai.entity.SysOpenConfigModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 第三方配置 持久层接口
 */
@Mapper
public interface SysOpenConfigMapper extends BaseMapper<SysOpenConfigModel> {


}
