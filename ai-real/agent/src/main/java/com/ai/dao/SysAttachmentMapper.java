package com.ai.dao;

import com.ai.entity.SysAttachmentModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysAttachmentMapper extends BaseMapper<SysAttachmentModel> {
}
