package com.ai.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ai.entity.SessionRecordModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会话详情 持久层接口
 */
@Mapper
public interface SessionRecordMapper extends BaseMapper<SessionRecordModel> {


}
