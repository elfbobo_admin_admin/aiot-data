package com.ai.chatsdk.spark.entity.request;

import lombok.Data;

@Data
public class Chat {

    private String domain;

    private Double temperature;

    private Integer max_tokens;

    private Integer top_k;

    private String chat_id;

}
