package com.ai.chatsdk.spark.entity.response;

import com.ai.chatsdk.spark.entity.Text;
import lombok.Data;

import java.util.List;

@Data
public class Choices {

    private List<Text> text;

}