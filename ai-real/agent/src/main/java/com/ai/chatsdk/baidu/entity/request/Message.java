package com.ai.chatsdk.baidu.entity.request;

import lombok.Data;

@Data
public class Message {

    private String role;

    private String content;

}
