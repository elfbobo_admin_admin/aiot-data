package com.ai.chatsdk.common.entity.convert;

import com.ai.chatsdk.common.entity.session.RecordData;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.ai.entity.SessionRecordModel;

import java.util.List;

@Mapper(builder = @Builder(disableBuilder = true))
public interface RecordConvert {

    RecordConvert INSTANCE = Mappers.getMapper(RecordConvert.class);

    RecordData sessionConvertRecord(SessionRecordModel model);

    List<RecordData> sessionConvertRecord(List<SessionRecordModel> model);

}
