package com.ai.chatsdk.spark.entity.request;

import lombok.Data;

@Data
public class Header {

    private String app_id;

    private String uid;

}
