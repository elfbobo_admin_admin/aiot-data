package com.ai.chatsdk.spark.entity.request;

import lombok.Data;

@Data
public class Parameter {

    private Chat chat;

}
