package com.ai.chatsdk.spark.entity.response;

import lombok.Data;

@Data
public class ChatResponse {

    private Header header;

    private Payload payload;

}
