package com.ai.chatsdk.common.service;

import com.ai.chatsdk.common.entity.ChatSdkStorageResponse;
import com.ai.chatsdk.common.entity.account.ChatSdkAccount;
import com.ai.chatsdk.common.entity.session.RecordData;
import com.ai.entity.SessionRecordModel;

import java.util.List;

public interface ChatResponseSyncDataService {

    /**
     * 同步持久化对话响应数据
     *
     * @param response
     * @param list
     */
    List<SessionRecordModel> syncChatResponse(ChatSdkAccount requestParam, ChatSdkStorageResponse response, List<RecordData> list);

}
