package com.ai.chatsdk.common.manager.accessToken.entity;

import lombok.Data;

@Data
public class AccessTokenBean {

    /**
     * 唯一键
     */
    private String uniqueKey;
    /**
     * accessToken
     */
    private String accessToken;
    /**
     * 过期时间
     */
    private long expiresTime;
    /**
     * 刷新token时间
     */
    private long refreshTime;
    /**
     * refresh Token
     */
    private String refreshToken;

}
