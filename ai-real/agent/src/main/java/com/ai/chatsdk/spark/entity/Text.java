package com.ai.chatsdk.spark.entity;

import lombok.Data;

@Data
public class Text {

    String role;

    String content;

}
