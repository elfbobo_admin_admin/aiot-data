package com.ai.chatsdk.common.utils;

import cn.hutool.core.util.StrUtil;

public class ContentUtil {

    public final static String lp = "↖";
    public final static String rp = "↘";

    public static String convertNormal(String content) {
        if (StrUtil.isEmpty(content)) {
            return content;
        }
        content = content.replace(" ", String.format("%semsp%s", lp, rp));
        content = content.replace("\n", String.format("%sbr%s", lp, rp));
        return content;
    }

}
