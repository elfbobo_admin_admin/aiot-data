package com.ai.chatsdk.spark.entity.request;

import com.ai.chatsdk.spark.entity.Text;
import lombok.Data;

import java.util.List;

@Data
public class Payload {

    private Message message;

    @Data
    public static class Message {
        private List<Text> text;
    }

}
