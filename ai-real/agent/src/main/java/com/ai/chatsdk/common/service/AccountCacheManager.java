package com.ai.chatsdk.common.service;

public interface AccountCacheManager {

    /**
     * 清除缓存
     *
     * @param key
     */
    void cacheRemove(String key);

    void cacheAll();

}
