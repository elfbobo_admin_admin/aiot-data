package com.ai.chatsdk.baidu.entity.request;

import lombok.Data;

import java.util.List;

@Data
public class ChatRequest {

    private List<Message> messages;

    private Boolean stream;

    private Double temperature;

    private Double top_p;

    private Double penalty_score;

    private String system;

    private String user_id;



}
