package com.ai.chatsdk.common.manager.accessToken;

import cn.hutool.core.util.StrUtil;
import com.ai.chatsdk.common.manager.accessToken.entity.AccessTokenBean;

import java.util.Date;
import java.util.Objects;

import com.ai.common.utils.RedisUtil;
import net.srt.framework.common.utils.SpringUtils;
import com.ai.common.exception.BusinessException;

public abstract class AccessTokenManager {

    public final static String CACHE_KEY = "ACCESS_TOKEN:";

    protected RedisUtil redisUtil;

    public AccessTokenManager() {
        this.redisUtil = SpringUtils.getBean(RedisUtil.class);
    }

    public String buildCacheKey(String uniqueKey) {
        if (StrUtil.isEmpty(uniqueKey))
            throw new BusinessException("唯一键不能为空");
        return CACHE_KEY + uniqueKey;
    }

    public AccessTokenBean getAccessToken(String uniqueKey) {
        String key = this.buildCacheKey(uniqueKey);
        AccessTokenBean accessTokenBean = redisUtil.getCacheObject(key);
        if (Objects.isNull(accessTokenBean)) {
            accessTokenBean = this.clientGetAccessToken(uniqueKey);
        }
        if (Objects.nonNull(accessTokenBean)) {
            long nowTime = new Date().getTime();
            long expiresTime = accessTokenBean.getExpiresTime();
            long refreshTime = accessTokenBean.getRefreshTime();
            if (nowTime >= refreshTime || nowTime >= expiresTime) {
                accessTokenBean = this.clientGetAccessToken(uniqueKey);
            }
        }

        return accessTokenBean;
    }

    public String getAccessTokenString(String uniqueKey) {
        AccessTokenBean accessToken = this.getAccessToken(uniqueKey);
        if (Objects.isNull(accessToken))
            return null;
        return accessToken.getAccessToken();
    }

    protected abstract AccessTokenBean clientGetAccessToken(String uniqueKey);

}
