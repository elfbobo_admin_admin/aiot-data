package com.ai.chatsdk.common.entity.session;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import io.swagger.v3.oas.annotations.media.Schema;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RecordData {

    /**
     * 角色
     */
    @Schema(description = "角色", type = "String")
    private String role;
    /**
     * 内容
     */
    @Schema(description = "内容", type = "String")
    private String content;

    private Long customerToken;

    private StringBuilder contentSB = new StringBuilder();

}
