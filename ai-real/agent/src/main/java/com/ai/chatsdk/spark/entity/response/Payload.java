package com.ai.chatsdk.spark.entity.response;

import lombok.Data;

@Data
public class Payload {
    private Choices choices;
}