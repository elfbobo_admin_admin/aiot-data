package com.ai.session.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.ai.entity.SessionInfoDrawModel;
import com.ai.entity.SessionRecordDrawModel;

import java.util.List;

/**
 * 绘图会话详情实体
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SessionDrawDetailVO extends SessionInfoDrawModel {

    private List<SessionRecordDrawModel> recordDrawList;

}
