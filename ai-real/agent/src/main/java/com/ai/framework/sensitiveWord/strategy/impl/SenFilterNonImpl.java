package com.ai.framework.sensitiveWord.strategy.impl;

import com.ai.framework.sensitiveWord.constants.SenWordFilterType;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class SenFilterNonImpl extends SenFilterCommonImpl {

    @Override
    public SenWordFilterType type() {
        return SenWordFilterType.non;
    }

    /**
     * 处理字符串类型值
     *
     * @param value
     * @return
     */
    @Override
    protected String handleString(String value) {
        return value;
    }

}
