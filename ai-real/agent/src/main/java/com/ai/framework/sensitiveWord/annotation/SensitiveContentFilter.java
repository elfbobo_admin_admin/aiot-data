package com.ai.framework.sensitiveWord.annotation;

import com.ai.framework.sensitiveWord.constants.SenWordFilterType;

import java.lang.annotation.*;

/**
 * 敏感词过滤注解
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SensitiveContentFilter {

    /**
     * 入口过滤类型
     *
     * @return
     */
    SenWordFilterType type() default SenWordFilterType.error;

    /**
     * 出口过滤类型
     *
     * @return
     */
    SenWordFilterType resultType() default SenWordFilterType.replace;

    /**
     * 替换字符
     *
     * @return
     */
    String replaceVal() default "*";

    /**
     * 字段值
     *
     * @return
     */
    String attrName();

}
