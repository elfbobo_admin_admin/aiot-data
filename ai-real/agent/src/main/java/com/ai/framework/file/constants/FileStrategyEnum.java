package com.ai.framework.file.constants;

/**
 * 文件操作策略常量
 */
public enum FileStrategyEnum {

    local,

    server,

    qiniu,

    // 其他.....

}
