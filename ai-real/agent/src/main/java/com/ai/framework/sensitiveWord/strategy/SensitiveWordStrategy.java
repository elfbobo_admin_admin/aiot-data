package com.ai.framework.sensitiveWord.strategy;

import com.ai.framework.sensitiveWord.constants.SenWordFilterType;


public interface SensitiveWordStrategy {

    String commonErrorText = "维护社区网络环境，请不要出现带有敏感政治、暴力倾向、不健康色彩的内容嗷~~";

    SenWordFilterType type();

    /**
     * 默认的替换方法
     *
     * @param value
     * @return
     */
    String defaultReplaceValue(String value,String replaceValue);

}
