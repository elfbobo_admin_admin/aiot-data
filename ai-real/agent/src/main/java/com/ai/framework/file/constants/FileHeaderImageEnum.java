package com.ai.framework.file.constants;

import com.ai.common.enums.FlowBaseEnum;
import lombok.AllArgsConstructor;


@AllArgsConstructor
public enum FileHeaderImageEnum implements FlowBaseEnum {

    IMAGE_PNG("image/png", ".png"),
    IMAGE_JPG("image/jpg", ".jpg"),
    IMAGE_JPEG("image/jpeg", ".jpeg"),
    IMAGE_BMP("image/bmp", ".bmp"),
    IMAGE_GIF("image/gif", ".gif"),
    ;

    private final String key;

    private final String value;

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getValue() {
        return value;
    }

}
