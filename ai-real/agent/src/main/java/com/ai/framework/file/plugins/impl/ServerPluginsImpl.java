package com.ai.framework.file.plugins.impl;

import com.ai.framework.file.constants.FileStrategyEnum;
import com.ai.framework.file.plugins.AbstractPluginsImpl;
import org.springframework.stereotype.Service;

/**
 * 插件 win 策略实现
 */
@Service
public class ServerPluginsImpl extends AbstractPluginsImpl {

    /**
     * 策略标识
     *
     * @return
     */
    @Override
    public FileStrategyEnum strategy() {
        return FileStrategyEnum.server;
    }

}
