package com.ai.framework.file.service;


import com.ai.entity.SysFileConfigModel;

public interface FileConfigCacheConfig {

    /**
     * 设置sysFileConfigModel
     *
     * @param fileConfig
     */
    void setFileConfig(SysFileConfigModel fileConfig);

    /**
     * 获取配置
     *
     * @return
     */
    SysFileConfigModel getFileConfig();

}
