package com.ai.framework.file.plugins;


import com.ai.framework.file.constants.FileStrategyEnum;

/**
 * 插件接口
 */
public interface Plugins {

    /**
     * 策略标识
     *
     * @return
     */
    FileStrategyEnum strategy();

}
