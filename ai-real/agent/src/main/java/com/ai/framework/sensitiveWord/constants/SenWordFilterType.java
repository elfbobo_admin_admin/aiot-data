package com.ai.framework.sensitiveWord.constants;


public enum SenWordFilterType {

    /**
     * 替换
     */
    replace,

    /**
     * 抛异常
     */
    error,

    /**
     * 不做动作
     */
    non,

}
