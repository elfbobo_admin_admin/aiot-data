package com.ai.framework.asyncMessage;

import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson2.JSON;
import com.ai.common.enums.ChannelEnum;

/**
 * 异步消息发送接口
 */
public interface MessageService {

    /**
     * send发送消息
     *
     * @param channel 通道id
     */
    void send(ChannelEnum channel, String data);

    default <T> void send(ChannelEnum channel, T data) {

        Assert.notNull(data);

        this.send(channel, JSON.toJSONString(data));
    }

}
