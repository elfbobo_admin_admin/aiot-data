package com.ai.framework.sensitiveWord.strategy.impl;

import com.ai.framework.sensitiveWord.constants.SenWordFilterType;
import com.ai.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class SenFilterErrorImpl extends SenFilterCommonImpl {

    @Override
    public SenWordFilterType type() {
        return SenWordFilterType.error;
    }

    /**
     * 处理字符串类型值
     *
     * @param value
     * @return
     */
    @Override
    protected String handleString(String value) {
        if (super.senWordHolder.exists(value)) {
            throw new BusinessException(commonErrorText);
        }
        return value;
    }

}
