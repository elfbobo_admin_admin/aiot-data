package com.ai.controller;

import com.ai.common.constants.ApiPrefixConstant;
import com.ai.entity.BaseResourceConfigEntity;
import com.ai.service.IBaseResourceConfigService;
import com.ai.vo.BaseResourceConfigVo;
import com.ai.vo.ResourceMainVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import com.ai.common.constants.ResourceConfigConstant;
import net.srt.framework.common.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(ApiPrefixConstant.Modules.SYSTEM + "/baseresourceconfig")
@Tag(name = "agent资源配置")
@Slf4j
public class BaseResourceConfigController {

    @Autowired
    private IBaseResourceConfigService service;

    @Operation(summary = "单独获取main配置信息", method = "GET")
    @GetMapping("/configMain")
    public Result getConfigMain() {
        BaseResourceConfigEntity model = service.queryByConfigKey(ResourceConfigConstant.MAIN_KEY);
        return Result.ok(model);
    }

    @Operation(summary = "给客户端需要的配置数据")
    @GetMapping("/clientConfig")
    public Result getClientConfig() {
        ResourceMainVO vo = service.getResourceMain();
        Map<String, ResourceMainVO> resourceMain = new HashMap<String, ResourceMainVO>();
        resourceMain.put("resourceMain", vo);
        return Result.ok(resourceMain);
    }

    @Operation(summary = "根据configKey获取信息", method = "GET" )
    @GetMapping("/queryByConfigKey/{configKey}")
    public Result queryByConfigKey(@PathVariable String configKey) {
        BaseResourceConfigEntity model = service.queryByConfigKey(configKey);
        return Result.ok(model);
    }

    @Operation(summary = "根据configKey编辑信息", method = "PUT")
    @PutMapping("/editByConfigKey")
    public Result editByConfigKey(@RequestBody BaseResourceConfigEntity param) {
        if (!service.editByConfigKey(param))
            return Result.error();
        return Result.ok();
    }
}
