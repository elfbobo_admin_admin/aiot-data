package com.ai.controller;

import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson2.JSON;
import com.ai.common.constants.ApiPrefixConstant;
import com.ai.common.constants.Constants;
import com.ai.entity.DomainModel;
import com.ai.service.DomainService;
import com.ai.common.exception.BusinessException;
import com.ai.utils.OptionalUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import lombok.RequiredArgsConstructor;
import net.srt.framework.common.utils.Result;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(ApiPrefixConstant.Modules.SESSION + "/domain")
@Tag(name = "DomainModel", description = "领域会话")
public class DomainController extends BaseController<DomainService, DomainModel> {

    @Operation(summary = "获取场景对话的配置信息")
    @GetMapping("/getWindowData/{domainKey}")
    public Result getWindowData(@PathVariable String domainKey){
        DomainModel one = service.lambdaQuery()
                .eq(DomainModel::getUniqueKey, domainKey)
                .eq(DomainModel::getIfShow, Constants.BOOLEAN.TRUE)
                .one();
        Assert.notNull(one,() -> new BusinessException("不存在的场景会话类型"));
        String windowData = one.getWindowData();
        return Result.ok(JSON.parseObject(windowData));
    }

    @Override
    protected Result baseQueryByParam(@RequestBody DomainModel param) {
        List<DomainModel> list = listByParam(param);
        OptionalUtil.ofNullList(list).forEach(item -> {
            item.setAboveContent(null);
            item.setFirstContent(null);
        });
        return Result.ok(list);
    }
}
