package com.ai.controller;

import com.ai.common.constants.ApiPrefixConstant;
import com.ai.entity.CmjChannelConfigModel;
import com.ai.service.ICmjChannelConfigService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * mj频道配置 控制器
 */
@RestController
@RequestMapping(ApiPrefixConstant.Modules.CONFIG + "/cmjchannelconfig")
@Tag(name = "mj频道配置")
public class CmjChannelController extends BaseController<ICmjChannelConfigService, CmjChannelConfigModel> {

}
