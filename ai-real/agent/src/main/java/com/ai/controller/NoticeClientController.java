package com.ai.controller;

import com.ai.common.constants.ApiPrefixConstant;
import com.ai.entity.NoticeClientModel;
import com.ai.service.NoticeClientService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import lombok.RequiredArgsConstructor;
import net.srt.framework.common.utils.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 通知公告
 */
@RequiredArgsConstructor
@RestController
@RequestMapping(ApiPrefixConstant.Modules.BUSINESS + "/noticeclient")
@Tag(name = "NoticeClient", description = "通知公告")
public class NoticeClientController extends BaseController<NoticeClientService, NoticeClientModel> {

    @Operation(summary = "获取最新的一条通告信息")
    @GetMapping("/getLastNotice")
    public Result getLastNotice() {
        NoticeClientModel one = service.lambdaQuery()
                .eq(NoticeClientModel::getNoticeType,"NOTIFY")
                .orderByAsc(NoticeClientModel::getSort).last("limit 1")
                .one();
        return Result.ok(one);
    }

}
