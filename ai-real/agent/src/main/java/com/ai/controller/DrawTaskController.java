package com.ai.controller;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.ai.common.QueryFastLambda;
import com.ai.common.enums.ResultEnum;
import com.ai.utils.ExtMapperUtil;
import com.ai.utils.FlowEnumUtils;
import com.ai.utils.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.ai.common.constants.ApiPrefixConstant;
import com.ai.core.drawTask.enums.DrawType;
import com.ai.core.drawTask.manager.DrawTaskDataManager;
import com.ai.core.drawTask.manager.queue.DrawTaskMjQueueManager;
import com.ai.core.drawTask.manager.queue.DrawTaskOpenaiQueueManager;
import com.ai.core.drawTask.manager.queue.DrawTaskSdQueueManager;
import com.ai.dto.TaskDrawDTO;

import com.ai.entity.draw.TaskDrawModel;
import com.ai.service.TaskDrawService;
import com.ai.utils.PageUtil;

import net.srt.framework.common.utils.Result;
import net.srt.framework.common.utils.SpringUtils;

import com.ai.common.exception.BusinessException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.srt.framework.security.user.SecurityUser;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(ApiPrefixConstant.Modules.DRAW + "/task")
@Tag(name = "绘画控制器")
public class DrawTaskController {

    private final TaskDrawService taskDrawService;

    @Operation(summary = "获取任务明细")
    @GetMapping("/getTaskDetail/{drawType}")
    public Result getTaskList(@PathVariable String drawType) {
        DrawType drawTypeEnum = FlowEnumUtils.getEnumByKey(drawType.toUpperCase(Locale.ROOT), DrawType.class);
        Assert.notNull(drawTypeEnum, () -> new BusinessException("不匹配的绘图类型"));

        int runningCount = 0;
        int sum = 0;
        switch (drawTypeEnum) {
            case OPENAI -> {
                DrawTaskDataManager queueManager = SpringUtils.getBean(DrawTaskOpenaiQueueManager.class);
                runningCount = queueManager.getRunningCount();
                sum = queueManager.getWaitTime();
            }
            case SD -> {
                DrawTaskDataManager queueManager = SpringUtils.getBean(DrawTaskSdQueueManager.class);
                runningCount = queueManager.getRunningCount();
                sum = queueManager.getWaitTime();
            }
            case MJ -> {
                DrawTaskDataManager queueManager = SpringUtils.getBean(DrawTaskMjQueueManager.class);
                runningCount = queueManager.getRunningCount();
                sum = queueManager.getWaitTime();
            }
        }

        Map rest = new HashMap<>();
        rest.put("runningCount", runningCount);
        rest.put("sum", sum);

        return Result.ok(rest);
    }

    @Operation(summary = "获取用户绘图任务列表")
    @GetMapping("/userTaskList/{drawType}")
    public Result userTaskList(@PathVariable String drawType, TaskDrawModel param) {
        param.setDrawType(drawType);
        param.setUserId(SecurityUser.getUserId());

        QueryFastLambda.build(param).sortCondition(TaskDrawModel::getCreateTime, false);

        IPage<TaskDrawModel> page = taskDrawService.page(PageUtil.pageBean(param), ExtMapperUtil.modelToWrapper(param));


        return Result.ok(page);
    }

    @Operation(summary = "创建绘图任务")
    @PostMapping("/createTask/{apiKey}")
    public Result createTask(@PathVariable String apiKey, @RequestBody HashMap<String, Object> paramMap) {
        String originalTaskDrawId = (String) paramMap.get("originalTaskDrawId");
        taskDrawService.createTask(apiKey, StrUtil.isEmpty(originalTaskDrawId) ? null : Long.valueOf(originalTaskDrawId), paramMap);
        return Result.ok();
    }

    @Operation(summary = "绘图任务分页查询")
    @PostMapping("/baseQueryPageByParam")
    public Result baseQueryPageByParam(@RequestBody TaskDrawDTO param) {
        IPage<TaskDrawModel> pageDto = taskDrawService.page(PageUtil.pageBean(param), ExtMapperUtil.modelToWrapper(param));
        return Result.ok(pageDto);
    }

    @DeleteMapping("/baseDeleteByIds/{ids}")
    @Operation(summary = "删除任务")
    public Result baseDeleteByIds(@PathVariable("ids") String ids) {
        if (StrUtil.isEmpty(ids)) {
            return Result.error("删除条件id不能为空");
        }
        String[] idsArr = ids.split(StringPool.COMMA);
        if (idsArr.length > 1000) {
            return Result.error("不能批量删除超过1000个数据");
        }
        List<Long> idList = StringUtils.splitToList(ids, Long::valueOf);
        if (taskDrawService.removeByIds(idList)) {
            return Result.ok(ResultEnum.SUCCESS_DELETE);
        }
        return Result.ok(ResultEnum.FAIL_DELETE);
    }
}
