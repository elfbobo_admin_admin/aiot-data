package com.ai.controller;

import cn.hutool.core.collection.CollUtil;
import com.ai.dto.SessionRecordDrawDTO;
import com.ai.utils.PageUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ai.common.constants.ApiPrefixConstant;

import com.ai.entity.SessionRecordDrawModel;
import com.ai.service.SessionRecordDrawService;

import com.ai.common.function.OR;
import com.ai.convert.SessionRecordDrawConvert;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import lombok.RequiredArgsConstructor;
import net.srt.api.module.system.SysUserApi;
import net.srt.api.module.system.dto.SysUserDTO;
import net.srt.framework.common.utils.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
@RequestMapping(ApiPrefixConstant.Modules.SESSION + "/sessionrecorddraw")
@Tag(name = "SessionRecordDrawModel", description = "绘图会话记录")
public class SessionRecordDrawController extends BaseController<SessionRecordDrawService, SessionRecordDrawModel> {

    private final SysUserApi sysUserApi;

    @Override
    protected Result baseQueryPageByParam(@RequestBody SessionRecordDrawModel param) {
        IPage<SessionRecordDrawModel> page = listPageByParam(PageUtil.pageBean(param), param);

        IPage<SessionRecordDrawDTO> dtoPage = PageUtil.convert(page, SessionRecordDrawConvert.INSTANCE::convertToDTO);
//        OR.run(dtoPage.getRecords(), CollUtil::isNotEmpty, list -> {
//            List<Long> userIds = list.stream().map(SessionRecordDrawDTO::getUserId).distinct().collect(Collectors.toList());
//            List<UserInfoModel> userList = userInfoService.lambdaQuery().in(UserInfoModel::getId, userIds).list();
//            list.forEach(item -> {
//                UserInfoModel userInfoModel = userList.stream().filter(user -> user.getId().equals(item.getUserId())).findFirst().orElseGet(UserInfoModel::new);
//                item.setUserName(userInfoModel.getUserName());
//            });
//        });
        OR.run(dtoPage.getRecords(), CollUtil::isNotEmpty, list -> {
            List<Long> userIds = list.stream().map(SessionRecordDrawDTO::getUserId).distinct().collect(Collectors.toList());
            try {
                List<SysUserDTO> userList = sysUserApi.listByIds(userIds);
                list.forEach(item -> {
                    SysUserDTO userInfoModel = userList.stream().filter(user -> user.getId().equals(item.getUserId())).findFirst().orElseGet(SysUserDTO::new);
                    item.setUserName(userInfoModel.getUsername());
                });
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        return Result.ok(dtoPage);
    }
}
