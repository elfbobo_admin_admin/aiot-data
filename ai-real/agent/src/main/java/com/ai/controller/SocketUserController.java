package com.ai.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.ai.common.constants.ApiPrefixConstant;

import com.ai.websocket.user.socket.manager.UserSocketGlobalData;
import com.ai.websocket.endpoint.SocketPointUser;
import com.ai.websocket.pool.UserSocketPool;
import com.ai.common.function.OR;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.srt.framework.common.utils.Result;
import net.srt.framework.security.user.SecurityUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.Session;
import java.util.Map;
import java.util.Objects;


@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(ApiPrefixConstant.Modules.USER + "/connect")
@Tag(name = "用户连接管理", description = "用户连接管理")
public class SocketUserController {

    @GetMapping("/getOnlineCount")
    @Operation(summary = "获取当前在线人数")
    public Result getOnlineCount() {
        return Result.ok(UserSocketGlobalData.onlineAmount);
    }

    @GetMapping("/closeSocket/{sessionId}")
    @Operation(summary = "手动清除socket连接")
    public Result closeSocket(@PathVariable("sessionId") String sessionId) {
        OR.run(UserSocketPool.get(String.valueOf(SecurityUser.getUserId()), sessionId), Objects::nonNull, SocketPointUser::closeSession);
        log.info("手动清除socket连接 - sessionId: {}", sessionId);
        return Result.ok();
    }

    @GetMapping("/verify/{sessionId}")
    @Operation(summary = "验证socket连接")
    public Result verify(@PathVariable("sessionId") String sessionId) {
        if (StrUtil.isEmpty(sessionId)) {
            return Result.ok();
        }
        Long userId = SecurityUser.getUserId();
        Map<String, SocketPointUser> map = UserSocketPool.get(String.valueOf(userId));
        if (CollUtil.isEmpty(map)) {
            return Result.error();
        }
        if (!map.containsKey(sessionId)) {
            return Result.error();
        }
        SocketPointUser socketPointUser = map.get(sessionId);
        Session session = socketPointUser.getSession();
        if (!session.isOpen()) {
            return Result.error();
        }
        return Result.ok();
    }
}
