package com.ai.controller;

import cn.hutool.core.map.MapUtil;
import com.ai.common.constants.ApiPrefixConstant;
import com.ai.common.constants.Constants;
import com.ai.entity.ChatModelModel;
import com.ai.service.IChatModelService;
import com.ai.utils.OptionalUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import io.swagger.v3.oas.annotations.tags.Tag;
import net.srt.framework.common.utils.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 对话模型管理 控制器
 */
@RestController
@RequestMapping(ApiPrefixConstant.Modules.CONFIG + "/chatmodel")
@Tag(name = "对话模型管理")
public class ChatModelController extends BaseController<IChatModelService, ChatModelModel> {

    @Operation(summary = "获取下拉")
    @GetMapping("/getLabelOption")
    public Result getLabelOption() {
        List<ChatModelModel> allList = service.getAllList();
        List<Map<String, Object>> labelOptions = OptionalUtil.ofNullList(allList).stream()
                .filter(item -> Constants.EnableStatus.USABLE.equals(item.getEnableStatus()))
                .map(item -> {
                    HashMap<String, Object> map = MapUtil.newHashMap();
                    map.put("value", item.getId());
                    map.put("label", item.getModelDescription());
                    map.put("ifChatPlus", item.getIfPlusModel());
                    return map;
                }).collect(Collectors.toList());
        return Result.ok(labelOptions);
    }

    @Operation(summary = "刷新缓存")
    @GetMapping("/flushCache")
    public void flushCache() {
        service.flushCache();
    }


}
