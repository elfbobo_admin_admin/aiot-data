package com.ai.controller;

import cn.hutool.core.collection.CollUtil;
import com.ai.session.vo.SessionDrawDetailVO;
import com.ai.utils.FlowEnumUtils;
import com.alibaba.fastjson2.JSON;
import com.ai.common.constants.ApiPrefixConstant;
import com.ai.core.drawTask.enums.DrawType;
import com.ai.entity.SessionInfoDrawModel;
import com.ai.entity.SessionRecordDrawModel;

import com.ai.service.SessionInfoDrawService;
import com.ai.service.SessionRecordDrawService;

import com.ai.utils.OptionalUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import lombok.RequiredArgsConstructor;
import net.srt.framework.common.utils.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 会话表 控制器
 */
@RequiredArgsConstructor
@RestController
@RequestMapping(ApiPrefixConstant.Modules.SESSION + "/sessioninfodraw")
@Tag(name = "SessionnfoDrawModel", description = "绘图会话")
public class SessionInfoDrawController {

    private final SessionInfoDrawService service;

    private final SessionRecordDrawService sessionRecordDrawService;

    @GetMapping("/userLastDrawSession")
    @Operation(summary = "获取用户最新的画图会话")
    public Result userLastDrawSession(String drawUniqueKey) {
        return Result.ok(service.getLastSession(drawUniqueKey));
    }

    @Operation(summary = "获取会话详情")
    @GetMapping("/getSessionDetail/{id}")
    public Result getSessionDetail(@PathVariable Long id) {
        SessionInfoDrawModel sessionInfoDrawModel = service.cacheGetById(id);
        SessionDrawDetailVO detail = Optional.ofNullable(
                JSON.parseObject(JSON.toJSONString(sessionInfoDrawModel), SessionDrawDetailVO.class)
        ).orElseGet(SessionDrawDetailVO::new);

        List<SessionRecordDrawModel> sessionRecordDrawList = OptionalUtil.ofNullList(sessionRecordDrawService.cacheGetBySessionInfoId(id));

        DrawType drawTypeEnum = FlowEnumUtils.getEnumByKey(sessionInfoDrawModel.getDrawUniqueKey(), DrawType.class);
        if (drawTypeEnum.equals(DrawType.MJ)) {
            Long originalTaskDrawId = sessionInfoDrawModel.getOriginalTaskDrawId();
            List<String> originalTaskDrawIds = CollUtil.newArrayList(sessionInfoDrawModel.getTaskId(), String.valueOf(OptionalUtil.ofNullLong(originalTaskDrawId)));
            List<Long> recordIds = sessionRecordDrawList.stream().map(SessionRecordDrawModel::getId).distinct().toList();
            List<SessionRecordDrawModel> mjRecordModelList = sessionRecordDrawService.lambdaQuery()
                    .in(SessionRecordDrawModel::getOriginalTaskDrawId, originalTaskDrawIds)
                    .eq(SessionRecordDrawModel::getUserId, sessionInfoDrawModel.getUserId())
                    .list();
            sessionRecordDrawList.addAll(
                    OptionalUtil.ofNullList(mjRecordModelList).stream().filter(item -> !recordIds.contains(item.getId())).toList()
            );
        }
        detail.setRecordDrawList(sessionRecordDrawList.stream().filter(Objects::nonNull).collect(Collectors.toList()));
        return Result.ok(detail);
    }

}
