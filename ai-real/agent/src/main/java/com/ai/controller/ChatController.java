package com.ai.controller;

import com.ai.common.constants.ApiPrefixConstant;
import com.ai.common.constants.LockGroupConstant;
import com.ai.chatsdk.common.handler.MessageSendHandler;
import com.ai.chatsdk.common.handler.pool.SessionMessageSendPool;

import com.ai.common.exception.BusinessException;
import com.ai.core.chat.entity.ChatRequestParam;
import com.ai.core.chat.lock.ChatLockHandle;

import com.ai.core.context.UserThreadLocal;
import com.ai.websocket.user.socket.UserMessagePushUtil;
import com.ai.common.websocket.constants.ResultCode;

import com.ai.framework.sensitiveWord.annotation.SensitiveContentFilter;
import com.ai.service.ChatService;
import com.ai.common.function.OR;

//import org.redisson.api.RLock;

import com.ai.session.valid.ChatSend;
import com.ai.session.valid.SendDomain;
import com.ai.utils.ValidatorUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import net.srt.framework.common.utils.Result;
import net.srt.framework.security.user.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(ApiPrefixConstant.MODEL_API + "/chat")
@Tag(name = "聊天会话控制器")
public class ChatController {

    private final ChatService chatService;
    private final String lockErrorMessage = "当前会话正在进行中，请等待结束";

    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @SensitiveContentFilter(attrName = "content")
    @Operation(summary = "聊天会话消息发送")
    @PostMapping("/send")
    public Result send(@RequestBody ChatRequestParam param) {
        ValidatorUtil.validateEntity(param, ChatSend.class);

        param.setUserId(SecurityUser.getUserId());

        ChatLockHandle sessionLockHandle = ChatLockHandle.init(LockGroupConstant.SESSION, redisTemplate);
//        RLock lock = sessionLockHandle.getLock(param.getSessionType(), param.getSessionId());
//        if (lock.isLocked()) {
//            throw new BusinessException(lockErrorMessage);
//        }
        boolean lock = sessionLockHandle.getLock(param.getSessionType(), param.getSessionId());
        if (lock) {
            throw new BusinessException(lockErrorMessage);
        }

        taskExecutor.execute(() -> {
            try {
                UserThreadLocal.set(param.getUserId());
                sessionLockHandle.handle(param.getSessionType(), param.getSessionId(), () -> {
                    try {
                        chatService.sendChatMessage(param);
                    } catch (BusinessException e) {
                        e.printStackTrace();
                        UserMessagePushUtil.pushMessageString(String.valueOf(param.getUserId()), ResultCode.S_MESSAGE_ERROR, e.getMessage());
                    }
                }, lockErrorMessage);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                UserThreadLocal.remove();
            }
        });

        return Result.ok();
    }


    @SensitiveContentFilter(attrName = "content")
    @Operation(summary = "领域会话消息发送")
    @PostMapping("/sendDomain")
    public Result sendDomain(@RequestBody ChatRequestParam param) {
        ValidatorUtil.validateEntity(param, SendDomain.class);
        param.setUserId(SecurityUser.getUserId());

        ChatLockHandle sessionLockHandle = ChatLockHandle.init(LockGroupConstant.SESSION);
//        RLock lock = sessionLockHandle.getLock(param.getSessionType(), param.getSessionId(), param.getDomainUniqueKey());
//        if (lock.isLocked()) {
//            throw new BusinessException(lockErrorMessage);
//        }
        boolean lock = sessionLockHandle.getLock(param.getSessionType(), param.getSessionId(), param.getDomainUniqueKey());
        if (lock) {
            throw new BusinessException(lockErrorMessage);
        }
        taskExecutor.execute(() -> {
            try {
                UserThreadLocal.set(param.getUserId());
                sessionLockHandle.handle(param.getSessionType(), param.getSessionId(), param.getDomainUniqueKey(), () -> {
                    try {
                        chatService.sendChatMessage(param);
                    } catch (BusinessException e) {
                        e.printStackTrace();
                        UserMessagePushUtil.pushMessageString(String.valueOf(param.getUserId()), ResultCode.S_MESSAGE_ERROR, e.getMessage());
                    }
                }, lockErrorMessage);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                UserThreadLocal.remove();
            }
        });

        return Result.ok();
    }

    @Operation(summary = "中断流式输出")
    @GetMapping("/stopStreamResponse/{contentId}")
    public Result stopStreamResponse(@PathVariable String contentId) {
        OR.run(SessionMessageSendPool.get(contentId), Objects::nonNull, MessageSendHandler::stop);
        return Result.ok();
    }

}
