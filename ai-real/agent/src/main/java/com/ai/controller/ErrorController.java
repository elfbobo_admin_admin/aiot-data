package com.ai.controller;

import net.srt.framework.common.utils.Result;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ErrorController {

    @RequestMapping("/error/print")
    public Result printError(HttpServletRequest request) {
        String errorMessage = (String) request.getAttribute("error.message");
        return Result.error(errorMessage);
    }

}
