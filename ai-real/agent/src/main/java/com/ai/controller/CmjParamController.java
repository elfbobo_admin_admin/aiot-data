package com.ai.controller;

import com.ai.common.constants.ApiPrefixConstant;

import com.ai.entity.CmjParamModel;
import com.ai.service.ICmjParamService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import net.srt.framework.common.utils.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * mj参数配置 控制器
 */
@RestController
@RequestMapping(ApiPrefixConstant.Modules.CONFIG + "/cmjparam")
@Tag(name = "CmjParamModel", description = "mj参数配置")
public class CmjParamController extends BaseController<ICmjParamService, CmjParamModel> {

    @Operation(summary = "刷新mj参数列表缓存")
    @GetMapping("/flushCache")
    public Result flushCache() {
        service.cacheClear();
        return Result.ok();
    }

}
