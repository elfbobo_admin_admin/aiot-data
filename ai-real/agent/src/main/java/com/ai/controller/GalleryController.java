package com.ai.controller;

import cn.hutool.core.collection.CollUtil;
import com.ai.utils.PageUtil;
import com.ai.vo.GalleryItemDataVO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ai.common.constants.ApiPrefixConstant;
import com.ai.common.constants.Constants;
import com.ai.entity.draw.GalleryCommonModel;

import com.ai.entity.SessionRecordDrawModel;

import com.ai.service.GalleryCommonService;
import com.ai.service.SessionInfoDrawService;
import com.ai.service.SessionRecordDrawService;

import com.ai.common.function.OR;
import com.ai.utils.OptionalUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.srt.framework.common.utils.Result;
import net.srt.framework.security.user.SecurityUser;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;


@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(ApiPrefixConstant.Modules.DRAW + "/gallery")
@Tag(name = "画廊", description = "画廊")
public class GalleryController {

    private final SessionRecordDrawService sessionRecordDrawService;

    private final SessionInfoDrawService sessionInfoDrawService;

    private final GalleryCommonService galleryCommonService;

//    private final UserInfoService userInfoService;

    @Operation(summary = "设置图片公开")
    @PostMapping("/setDrawCommon")
    public Result setDrawCommon(@RequestBody GalleryCommonModel param) {
        GalleryCommonModel galleryCommonModel = galleryCommonService.setDrawCommon(param);
        return Result.ok(galleryCommonModel);
    }

    @Operation(summary = "取消图片公开")
    @PostMapping("/cancelDrawCommon")
    public Result cancelDrawCommon(@RequestBody GalleryCommonModel param) {
        galleryCommonService.removeDrawCommon(param);
        return Result.ok();
    }

    @PostMapping("/commonGallery")
    @Operation(summary = "公开画廊")
    public Result commonGallery(@RequestBody SessionRecordDrawModel param) {
        IPage<SessionRecordDrawModel> page = galleryCommonService.queryCommonSessionRecord(param);
        IPage<GalleryItemDataVO> resultData = PageUtil.convert(page, this::handlerRecordConvertGalleyShowData);
        return Result.ok(resultData);
    }

    @PostMapping("/ownerGallery")
    @Operation(summary = "我的画廊(过滤mj的缩略图)")
    public Result getOwnerGallery(@RequestBody SessionRecordDrawModel param) {
        return getOwnerGallery(null, param);
    }

    @PostMapping("/ownerGallery/{userId}")
    @Operation(summary = "我的画廊")
    public Result getOwnerGallery(@PathVariable(required = false, value = "userId") Long userId, @RequestBody SessionRecordDrawModel param) {
        if (Objects.isNull(userId)) {
            userId = SecurityUser.getUserId();
        }
        LambdaQueryWrapper<SessionRecordDrawModel> wrapper = Wrappers.lambdaQuery();
        wrapper
                .eq(SessionRecordDrawModel::getUserId, userId)
                .orderByDesc(SessionRecordDrawModel::getCreateTime)
        ;
        IPage<SessionRecordDrawModel> page = sessionRecordDrawService.page(PageUtil.pageBean(param), wrapper);

        IPage<GalleryItemDataVO> resultData = PageUtil.convert(page, this::handlerRecordConvertGalleyShowData);

        OR.run(page.getRecords(), CollUtil::isNotEmpty, records -> {
            List<Long> sessionRecordDrawIds = records.stream().map(SessionRecordDrawModel::getId).distinct().toList();
            List<GalleryCommonModel> galleryCommonModels = OptionalUtil.ofNullList(
                    galleryCommonService.lambdaQuery().select(GalleryCommonModel::getId, GalleryCommonModel::getSessionRecordDrawId).in(GalleryCommonModel::getSessionRecordDrawId, sessionRecordDrawIds).list()
            );

            resultData.getRecords().forEach(item -> {
                GalleryCommonModel galleryCommonModel = galleryCommonModels.stream().filter(e -> e.getSessionRecordDrawId().equals(item.getId())).findFirst().orElse(null);
                item.setIfCommon(Constants.BOOLEAN.FALSE);
                if (Objects.nonNull(galleryCommonModel)) {
                    item.setIfCommon(Constants.BOOLEAN.TRUE);
                    item.setGalleryCommonId(galleryCommonModel.getId());
                }
            });

        });
        return Result.ok(resultData);
    }

    /**
     * 转换画廊展示数据
     *
     * @param recordDrawList
     * @return
     */
    public List<GalleryItemDataVO> handlerRecordConvertGalleyShowData(List<SessionRecordDrawModel> recordDrawList) {
        if (CollUtil.isEmpty(recordDrawList))
            return CollUtil.newArrayList();

        List<Long> userIds = recordDrawList.stream().map(SessionRecordDrawModel::getUserId).filter(Objects::nonNull).distinct().toList();

//        List<UserInfoModel> userInfoList = OptionalUtil.ofNullList(userInfoService.lambdaQuery().in(UserInfoModel::getId, userIds).list());
//        return recordDrawList.stream().map(item -> {
//            Long userId = item.getUserId();
//            UserInfoModel userInfoModel = userInfoList.stream().filter(userItem -> userItem.getId().equals(userId)).findFirst().orElseGet(UserInfoModel::new);
//            GalleryItemDataVO showVO = new GalleryItemDataVO();
//            showVO.setId(item.getId());
//            showVO.setUserId(userId);
//            showVO.setUserName(userInfoModel.getNickName());
//            showVO.setUserImgUrl(userInfoModel.getImgUrl());
//            showVO.setEmail(userInfoModel.getEmail());
//            showVO.setSessionInfoDrawId(item.getSessionInfoDrawId());
//            showVO.setSessionRecordDrawId(item.getId());
//            showVO.setPrompt(item.getPrompt());
//            showVO.setImgUrl(item.getDrawImgUrl());
//            return showVO;
//        }).toList();
        return null;
    }

}
