package com.ai.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.ai.common.QueryFastLambda;
import com.ai.dto.SessionRecordDTO;
import com.ai.utils.PageUtil;
import com.ai.utils.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ai.common.constants.ApiPrefixConstant;
import com.ai.common.constants.Constants;

import com.ai.entity.SessionRecordModel;
import com.ai.service.SessionRecordService;

import com.ai.common.exception.BusinessException;
import com.ai.utils.OptionalUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.srt.framework.common.utils.Result;
import net.srt.framework.security.user.SecurityUser;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 会话详情 控制器
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(ApiPrefixConstant.Modules.SESSION + "/sessionrecord")
@Tag(name = "SessionRecordModel", description = "会话详情")
public class SessionRecordController extends BaseController<SessionRecordService, SessionRecordModel> {

    private final SessionRecordService service;

    @Operation(summary = "获取会话（可分页）")
    @GetMapping("/getPageRecordSession")
    public Result getPageRecordSession(SessionRecordDTO param) {
        Long sessionId = param.getSessionId();
        Assert.notNull(sessionId, () -> new BusinessException("会话号为空"));

        QueryFastLambda.build(param).sortCondition(SessionRecordDTO::getId, true);
        IPage<SessionRecordModel> page = listPageByParam(PageUtil.pageBean(param), param);
        return Result.ok(page);
    }

    @Operation(summary = "获取会话（不分页）")
    @GetMapping("/getRecordSession")
    public Result getRecordSession(SessionRecordDTO param) {
        Long sessionId = param.getSessionId();
        Assert.notNull(sessionId, () -> new BusinessException("会话号为空"));

        List<SessionRecordModel> result = OptionalUtil.ofNullList(service.cacheGetListBySessionId(sessionId)).stream()
                .filter(item -> Constants.BOOLEAN.TRUE.equals(item.getIfShow()))
                .sorted(Comparator.comparing(SessionRecordModel::getId))
                .collect(Collectors.toList());
        return Result.ok(result);
    }

    @Operation(summary = "删除一条会话记录")
    @GetMapping("/removeSessionRecord")
    public Result removeSessionRecord(@RequestParam(value = "sessionId") String sessionId, @RequestParam(value = "sessionRecordIds") String sessionRecordIds) {
        if (StrUtil.isEmpty(sessionRecordIds))
            return Result.error("删除失败，请求参数为空");
        List<Long> sessionRecordIdList = StringUtils.splitToList(sessionRecordIds, Long::valueOf);
        Long userId = SecurityUser.getUserId();
        // 删除
        service.cacheDeleteRecord(CollUtil.newArrayList(Long.valueOf(sessionId)));
        service.lambdaUpdate().eq(SessionRecordModel::getUserId, userId).in(SessionRecordModel::getId, sessionRecordIdList).remove();
        return Result.ok();
    }

}
