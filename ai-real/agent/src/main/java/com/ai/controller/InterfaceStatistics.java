package com.ai.controller;

import com.ai.common.constants.ApiPrefixConstant;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.ai.common.constants.RedisCacheKey.WebClientRequestCount;

import com.ai.common.utils.RedisUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RequiredArgsConstructor
@RestController
@RequestMapping(ApiPrefixConstant.Common.COMMON + "/interface")
@Tag(name = "接口统计")
public class InterfaceStatistics {

    private final RedisUtil redisUtil;

    @Operation(summary = "统计访问次数")
    @GetMapping("/info")
    public void incr() {
        redisUtil.incr(WebClientRequestCount);
    }

}
