package com.ai.controller;

import com.ai.common.constants.ApiPrefixConstant;
import com.ai.entity.ChatKeysModel;
import com.ai.service.IChatKeysService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 对话秘钥池 控制器
 */
@RestController
@RequestMapping(ApiPrefixConstant.Modules.CONFIG + "/chatkeys")
@Tag(name = "对话秘钥池")
public class ChatKeysController extends BaseController<IChatKeysService, ChatKeysModel> {

    @Operation(summary = "刷新缓存")
    @GetMapping("/flushCache")
    public void flushCache(){
        service.flushCache();
    }

}
