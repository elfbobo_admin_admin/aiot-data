package com.ai.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.ai.common.enums.ResultEnum;
import com.ai.utils.ExtMapperUtil;
import com.ai.utils.PageUtil;
import com.ai.utils.StringUtils;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;

import com.baomidou.mybatisplus.extension.service.IService;

import com.ai.core.BaseModel;

import net.srt.framework.common.utils.Result;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 公共控制器-基本增删改查
 */
public abstract class BaseController<S extends IService<M>, M extends BaseModel> {

    protected final Logger log = LoggerFactory.getLogger(BaseController.class);

    @Autowired
    protected S service;

    @ResponseBody
    @GetMapping("/baseQueryById/{id}")
    protected Result baseQueryById(@PathVariable("id") Long id) {
        if (ObjectUtil.isNull(id) || id <= 0L) {
            return Result.error("id不能为空！");
        }
        M m = service.getById(id);
        return Result.ok(m);
    }

    @ResponseBody
    @PostMapping("/baseQueryByParam")
    protected Result baseQueryByParam(@RequestBody(required = false) M param) {
        List<M> list = listByParam(param);
        return Result.ok(list);
    }

    @ResponseBody
    @PostMapping("/baseQueryPageByParam")
    protected Result baseQueryPageByParam(@RequestBody(required = false) M param) {
        IPage<M> page = PageUtil.pageBean(param);
        return Result.ok(listPageByParam(page, param));
    }

    @ResponseBody
    @PostMapping("/baseAdd")
    protected <DTO> Result baseAdd(@RequestBody DTO m) {
//        M param = JSON.parseObject(JSON.toJSONString(m), service.getModelClass());
//        if (!service.save(param)) {
//            return Result.error(ResultEnum.FAIL_INSERT);
//        }
//        return Result.ok(ResultEnum.SUCCESS_INSERT);
        return null;
    }

    @ResponseBody
    @PutMapping("/baseEdit")
    protected <DTO> Result baseEdit(@RequestBody DTO m) {
//        M param = JSON.parseObject(JSON.toJSONString(m), service.getModelClass());
//        if (!service.updateById(param)) {
//            return Result.error(ResultEnum.FAIL_UPDATE);
//        }
//        return Result.ok(ResultEnum.SUCCESS_UPDATE);
        return null;
    }

    @ResponseBody
    @DeleteMapping("/baseDeleteByIds/{ids}")
    protected Result baseDeleteByIds(@PathVariable("ids") String ids) {
        if (StrUtil.isEmpty(ids)) {
            return Result.error("删除条件id不能为空");
        }
        String[] idsArr = ids.split(StringPool.COMMA);
        if (idsArr.length > 1000) {
            return Result.error("不能批量删除超过1000个数据");
        }
        List<Long> idList = StringUtils.splitToList(ids, Long::valueOf);
        if (service.removeByIds(idList)) {
            return Result.ok();
        }
        return Result.error("删除失败");
    }

    /**
     * 查询列表通过Model
     *
     * @param param
     * @return
     */
    List<M> listByParam(M param) {
        if(param == null)
            return service.list();
        return service.list(ExtMapperUtil.modelToWrapper(param));
    }

    IPage<M> listPageByParam(IPage<M> page, M param) {
        return service.page(page, ExtMapperUtil.modelToWrapper(param));
    }

}
