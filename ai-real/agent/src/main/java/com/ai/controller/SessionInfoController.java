package com.ai.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.ai.common.QueryFastLambda;
import com.ai.convert.SessionInfoConvert;
import com.ai.dto.SessionInfoDTO;
import com.ai.session.valid.AddChatSession;
import com.ai.session.valid.AddDomainSession;
import com.ai.session.vo.SessionBaseRequest;
import com.ai.utils.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ai.common.constants.ApiPrefixConstant;

import com.ai.entity.SessionInfoModel;

import com.ai.service.SessionInfoService;

import com.ai.common.function.OR;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import lombok.RequiredArgsConstructor;
import net.srt.api.module.system.SysUserApi;
import net.srt.api.module.system.dto.SysUserDTO;
import net.srt.framework.common.utils.Result;
import net.srt.framework.security.user.SecurityUser;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 会话表 控制器
 */
@RequiredArgsConstructor
@RestController
@RequestMapping(ApiPrefixConstant.Modules.SESSION + "/sessioninfo")
@Tag(name = "SessionInfoModel", description = "会话表")
public class SessionInfoController {

    private final SessionInfoService service;

//    private final UserInfoService userInfoService;
    private final SysUserApi sysUserApi;

    @PostMapping("/addSession")
    @Operation(summary = "新增会话")
    public Result addSession(@RequestBody SessionBaseRequest param) {
        ValidatorUtil.validateEntity(param, AddChatSession.class);
        SessionInfoModel sessionInfoModel = service.addSession(param.getSessionType(), null, null);
        return Result.ok(sessionInfoModel);
    }


    @PostMapping("/addDomainSession")
    @Operation(summary = "新增领域会话")
    public Result addDomainSession(@RequestBody SessionBaseRequest param) {
        ValidatorUtil.validateEntity(param, AddDomainSession.class);
        SessionInfoModel sessionInfoModel = service.addSession(param.getSessionType(), param.getDomainUniqueKey(), param.getContent());
        return Result.ok(sessionInfoModel);
    }


    @DeleteMapping("/deleteSession/{sessionIds}")
    @Operation(summary = "删除会话")
    public Result deleteSession(@PathVariable String sessionIds) {
        List<String> list = StringUtils.splitToList(sessionIds, String::valueOf);
        service.removeByIds(list);
        return Result.ok();
    }

    @Operation(summary = "获取会话信息")
    @GetMapping("/getSessionInfo/{sessionId}")
    public Result getSessionInfo(@PathVariable Long sessionId) {
        SessionInfoModel sessionInfo = service.getById(sessionId);
        return Result.ok(sessionInfo);
    }

    @GetMapping("/userLastSession/{sessionType}")
    @Operation(summary = "获取用户最新的会话，根据sessionType")
    public Result userLastSession(@PathVariable String sessionType) {
        SessionInfoModel lastSession = service.userLastSession(sessionType);
        // 不存在就新建一条会话
        if (Objects.isNull(lastSession)) {
            SessionInfoModel sessionInfoModel = service.addSession(sessionType, null, null);
            return Result.ok(sessionInfoModel);
        }
        return Result.ok(lastSession);
    }

    @GetMapping("/userLastDomainSession")
    @Operation(summary = "获取用户最新的领域会话")
    public Result userLastDomainSession(String sessionType, String domainUniqueKey, String content) {
        SessionInfoModel lastSession = service.userLastDomainSession(sessionType, domainUniqueKey);
        // 不存在就新建一条会话
        if (Objects.isNull(lastSession)) {
            SessionInfoModel sessionInfoModel = service.addSession(sessionType, domainUniqueKey, content);
            return Result.ok(sessionInfoModel);
        }
        return Result.ok(lastSession);
    }


    @GetMapping("/clearSession/{sessionId}")
    @Operation(summary = "清空会话列表")
    public Result clearSession(@PathVariable Long sessionId) {
        service.clearSession(sessionId);
        return Result.ok();
    }

    @GetMapping("/getUserSessionList")
    @Operation(summary = "获取用户会话列表（分页）")
    public Result getUserSessionList(SessionInfoModel param) {
        SessionInfoModel queryParam = new SessionInfoModel();
        queryParam.setSize(param.getSize());
        queryParam.setPage(param.getPage());
        queryParam.setType(param.getType());
        queryParam.setDomainUniqueKey(param.getDomainUniqueKey());
        queryParam.setUserId(SecurityUser.getUserId());
        QueryFastLambda.build(queryParam).sortCondition(SessionInfoModel::getCreateTime, false);

        IPage<SessionInfoModel> page = service.page(PageUtil.pageBean(queryParam), ExtMapperUtil.modelToWrapper(queryParam));
        return Result.ok(page);
    }

    @Operation(summary = "管理端-列表分页")
    @PostMapping("/baseQueryPageByParam")
    public Result baseQueryPageByParam(@RequestBody SessionInfoDTO param) {
//        if (
//                StrUtil.isNotEmpty(param.getEmail()) ||
//                        StrUtil.isNotEmpty(param.getIfTourist()) ||
//                        StrUtil.isNotEmpty(param.getUserIpAddress()) ||
//                        StrUtil.isNotEmpty(param.getUserName())
//        ) {
//            List<UserInfoModel> userList = userInfoService.lambdaQuery()
//                    .select(UserInfoModel::getId)
//                    .eq(StrUtil.isNotEmpty(param.getEmail()), UserInfoModel::getEmail, param.getEmail())
//                    .like(StrUtil.isNotEmpty(param.getUserName()), UserInfoModel::getUserName, param.getUserName())
//                    .eq(StrUtil.isNotEmpty(param.getUserIpAddress()), UserInfoModel::getIpaddress, param.getUserIpAddress())
//                    .eq(StrUtil.isNotEmpty(param.getIfTourist()), UserInfoModel::getIfTourist, param.getIfTourist())
//                    .list();
//
//            List<Long> userIds = OptionalUtil.ofNullList(userList).stream().map(UserInfoModel::getId).filter(Objects::nonNull).distinct().collect(Collectors.toList());
//            QueryFastLambda.<SessionInfoModel>build(param).queryConditionIn(SessionInfoModel::getUserId, userIds);
//        }

        IPage<SessionInfoModel> modelPage = service.page(PageUtil.pageBean(param), ExtMapperUtil.modelToWrapper(param));
        IPage<SessionInfoDTO> page = PageUtil.convert(modelPage, SessionInfoConvert.INSTANCE::convertToDTO);

        OR.run(page.getRecords(), CollUtil::isNotEmpty, dtoPage -> {
            List<Long> userIds = dtoPage.stream().map(SessionInfoDTO::getUserId).filter(Objects::nonNull).distinct().collect(Collectors.toList());

//            List<UserInfoModel> userInfoList = OptionalUtil.ofNullList(
//                    userInfoService.lambdaQuery()
//                            .select(UserInfoModel::getUserName, UserInfoModel::getIpaddress, UserInfoModel::getEmail, UserInfoModel::getIfTourist, UserInfoModel::getId)
//                            .in(UserInfoModel::getId, userIds)
//                            .list()
//            );
//            dtoPage.forEach(item -> {
//                UserInfoModel userInfoModel = userInfoList.stream().filter(x -> x.getId().equals(item.getUserId())).findFirst().orElseGet(UserInfoModel::new);
//                item.setEmail(userInfoModel.getEmail());
//                item.setUserName(userInfoModel.getUserName());
//                item.setUserIpAddress(userInfoModel.getIpaddress());
//                item.setIfTourist(userInfoModel.getIfTourist());
//            });
            try {
                List<SysUserDTO> userdtos = sysUserApi.listByIds(userIds);
                dtoPage.forEach(item -> {
                    SysUserDTO userInfoModel = userdtos.stream().filter(x -> x.getId().equals(item.getUserId())).findFirst().orElseGet(SysUserDTO::new);
                    item.setEmail(userInfoModel.getEmail());
                    item.setUserName(userInfoModel.getUsername());
//                    item.setUserIpAddress(userInfoModel.getIpaddress());
//                    item.setIfTourist(userInfoModel.getIfTourist());
                });
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        });

        return Result.ok(page);
    }

}
