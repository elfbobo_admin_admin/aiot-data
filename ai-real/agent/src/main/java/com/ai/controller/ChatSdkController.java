package com.ai.controller;

import com.ai.common.constants.ApiPrefixConstant;
import com.ai.entity.ChatSdkModel;
import com.ai.service.IChatSdkService;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 对话第三方平台管理 控制器
 */
@RestController
@RequestMapping(ApiPrefixConstant.Modules.CONFIG + "/chatsdk")
@Tag(name = "对话第三方平台管理")
public class ChatSdkController extends BaseController<IChatSdkService, ChatSdkModel> {

}
