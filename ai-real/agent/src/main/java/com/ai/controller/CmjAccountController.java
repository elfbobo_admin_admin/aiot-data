package com.ai.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import com.ai.convert.CmjAccountConvert;
import com.ai.dto.CmjAccountDTO;
import com.ai.utils.PageUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ai.common.constants.ApiPrefixConstant;
import com.ai.common.enums.flow.AccountStatus;

import com.ai.entity.CmjAccountModel;
import com.ai.vo.CmjAccountDetailVO;
import com.ai.core.midjourney.client.DiscordSocketClient;
import com.ai.core.midjourney.common.entity.DiscordAccount;
import com.ai.core.midjourney.pool.DiscordAccountCacheObj;
import com.ai.core.midjourney.pool.DiscordSocketAccountPool;
import com.ai.service.ICmjAccountService;
import com.ai.common.function.OR;
import com.ai.common.exception.BusinessException;

import net.srt.framework.common.utils.Result;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * mj账户配置 控制器
 */
@RestController
@RequestMapping(ApiPrefixConstant.Modules.CONFIG + "/cmjaccount")
@Tag(name = "mj账户配置")
public class CmjAccountController extends BaseController<ICmjAccountService, CmjAccountModel> {

    @Operation(summary = "Discord账户重新连接")
    @GetMapping("/againConnect/{id}")
    public Result againConnect(@PathVariable Long id) {
        List<CmjAccountDetailVO> accountAll = service.getAccountAll();

        CmjAccountDetailVO accountDetailVO = accountAll.stream().filter(item -> id.equals(item.getId())).findFirst().orElse(null);
        Assert.notNull(accountDetailVO, () -> new BusinessException("未找到该账户"));

        Assert.isFalse(!AccountStatus.NORMAL.getKey().equals(accountDetailVO.getAccountStatus()), () -> new BusinessException("账户已停用"));

        DiscordAccountCacheObj discordAccountCacheObj = DiscordSocketAccountPool.get(accountDetailVO.getUserName());
        if (Objects.nonNull(discordAccountCacheObj)) {
            discordAccountCacheObj.getWebSocket().cancel();
        }
        DiscordSocketClient.getDiscordAccountMap().remove(accountDetailVO.getUserName());

        DiscordAccount discordAccount = DiscordSocketClient.buildAccount(accountDetailVO);
        DiscordSocketClient.connection(discordAccount);
        return Result.ok();
    }

    @Operation(summary = "Discord账户断开连接")
    @GetMapping("/closeConnect/{id}")
    public Result closeConnect(@PathVariable Long id) {
        CmjAccountModel model = service.getById(id);
        Assert.notNull(model, () -> new BusinessException("未找到账户"));

        List<CmjAccountDetailVO> accountAll = service.getAccountAll();

        CmjAccountDetailVO accountDetailVO = accountAll.stream().filter(item -> id.equals(item.getId())).findFirst().orElse(null);
        Assert.notNull(accountDetailVO, () -> new BusinessException("未找到该账户"));

        DiscordAccountCacheObj discordAccountCacheObj = DiscordSocketAccountPool.get(model.getUserName());
        if (Objects.nonNull(discordAccountCacheObj)) {
            discordAccountCacheObj.getWebSocket().cancel();
        }
        Map<String, DiscordAccount> discordAccountMap = DiscordSocketClient.getDiscordAccountMap();
        discordAccountMap.remove(accountDetailVO.getUserName());
        return Result.ok();
    }

    @Operation(summary = "刷新缓存")
    @GetMapping("/flushCache")
    public Result flushCache() {
        service.flushCache();
        return Result.ok();
    }

    @Override
    protected Result baseQueryPageByParam(@RequestBody CmjAccountModel param) {
        IPage<CmjAccountModel> page = super.listPageByParam(PageUtil.pageBean(param), param);
        IPage<CmjAccountDTO> dtoPage = PageUtil.convert(page, CmjAccountConvert.INSTANCE::convertToDTO);
        OR.run(dtoPage.getRecords(), CollUtil::isNotEmpty, list -> {
            list.forEach(item -> {
                item.setSocketStatus(0);
                OR.run(DiscordSocketAccountPool.get(item.getUserName()), Objects::nonNull, discordAccountCacheObj -> {
                    item.setSocketStatus(1);
                });
            });
        });
        return Result.ok(dtoPage);
    }
}
