package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ai.core.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 绘图会话 Model
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("tb_session_info_draw")
public class SessionInfoDrawModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    @Schema(description = "用户id", type = "Long")
    private Long userId;

    @Schema(description = "prompt")
    private String prompt;

    @Schema(description = "绘画接口类型唯一标识")
    private String drawUniqueKey;

    @Schema(description = "绘图接口标识")
    private String drawApiKey;

    @Schema(description = "展示图")
    private String showImg;

    @Schema(description = "底图")
    private String baseImg;

    @Schema(description = "sd info响应参数json")
    private String sdResponseInfo;

    @Schema(description = "mj原图地址")
    private String originalImgUrl;

    @Schema(description = "任务Id")
    private String taskId;

    @Schema(description = "原始任务ID")
    private Long originalTaskDrawId;

}
