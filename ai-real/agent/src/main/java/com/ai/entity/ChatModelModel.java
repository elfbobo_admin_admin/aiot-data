package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ai.core.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 对话模型管理 Model
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_chat_model")
public class ChatModelModel extends BaseModel {

	private static final long serialVersionUID = 1L;

	/**
	 * 关联ID
	 */
	@Schema(description = "关联ID", type = "Long")
	private Long chatSdkId;
	/**
	 * 唯一标识
	 */
	@Schema(description = "唯一标识", type = "String")
	private String uniqueKey;
	/**
	 * 模型描述
	 */
	@Schema(description = "模型描述", type = "String")
	private String modelDescription;
	/**
	 * 单次对话token限制
	 */
	@Schema(description = "单次对话token限制")
	private Integer onceToken;
	/**
	 * 最大请求token限制
	 */
	@Schema(description = "最大请求token限制")
	private Integer maxToken;
	/**
	 * model参数值
	 */
	@Schema(description = "model参数值")
	private String modelValue;

	/**
	 * 排序号
	 */
	private Integer sortNo;
	/**
	 * 是否增强模型
	 */
	private String ifPlusModel;
	/**
	 * 请求地址
	 */
	private String requestUrl;
	/**
	 * 可用状态
	 */
	@Schema(description = "可用状态", type = "String")
	private String enableStatus;

}
