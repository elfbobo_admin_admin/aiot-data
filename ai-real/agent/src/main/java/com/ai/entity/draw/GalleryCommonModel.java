package com.ai.entity.draw;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ai.core.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 公开画廊
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("tb_gallery_common")
public class GalleryCommonModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    @Schema(description = "用户ID")
    private Long userId;

    @Schema(description = "绘图会话ID")
    private Long sessionInfoDrawId;

    @Schema(description = "绘图会话详情ID")
    private Long sessionRecordDrawId;

    @Schema(description = "图片url")
    private String imgUrl;

    @Schema(description = "是否展示会话下所有")
    private String ifShowAll;

    @Schema(description = "提示词")
    private String prompt;

    @TableField(exist = false)
    private int delFlag;

    public int getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(int delFlag) {
        this.delFlag = delFlag;
    }

}
