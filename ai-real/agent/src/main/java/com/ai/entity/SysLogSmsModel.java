package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import com.ai.core.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_log_sms")
public class SysLogSmsModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 短信类型
     */
    @Schema(description = "短信类型", type = "String")
    private String smsType;
    /**
     * 短信类型描述
     */
    @Schema(description = "短信类型描述", type = "String")
    private String smsTypeDescription;
    /**
     * 唯一标识
     */
    @Schema(description = "唯一标识", type = "String")
    private String uniqueKey;
    /**
     * 内容
     */
    @Schema(description = "内容", type = "String")
    private String content;

}
