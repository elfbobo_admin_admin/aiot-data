package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ai.core.BaseModel;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * apikeys Model
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_openai_keys")
public class OpenaiKeysModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * openai key
     */
    @NotEmpty(message = "apiKey不能为空")
    @Schema(description = "openai key", type = "String")
    private String apiKey;
    /**
     * key名称
     */
    @Schema(description = "key名称", type = "String")
    private String name;
    /**
     * 总额度
     */
    @Schema(description = "总额度", type = "Double")
    private Double totalAmount;
    /**
     * 使用额度
     */
    @Schema(description = "使用额度", type = "Double")
    private Double totalUsage;
    /**
     * 过期时间
     */
    @Schema(description = "过期时间", type = "Date")
    private String expiredTime;
    /**
     * 是否通用key
     */
    @Schema(description = "是否通用key")
    private String ifCommon;
    /**
     * 用户id
     */
    @Schema(description = "用户id")
    private Long userId;
    /**
     * 可用状态
     */
    @Schema(description = "可用状态")
    private String enableStatus;

}
