package com.ai.entity;

import com.ai.core.BaseModel;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 附件管理
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("sys_attachment")
public class SysAttachmentModel extends BaseModel {

    @Schema(name = "业务主键id")
    private Long businessId;

    @Schema(name = "原始文件名")
    private String originalFileName;

    @Schema(name = "文件大小")
    private Long fileSize;

    @Schema(name = "文件唯一名称")
    private String fileNameMd5;

    @Schema(name = "文件绝对路径")
    private String fileAbsolutePath;

    @Schema(name = "文件后缀")
    private String fileSuffix;

}
