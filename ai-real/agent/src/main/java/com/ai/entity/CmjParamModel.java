package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ai.core.BaseModel;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * mj参数配置 Model
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_cmj_param")
public class CmjParamModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 唯一标识
     */
    @NotNull(message = "唯一标识")
    @Schema(description = "唯一标识", type = "String")
    private String uniqueKey;

    /**
     * 参数值
     */
    @NotNull(message = "参数值")
    @Schema(description = "参数值", type = "String")
    private String paramValue;

}
