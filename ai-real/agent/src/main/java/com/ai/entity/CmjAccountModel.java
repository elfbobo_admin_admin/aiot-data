package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ai.common.serializer.SensitiveStringSerializer;
import com.ai.core.BaseModel;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * mj账户配置 Model
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_cmj_account")
public class CmjAccountModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 账户名
     */
    @NotEmpty(message = "账户名不能为空")
    @Schema(description = "账户名", type = "String")
    private String userName;
    /**
     * token
     */
    @NotEmpty(message = "token不能为空")
    @JsonSerialize(using = SensitiveStringSerializer.class)
    @Schema(description = "token", type = "String")
    private String userToken;
    /**
     * ua
     */
    @NotEmpty(message = "userAgent不能为空")
    @Schema(description = "ua", type = "String")
    private String userAgent;
    /**
     * dataObject
     */
    @NotEmpty(message = "dataObject不能为空")
    @Schema(description = "dataObject", type = "String")
    private String dataObject;

    private String accountStatus;

    private String ifProxy;

}
