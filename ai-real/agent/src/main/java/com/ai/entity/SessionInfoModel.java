package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ai.core.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 会话表 Model
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("tb_session_info")
public class SessionInfoModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    @Schema(description = "用户id", type = "Long")
    private Long userId;

    @Schema(description = "类型")
    private String type;

    @Schema(description = "会话名称")
    private String sessionName;

    @Schema(description = "领域会话类型唯一标识")
    private String domainUniqueKey;

    @Schema(description = "会话状态", type = "String")
    private String status;

    @Schema(description = "总消耗token")
    private Integer allConsumerToken;

    @Schema(description = "最近一次使用的模型ID")
    private Long chatModelId;

}
