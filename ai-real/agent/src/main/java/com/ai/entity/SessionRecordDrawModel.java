package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ai.core.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 图像会话详情 Model
 */
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_session_record_draw")
public class SessionRecordDrawModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    @Schema(description = "绘图会话id")
    private Long sessionInfoDrawId;

    @Schema(description = "原始任务ID")
    private Long originalTaskDrawId;

    @Schema(description = "用户id", type = "Long")
    private Long userId;

    @Schema(description = "任务Id")
    private String taskId;

    @Schema(description = "绘画接口类型唯一标识")
    private String drawUniqueKey;

    @Schema(description = "绘图接口标识")
    private String drawApiKey;

    @Schema(description = "图片路径")
    private String drawImgUrl;

    @Schema(description = "底图")
    private String baseImg;

    @Schema(description = "prompt")
    private String prompt;

    @Schema(description = "副图1")
    private String assistantImg1;

    @Schema(description = "副图2")
    private String assistantImg2;

    @Schema(description = "原图地址url")
    private String originalImgUrl;

    @Schema(description = "mj扩展参数")
    private String mjExtendParam;

    @Schema(description = "mj u v下标")
    private Integer mjImageIndex;

    @Schema(description = "mj频道id")
    private String mjChannelId;

    @Schema(description = "mj服务器id")
    private String mjGuildId;

    @Schema(description = "mj帐号id")
    private String mjApplicationId;

}
