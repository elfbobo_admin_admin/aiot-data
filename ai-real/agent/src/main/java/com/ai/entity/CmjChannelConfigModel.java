package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ai.core.BaseModel;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * mj频道配置 Model
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_cmj_channel_config")
public class CmjChannelConfigModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 关联账户
     */
    @NotNull(message = "关联账户不能为空")
    @Schema(description = "关联账户", type = "Long")
    private Long cmjAccountId;
    /**
     * 服务器id
     */
    @NotEmpty(message = "服务器id不能为空")
    @Schema(description = "服务器id", type = "String")
    private String guildId;
    /**
     * 频道id
     */
    @NotEmpty(message = "频道id不能为空")
    @Schema(description = "频道id", type = "String")
    private String channelId;

}
