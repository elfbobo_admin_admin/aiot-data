package com.ai.entity;

import com.ai.core.BaseModel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.*;

import com.ai.common.serializer.SensitiveStringSerializer;

/**
 * 对话秘钥池 Model
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_chat_keys")
public class ChatKeysModel extends BaseModel {

	private static final long serialVersionUID = 1L;

	/**
	 * 关联ID
	 */
	@Schema(description = "关联ID", type = "Long")
	private Long chatSdkId;
	/**
	 * app_id
	 */
	@JsonSerialize(using = SensitiveStringSerializer.class)
	@Schema(description = "app_id", type = "String")
	private String appId;
	/**
	 * apiSecret
	 */
	@JsonSerialize(using = SensitiveStringSerializer.class)
	@Schema(description = "apiSecret", type = "String")
	private String apiSecret;
	/**
	 * apiToken
	 */
	@JsonSerialize(using = SensitiveStringSerializer.class)
	@Schema(description = "apiToken", type = "String")
	private String apiToken;
	/**
	 * 别名
	 */
	@Schema(description = "别名", type = "String")
	private String aliasName;
	/**
	 * 过期时间
	 */
	@Schema(description = "过期时间", type = "String")
	private String expiredTime;
	/**
	 * 可用状态
	 */
	@Schema(description = "可用状态", type = "String")
	private String enableStatus;
	/**
	 * 权重
	 */
	@Schema(description = "权重", type = "Integer")
	private Integer weightValue;

	private String remark;

}
