package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ai.core.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 对话第三方平台管理 Model
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_chat_sdk")
public class ChatSdkModel extends BaseModel {

	private static final long serialVersionUID = 1L;

	/**
	 * 唯一键
	 */
	@Schema(description = "唯一键", type = "String")
	private String uniqueKey;
	/**
	 * 平台名称
	 */
	@Schema(description = "平台名称", type = "String")
	private String sdkName;
	/**
	 * 可用状态
	 */
	@Schema(description = "可用状态", type = "String")
	private String enableStatus;
	/**
	 * key池获取规则
	 */
	@Schema(description = "key池获取规则", type = "String")
	private String keysRules;
	/**
	 * 是否开启镜像地址负载均衡
	 */
	@Schema(description = "是否开启镜像地址负载均衡")
	private String loadBalanceHost;

}
