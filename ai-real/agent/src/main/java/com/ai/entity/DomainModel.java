package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ai.core.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.NotEmpty;

/**
 * 领域会话
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("tb_domain")
public class DomainModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "唯一标识不能为空")
    @Schema(description = "唯一标识")
    private String uniqueKey;

    @NotEmpty(message = "上文内容不能为空")
    @Schema(description = "上文内容")
    private String aboveContent;

    @Schema(description = "排序号")
    private Integer sort;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "跳转路由（前端跳转）")
    private String routePath;

    @Schema(description = "显示名称")
    private String iconName;

    @Schema(description = "图片路径（本地）")
    private String iconPath;

    @Schema(description = "窗口会话数据json")
    private String windowData;

    @NotEmpty(message = "领域类型不能为空")
    @Schema(description = "领域类型（domain_type）")
    private String type;

    @Schema(description = "是否显示")
    private String ifShow;

    @Schema(description = "首次会话内容")
    private String firstContent;

    @Schema(description = "是否桌面显示")
    private String ifDeskShow;

}
