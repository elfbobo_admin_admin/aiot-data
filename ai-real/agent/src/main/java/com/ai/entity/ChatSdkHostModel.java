package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ai.core.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 对话镜像地址管理 Model
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_chat_sdk_host")
public class ChatSdkHostModel extends BaseModel {

	private static final long serialVersionUID = 1L;

	/**
	 * 关联ID
	 */
	@Schema(description = "关联ID", type = "Long")
	private Long chatSdkId;
	/**
	 * 请求地址
	 */
	@Schema(description = "请求地址", type = "String")
	private String hostUrl;
	/**
	 * 可用状态
	 */
	@Schema(description = "可用状态", type = "String")
	private String enableStatus;
	/**
	 * 是否默认
	 */
	@Schema(description = "是否默认", type = "String")
	private String ifDefault;
	/**
	 * 是否开启代理
	 */
	@Schema(description = "是否开启代理", type = "String")
	private String ifProxy;

	/**
	 * 请求超时时间（秒）
	 */
	@Schema(description = "请求超时时间（秒）")
	private Integer timeoutValue;
	/**
	 * 最大连接数
	 */
	@Schema(description = "最大连接数")
	private Integer maxConnect;
	/**
	 * 权重
	 */
	@Schema(description = "权重")
	private Integer weightValue;

}
