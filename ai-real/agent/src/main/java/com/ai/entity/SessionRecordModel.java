package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import com.ai.core.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 会话详情 Model
 */
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_session_record")
public class SessionRecordModel extends BaseModel {

    private static final long serialVersionUID = 1L;
    /**
     * 用户id
     */
    @Schema(description = "用户id", type = "Long")
    private Long userId;
    /**
     * 会话id
     */
    @Schema(description = "会话id", type = "String")
    private Long sessionId;

    @Schema(description = "领域类型唯一标识")
    private String domainUniqueKey;
    /**
     * 角色
     */
    @Schema(description = "角色", type = "String")
    private String role;
    /**
     * 内容
     */
    @Schema(description = "内容", type = "String")
    private String content;
    /**
     * 是否显示
     */
    @Schema(description = "是否显示", type = "String")
    private String ifShow;
    /**
     * 是否可统计为上下文 0 否 1 是
     */
    @Schema(description = "是否可统计为上下文 0 否 1 是", type = "String")
    private String ifContext;

    @Schema(description = "是否为领域会话的最上文", type = "String")
    private String ifDomainTop;

    @Schema(description = "消耗token数", type = "Integer")
    private Integer consumerToken;

    @Schema(description = "使用模型ID")
    private Long chatModelId;

    @Schema(description = "模型值")
    private String chatModelValue;

}
