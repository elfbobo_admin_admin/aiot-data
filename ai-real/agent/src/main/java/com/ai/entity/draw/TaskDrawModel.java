package com.ai.entity.draw;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ai.core.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.Date;

/**
 * 绘图任务
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("tb_task_draw")
public class TaskDrawModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    @Schema(description = "绘图任务类型")
    private String drawType;

    @Schema(description = "绘图接口标识")
    private String drawApiKey;

    @Schema(description = "用户id")
    private Long userId;

    @Schema(description = "任务状态")
    private String taskStatus;

    @Schema(description = "绘图会话id")
    private Long sessionInfoDrawId;

    @Schema(description = "原始任务ID")
    private Long originalTaskDrawId;

    @Schema(description = "任务结束时间")
    private Date taskEndTime;

    @Schema(description = "任务请求参数")
    private String requestParam;

    @Schema(description = "展示图")
    private String showImg;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "mj频道id")
    private String mjChannelId;

    @Schema(description = "mj服务器id")
    private String mjGuildId;

    @Schema(description = "mj帐号id")
    private String mjApplicationId;

}
