package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ai.core.BaseModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 通告信息 Model
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("tb_notice_client")
public class NoticeClientModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    @Schema(description = "通知标题")
    private String title;

    @Schema(description = "通知类型")
    private String noticeType;

    @Schema(description = "内容")
    private String content;

    @Schema(description = "排序")
    private Integer sort;

    @Schema(description = "是否显示")
    private String ifShow;

}
