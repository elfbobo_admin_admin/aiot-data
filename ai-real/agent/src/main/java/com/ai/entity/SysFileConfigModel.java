package com.ai.entity;

import com.ai.core.BaseModel;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_file_config")
public class SysFileConfigModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 策略标识
     */
    @Schema(name = "策略标识", type = "String")
    private String uniqueKey;
    /**
     * 保存路径
     */
    @Schema(name = "保存路径", type = "String")
    private String savePath;

}

