package com.ai.entity;

import com.ai.core.BaseModel;
import com.baomidou.mybatisplus.annotation.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("base_resource_config")
public class BaseResourceConfigEntity extends BaseModel {
    private static final long serialVersionUID = 1L;

    /**
     * 参数键
     */
    @Schema(name = "参数键", type = "String")
    private String configKey;
    /**
     * 资源值
     */
    @Schema(name = "资源值", type = "String")
    private String resourceValue;
}
