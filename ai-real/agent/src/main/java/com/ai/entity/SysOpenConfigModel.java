package com.ai.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import com.ai.core.BaseModel;

import com.ai.common.serializer.SensitiveStringSerializer;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 第三方配置 Model
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_open_config")
public class SysOpenConfigModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 策略标识
     */
    @Schema(description = "策略标识", type = "String")
    private String uniqueKey;
    /**
     * 标题
     */
    @Schema(description = "标题", type = "String")
    private String title;
    /**
     * appId
     */
    @JsonSerialize(using = SensitiveStringSerializer.class)
    @Schema(description = "appId", type = "String")
    private String appId;
    /**
     * accessKey
     */
    @JsonSerialize(using = SensitiveStringSerializer.class)
    @Schema(description = "accessKey", type = "String")
    private String accessKey;
    /**
     * secretKey
     */
    @JsonSerialize(using = SensitiveStringSerializer.class)
    @Schema(description = "secretKey ", type = "String")
    private String secretKey;
    /**
     * 存储空间名称
     */
    @Schema(description = "存储空间名称", type = "String")
    private String bucketName;
    /**
     * 数据处理服务域名
     */
    @Schema(description = "数据处理服务域名", type = "String")
    private String dataHandleDomain;
    /**
     * 表单数据
     */
    @Schema(description = "表单数据")
    private String formData;
    /**
     * 回调地址
     */
    @Schema(description = "回调地址")
    private String callbackUrl;
    /**
     * 商户ID
     */
    @JsonSerialize(using = SensitiveStringSerializer.class)
    @Schema(description = "商户ID")
    private String tenantId;

    @JsonSerialize(using = SensitiveStringSerializer.class)
    @Schema(description = "公钥")
    private String publicKey;

    @JsonSerialize(using = SensitiveStringSerializer.class)
    @Schema(description = "私钥")
    private String privateKey;

    @Schema(description = "网关")
    private String getawayUrl;

    @Schema(description = "同步跳转地址")
    private String returnUrl;

    @JsonSerialize(using = SensitiveStringSerializer.class)
    @Schema(description = "应用公钥")
    private String softwarePublicKey;
}
