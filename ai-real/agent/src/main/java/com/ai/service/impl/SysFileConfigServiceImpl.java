package com.ai.service.impl;

import com.ai.common.constants.RedisConstant;
import com.ai.common.support.init.InitCache;
import com.ai.common.utils.RedisUtil;
import com.ai.common.webApi.baseResource.BaseResourceWebApi;
import com.ai.dao.SysFileConfigMapper;
import com.ai.entity.SysFileConfigModel;

import com.ai.utils.ModelUtil;
import com.ai.utils.OptionalUtil;
import com.ai.vo.ResourceMainVO;
import net.srt.framework.mybatis.service.impl.BaseServiceImpl;

import com.ai.service.SysFileConfigService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * 文件上传配置 业务实现类
 */
@Service
public class SysFileConfigServiceImpl extends BaseServiceImpl<SysFileConfigMapper, SysFileConfigModel> implements SysFileConfigService, InitCache {

    private final String CACHE_KEY = ModelUtil.modelMainKey(SysFileConfigModel.class) + RedisConstant.Keyword.ALL_LIST;

    @Resource
    private BaseResourceWebApi baseResourceWebApi;
    @Resource
    private RedisUtil redisUtil;

    /**
     * 获取当前文件配置路径
     *
     * @return
     */
    @Override
    public String getFileConfigPath() {
        ResourceMainVO resourceMain = baseResourceWebApi.getResourceMain();
        String fileSaveStrategy = resourceMain.getFileSaveStrategy();
        SysFileConfigModel model = this.getAll().stream().filter(item -> item.getUniqueKey().equals(fileSaveStrategy)).findFirst().orElse(null);
        if (Objects.isNull(model)) return null;
        return model.getSavePath();
    }

    /**
     * 获取所有配置
     *
     * @return
     */
    @Override
    public List<SysFileConfigModel> getAll() {
        return redisUtil.getCacheList(CACHE_KEY);
    }

    /**
     * 初始化缓存
     */
    @Override
    public void runInitCache() {
        List<SysFileConfigModel> list = OptionalUtil.ofNullList(super.list());
        redisUtil.setCacheList(CACHE_KEY, list);
    }

    @Override
    public boolean save(SysFileConfigModel entity) {
        if (super.save(entity)) {
            this.runInitCache();
        }
        return true;
    }

    @Override
    public boolean updateById(SysFileConfigModel entity) {
        if (super.updateById(entity)) {
            this.runInitCache();
        }
        return true;
    }

    @Override
    public boolean removeByIds(Collection<?> list) {
        if (super.removeByIds(list)) {
            this.runInitCache();
        }
        return true;
    }
}

