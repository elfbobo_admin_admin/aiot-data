package com.ai.service;

import cn.hutool.core.util.StrUtil;

import com.ai.common.webApi.openCnofig.OpenConfigWebApi;
import com.ai.entity.SysOpenConfigModel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 第三方配置 业务接口
 */
public interface ISysOpenConfigService extends IService<SysOpenConfigModel>, OpenConfigWebApi {

    @Override
    default SysOpenConfigModel queryOpenConfigByUniqueKey(String key) {
        if (StrUtil.isEmpty(key))
            return null;
        return this.lambdaQuery().eq(SysOpenConfigModel::getUniqueKey, key).one();
    }
}
