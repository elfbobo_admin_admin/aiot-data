package com.ai.service;

import com.ai.entity.ChatSdkHostModel;
import net.srt.framework.mybatis.service.BaseService;

import java.util.List;

/**
 * 对话镜像地址管理 业务接口
 */
public interface IChatSdkHostService extends BaseService<ChatSdkHostModel> {

    /**
     * 通过chatSdkId获取列表
     *
     * @param chatSdkId
     * @return
     */
    List<ChatSdkHostModel> getUsableByChatSdkId(Long chatSdkId);

    void flushCache();

}
