package com.ai.service;

import com.ai.entity.DomainModel;
import net.srt.framework.mybatis.service.BaseService;

public interface DomainService extends BaseService<DomainModel> {

    /**
     * 通过唯一标识获取model
     * @param key
     * @return
     */
    DomainModel getByUniqueKey(String key);

}
