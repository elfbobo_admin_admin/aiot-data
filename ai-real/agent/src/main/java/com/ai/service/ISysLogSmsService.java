package com.ai.service;

import com.ai.entity.SysLogSmsModel;
import com.ai.common.webApi.log.SysLogSmsWebApi;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 验证码日志 业务接口
 */
public interface ISysLogSmsService extends IService<SysLogSmsModel>, SysLogSmsWebApi {

    @Override
    default void saveLog(SysLogSmsModel param) {
        save(param);
    }
}
