package com.ai.service;

import com.ai.entity.SessionRecordDrawModel;
import net.srt.framework.mybatis.service.BaseService;

import java.util.List;

/**
 * 绘图会话详情 业务接口
 */
public interface SessionRecordDrawService extends BaseService<SessionRecordDrawModel> {

    /**
     * 根据会话主键获取列表
     *
     * @param sessionDrawInfoId
     * @return
     */
    List<SessionRecordDrawModel> cacheGetBySessionInfoId(Long sessionDrawInfoId);

}
