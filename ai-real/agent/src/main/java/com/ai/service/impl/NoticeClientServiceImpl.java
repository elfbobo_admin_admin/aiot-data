package com.ai.service.impl;

import com.ai.entity.NoticeClientModel;
import com.ai.dao.NoticeClientMapper;
import com.ai.service.NoticeClientService;

import net.srt.framework.mybatis.service.impl.BaseServiceImpl;

import org.springframework.stereotype.Service;

/**
 * NoticeClient 业务实现类
 */
@Service
public class NoticeClientServiceImpl extends BaseServiceImpl<NoticeClientMapper, NoticeClientModel> implements NoticeClientService {

}
