package com.ai.service.impl;

import com.ai.common.constants.RedisConstant;
import com.ai.convert.SessionInfoDrawConvert;
import com.ai.entity.SessionInfoDrawModel;
import com.ai.entity.SessionRecordDrawModel;
import com.ai.dao.SessionInfoDrawMapper;
import com.ai.service.SessionInfoDrawService;
import com.ai.service.SessionRecordDrawService;
import com.ai.dto.SessionInfoDrawDTO;
import com.ai.common.function.OR;

import com.ai.utils.ModelUtil;
import net.srt.framework.mybatis.service.impl.BaseServiceImpl;
import com.ai.common.utils.RedisUtil;

import net.srt.framework.security.user.SecurityUser;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * 会话表 业务实现类
 */
@Service
public class SessionInfoDrawServiceImpl extends BaseServiceImpl<SessionInfoDrawMapper, SessionInfoDrawModel> implements SessionInfoDrawService {

    @Resource
    private SessionRecordDrawService sessionRecordDrawService;
    @Resource
    private RedisUtil redisUtil;

    /**
     * 获取最新的会话详情
     *
     * @param drawUniqueKey
     * @return
     */
    @Override
    public SessionInfoDrawDTO getLastSession(String drawUniqueKey) {
        Long userId = SecurityUser.getUserId();

        SessionInfoDrawModel lastSession = super.lambdaQuery()
                .eq(SessionInfoDrawModel::getUserId, userId)
                .eq(SessionInfoDrawModel::getDrawUniqueKey, drawUniqueKey)
                .orderByDesc(SessionInfoDrawModel::getCreateTime)
                .last("limit 1").one();

        SessionInfoDrawDTO dto = SessionInfoDrawConvert.INSTANCE.convertToDTO(lastSession);

        OR.run(dto, Objects::nonNull, () -> {
            Long sessionDrawId = dto.getId();
            dto.setRecordList(
                    sessionRecordDrawService.lambdaQuery().eq(SessionRecordDrawModel::getSessionInfoDrawId, sessionDrawId).list()
            );
        });

        return dto;
    }

    /**
     * 根据主键获取一行 缓存
     *
     * @param id
     * @return
     */
    @Override
    public SessionInfoDrawModel cacheGetById(Long id) {
        if (Objects.isNull(id)){
            return null;
        }
        final String KEY = ModelUtil.modelMainKey(SessionInfoDrawModel.class) + RedisConstant.Keyword.ID + ":" + id;

        SessionInfoDrawModel obj = redisUtil.getCacheObject(KEY);
        if (Objects.isNull(obj)){
            SessionInfoDrawModel one = this.getById(id);
            if (Objects.nonNull(one)){
                redisUtil.setCacheObject(KEY,one,30L, TimeUnit.MINUTES);
                obj = one;
            }
        }
        return obj;
    }
}
