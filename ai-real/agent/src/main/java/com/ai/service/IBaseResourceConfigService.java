package com.ai.service;

import com.ai.common.webApi.baseResource.BaseResourceWebApi;
import com.ai.entity.BaseResourceConfigEntity;
import com.ai.vo.ResourceChatConfigVO;
import com.ai.vo.ResourceDrawVO;
import com.ai.vo.ResourceMainVO;
import com.ai.vo.ResourceSmsConfigVO;
import com.ai.common.constants.ResourceConfigConstant;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 系统参数配置表 业务接口
 */
public interface IBaseResourceConfigService extends IService<BaseResourceConfigEntity>, BaseResourceWebApi {

    /**
     * 根据configKey查询单个信息
     *
     * @param configKey
     * @return
     */
    BaseResourceConfigEntity queryByConfigKey(String configKey);

    /**
     * 根据configKey获取configValue
     *
     * @param configKey
     * @return
     */
    String queryValueByConfigKey(String configKey);

    /**
     * 根据configKey获取configValue
     *
     * @param configKey
     * @param tClass
     * @param cache     是否使用缓存
     * @param <T>
     * @return
     */
    <T> T queryValueByConfigKey(String configKey, Class<T> tClass, Boolean cache);

    default <T> T queryValueByConfigKey(String configKey, Class<T> tClass) {
        return queryValueByConfigKey(configKey, tClass, true);
    }

    /**
     * ResourceMainVO 配置
     *
     * @return
     */
    default ResourceMainVO getResourceMain() {
        return queryValueByConfigKey(ResourceConfigConstant.MAIN_KEY, ResourceMainVO.class, true);
    }

    @Override
    default ResourceChatConfigVO getResourceChatConfigVO() {
        return queryValueByConfigKey(ResourceConfigConstant.CHAT_CONFIG, ResourceChatConfigVO.class, true);
    }

    /**
     * ResourceDrawVO 配置
     *
     * @return
     */
    default ResourceDrawVO getResourceDraw() {
        return queryValueByConfigKey(ResourceConfigConstant.DRAW, ResourceDrawVO.class, true);
    }

    default ResourceSmsConfigVO getResourceSmsConfig() {
        return queryValueByConfigKey(ResourceConfigConstant.SMS_CONFIG, ResourceSmsConfigVO.class, true);
    }

    /**
     * 根据configKey编辑configValue
     *
     * @param param
     * @return
     */
    boolean editByConfigKey(BaseResourceConfigEntity param);

}
