package com.ai.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ai.entity.draw.GalleryCommonModel;
import com.ai.entity.SessionRecordDrawModel;
import net.srt.framework.mybatis.service.BaseService;

public interface GalleryCommonService extends BaseService<GalleryCommonModel> {


    /**
     * 公开画廊绘图记录查询
     *
     * @param param
     * @return
     */
    IPage<SessionRecordDrawModel> queryCommonSessionRecord(SessionRecordDrawModel param);

    /**
     * 设置图片公开性
     *
     * @param param
     */
    GalleryCommonModel setDrawCommon(GalleryCommonModel param);

    /**
     * 删除图片公开性
     *
     * @param param
     */
    void removeDrawCommon(GalleryCommonModel param);

}
