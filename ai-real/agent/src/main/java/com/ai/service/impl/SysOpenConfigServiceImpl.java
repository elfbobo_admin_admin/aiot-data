package com.ai.service.impl;

import com.ai.dao.SysOpenConfigMapper;
import com.ai.service.ISysOpenConfigService;
import org.springframework.stereotype.Service;
import com.ai.entity.SysOpenConfigModel;
import net.srt.framework.mybatis.service.impl.BaseServiceImpl;

/**
 * 第三方配置 业务实现类
 */
@Service
public class SysOpenConfigServiceImpl extends BaseServiceImpl<SysOpenConfigMapper, SysOpenConfigModel> implements ISysOpenConfigService {

}
