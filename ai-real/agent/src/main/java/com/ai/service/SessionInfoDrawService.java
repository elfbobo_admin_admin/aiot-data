package com.ai.service;


import com.ai.dto.SessionInfoDrawDTO;
import com.ai.entity.SessionInfoDrawModel;
import net.srt.framework.mybatis.service.BaseService;

/**
 * 绘图会话 业务接口
 */
public interface SessionInfoDrawService extends BaseService<SessionInfoDrawModel> {

    /**
     * 获取最新的会话详情
     *
     * @param drawUniqueKey
     * @return
     */
    SessionInfoDrawDTO getLastSession(String drawUniqueKey);

    /**
     * 根据主键获取一行 缓存
     *
     * @param id
     * @return
     */
    SessionInfoDrawModel cacheGetById(Long id);

}
