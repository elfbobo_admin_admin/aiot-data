package com.ai.service;

import com.ai.entity.SysFileConfigModel;
import net.srt.framework.mybatis.service.BaseService;

import com.ai.utils.OptionalUtil;

import java.util.List;

/**
 * 文件上传配置 业务接口
 */
public interface SysFileConfigService extends BaseService<SysFileConfigModel> {

    /**
     * 获取当前文件配置路径
     *
     * @return
     */
    String getFileConfigPath();

    /**
     * 获取所有配置
     *
     * @return
     */
    List<SysFileConfigModel> getAll();

    default SysFileConfigModel getByUniqueKye(String uniqueKey) {
        List<SysFileConfigModel> all = OptionalUtil.ofNullList(this.getAll());
        return all.stream().filter(item -> item.getUniqueKey().equals(uniqueKey)).findFirst().orElse(null);
    }

}
