package com.ai.service;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.ai.entity.draw.TaskDrawModel;
import com.ai.core.drawTask.enums.DrawType;
import net.srt.framework.mybatis.service.BaseService;

import java.util.HashMap;
import java.util.function.Consumer;


public interface TaskDrawService extends BaseService<TaskDrawModel> {

    /**
     * 创建任务
     *
     * @param apiKey   {@link DrawType.ApiKey}
     * @param paramMap
     */
    void createTask(String apiKey, Long originalTaskDrawId, HashMap<String, Object> paramMap);

    default void createTask(String apiKey, HashMap<String, Object> paramMap) {
        createTask(apiKey, null, paramMap);
    }

    /**
     * 任务失败处理
     *
     * @param id
     * @param paramConsumer
     */
    void failTask(String id, Consumer<LambdaUpdateWrapper<TaskDrawModel>> paramConsumer);

}
