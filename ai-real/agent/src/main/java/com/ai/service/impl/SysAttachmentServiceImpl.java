package com.ai.service.impl;

import com.ai.dao.SysAttachmentMapper;
import com.ai.entity.SysAttachmentModel;
import com.ai.service.ISysAttachmentService;
import net.srt.framework.mybatis.service.impl.BaseServiceImpl;

import org.springframework.stereotype.Service;

@Service
public class SysAttachmentServiceImpl extends BaseServiceImpl<SysAttachmentMapper, SysAttachmentModel> implements ISysAttachmentService {
}
