package com.ai.service;

import com.ai.common.webApi.mjParam.MjParamWebApi;
import com.ai.entity.CmjParamModel;
import net.srt.framework.mybatis.service.BaseService;

/**
 * mj参数 业务接口
 */
public interface ICmjParamService extends BaseService<CmjParamModel>, MjParamWebApi {

}
