package com.ai.service;

import cn.hutool.core.util.StrUtil;
import com.ai.entity.ChatKeysModel;
import com.ai.common.webApi.configChat.ChatKeysWebApi;
import net.srt.framework.mybatis.service.BaseService;
import com.ai.utils.OptionalUtil;

import java.util.List;

/**
 * 对话秘钥池 业务接口
 */
public interface IChatKeysService extends BaseService<ChatKeysModel>, ChatKeysWebApi {

    @Override
    default ChatKeysModel queryByApiToken(String apiToken) {
        if (StrUtil.isEmpty(apiToken))
            return null;
        return OptionalUtil.ofNullList(
                this.lambdaQuery().eq(ChatKeysModel::getApiToken, apiToken).list()
        ).stream().findFirst().orElse(null);
    }

    List<ChatKeysModel> getAbleByChatSdkId(Long chatSdkId);

    void removeByApiToken(String token);

    void flushCache();

}
