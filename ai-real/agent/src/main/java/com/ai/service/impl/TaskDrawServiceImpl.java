package com.ai.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.extra.spring.SpringUtil;
import com.ai.utils.DateUtils;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ai.common.enums.UserTypeEnum;
import com.ai.core.drawTask.enums.DrawType;
import com.ai.common.enums.flow.TaskStatus;
import com.ai.common.webApi.baseResource.BaseResourceWebApi;
import com.ai.core.drawTask.manager.DrawTaskDataManager;
import com.ai.entity.draw.valid.CreateTask;
import com.ai.core.midjourney.manager.TaskQueueManager;
import com.ai.core.midjourney.service.MidjourneyTaskEventListener;

import com.ai.entity.draw.TaskDrawModel;
import com.ai.dao.TaskDrawMapper;
import com.ai.service.TaskDrawService;
import com.ai.entity.SessionInfoDrawModel;
import com.ai.entity.SessionRecordDrawModel;
import com.ai.dao.SessionInfoDrawMapper;
import com.ai.dao.SessionRecordDrawMapper;
import com.ai.vo.ResourceDrawVO;
import com.ai.common.function.OR;
import lombok.extern.slf4j.Slf4j;
import net.srt.framework.common.utils.SpringUtils;

import com.ai.common.exception.BusinessException;
import net.srt.framework.mybatis.service.impl.BaseServiceImpl;
import com.ai.utils.ValidatorUtil;

import net.srt.framework.security.user.SecurityUser;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.function.Consumer;


@Service
@Slf4j
public class TaskDrawServiceImpl extends BaseServiceImpl<TaskDrawMapper, TaskDrawModel> implements TaskDrawService {

    @Resource
    private RabbitTemplate rabbitTemplate;
    @Resource
    private SessionInfoDrawMapper drawMapper;
    @Resource
    private SessionRecordDrawMapper recordDrawMapper;

    /**
     * 创建任务
     *
     * @param apiKey
     * @param paramMap
     */
    @Transactional
    @Override
    public void createTask(String apiKey, Long originalTaskDrawId, HashMap<String, Object> paramMap) {
        Long userId = SecurityUser.getUserId();

        DrawType.ApiKey apiKeyValue = DrawType.ApiKey.getByName(apiKey);
        DrawType drawType = DrawType.getByApiKey(apiKeyValue);
        if (drawType.equals(DrawType.OPENAI)) {
            ResourceDrawVO resourceDraw = SpringUtils.getBean(BaseResourceWebApi.class).getResourceDraw();
            Assert.isFalse(!resourceDraw.getOpenDrawOpenai(), () -> new BusinessException("openai绘图功能暂时关闭了"));
        }

        // 校验
        Object requestParamObject = JSON.parseObject(JSON.toJSONString(paramMap), apiKeyValue.getMappingCls());
        ValidatorUtil.validateEntity(requestParamObject, CreateTask.class);

        Assert.isFalse(
                super.lambdaQuery().eq(TaskDrawModel::getUserId, userId)
                        .eq(TaskDrawModel::getDrawType, drawType.getKey())
                        .in(TaskDrawModel::getTaskStatus, CollUtil.newArrayList(TaskStatus.WAIT, TaskStatus.RUNNING))
                        .count() > 0,
                () -> new BusinessException("有任务正在进行中~请等待任务执行完成")
        );

        TaskDrawModel taskDrawModel = TaskDrawModel.builder()
                .userId(userId)
                .drawType(drawType.getKey())
                .drawApiKey(apiKeyValue.name())
                .taskStatus(TaskStatus.WAIT.getKey())
                .originalTaskDrawId(originalTaskDrawId)
                .requestParam(JSON.toJSONString(paramMap))
                .build();

        super.save(taskDrawModel);

        log.info("绘图任务创建 - 消息发送完成，channel: {},param:{}", drawType.queueKey(), JSON.toJSONString(taskDrawModel));
        rabbitTemplate.convertAndSend(drawType.queueKey(), taskDrawModel);
    }

    @Transactional
    @Override
    public void failTask(String id, Consumer<LambdaUpdateWrapper<TaskDrawModel>> paramConsumer) {
        Assert.notNull(id);
        LambdaUpdateWrapper<TaskDrawModel> wrapper = Wrappers.lambdaUpdate();
        paramConsumer.accept(wrapper);
        wrapper.eq(TaskDrawModel::getId, id);
        wrapper.set(TaskDrawModel::getTaskStatus, TaskStatus.FAIL.getKey())
                .set(TaskDrawModel::getTaskEndTime, DateUtils.nowDateFormat());
        super.update(wrapper);
    }

    @Override
    public boolean removeByIds(Collection<?> list) {
        List<TaskDrawModel> taskDrawModelList = lambdaQuery().in(TaskDrawModel::getId, list).list();
//        UserTypeEnum userType = SecurityContextUtil.getUserType();
//        List<Long> ids = taskDrawModelList.stream().peek(item -> Assert.isFalse(
//                TaskStatus.SUCCESS.getKey().equals(item.getTaskStatus()) && !userType.equals(UserTypeEnum.SYS),
//                () -> new BusinessException("已完成的任务无法删除")
//        )).map(TaskDrawModel::getId).distinct().toList();
        List<Long> ids = taskDrawModelList.stream().peek(item -> Assert.isFalse(
                TaskStatus.SUCCESS.getKey().equals(item.getTaskStatus()),
                () -> new BusinessException("已完成的任务无法删除")
        )).map(TaskDrawModel::getId).distinct().toList();

        Long userId = SecurityUser.getUserId();
        // 删除子表
//        drawMapper.delete(Wrappers.<SessionInfoDrawModel>lambdaQuery().in(SessionInfoDrawModel::getTaskId, ids).eq(!userType.equals(UserTypeEnum.SYS), SessionInfoDrawModel::getUserId, userId));
//        recordDrawMapper.delete(Wrappers.<SessionRecordDrawModel>lambdaQuery().in(SessionRecordDrawModel::getTaskId, ids).eq(!userType.equals(UserTypeEnum.SYS), SessionRecordDrawModel::getUserId, userId));
        drawMapper.delete(Wrappers.<SessionInfoDrawModel>lambdaQuery().in(SessionInfoDrawModel::getTaskId, ids).eq(SessionInfoDrawModel::getUserId, userId));
        recordDrawMapper.delete(Wrappers.<SessionRecordDrawModel>lambdaQuery().in(SessionRecordDrawModel::getTaskId, ids).eq(SessionRecordDrawModel::getUserId, userId));
        // 删除主表
        super.removeByIds(ids);
        // 删任务
        Map<String, DrawTaskDataManager> drawTaskDataManagerMap = SpringUtil.getBeansOfType(DrawTaskDataManager.class);
        drawTaskDataManagerMap.forEach((k, v) -> {
            ids.forEach(id -> {
                OR.run(TaskQueueManager.get(String.valueOf(id)), Objects::nonNull, taskObj -> {
                    SpringUtils.getBean(MidjourneyTaskEventListener.class).endTask(taskObj);
                });
                v.overQueue(String.valueOf(id));
            });
        });
        return true;
    }
}
