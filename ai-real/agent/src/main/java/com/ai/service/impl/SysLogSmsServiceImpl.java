package com.ai.service.impl;

import com.ai.dao.SysLogSmsMapper;
import com.ai.entity.SysLogSmsModel;
import com.ai.service.ISysLogSmsService;
import org.springframework.stereotype.Service;
import net.srt.framework.mybatis.service.impl.BaseServiceImpl;

/**
 * 验证码日志 业务实现类
 */
@Service
public class SysLogSmsServiceImpl extends BaseServiceImpl<SysLogSmsMapper, SysLogSmsModel> implements ISysLogSmsService {

}
