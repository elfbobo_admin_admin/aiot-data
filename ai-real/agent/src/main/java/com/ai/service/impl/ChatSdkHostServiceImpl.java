package com.ai.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.ai.common.constants.Constants;
import com.ai.entity.ChatSdkHostModel;
import com.ai.dao.ChatSdkHostMapper;
import com.ai.service.IChatSdkHostService;

import com.ai.common.utils.RedisUtil;
import com.ai.utils.ModelUtil;
import net.srt.framework.mybatis.service.impl.BaseServiceImpl;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import java.util.List;

/**
 * 对话镜像地址管理 业务实现类
 */
@Slf4j
@Service
public class ChatSdkHostServiceImpl extends BaseServiceImpl<ChatSdkHostMapper, ChatSdkHostModel> implements IChatSdkHostService {

    private final static String USABLE_KEY = ModelUtil.modelMainKey(ChatSdkHostModel.class) + "USABLE:";

    @Resource
    private RedisUtil redisUtil;

    @Override
    public List<ChatSdkHostModel> getUsableByChatSdkId(Long chatSdkId) {
        String key = USABLE_KEY + chatSdkId;
        List<ChatSdkHostModel> cacheList = redisUtil.getCacheList(key);
        if (CollUtil.isEmpty(cacheList)) {
            cacheList = super.lambdaQuery().eq(ChatSdkHostModel::getEnableStatus, Constants.EnableStatus.USABLE).eq(ChatSdkHostModel::getChatSdkId, chatSdkId).list();
            redisUtil.setCacheList(key, cacheList);
        }
        return cacheList;
    }

    @Override
    public void flushCache() {
        String key = ModelUtil.modelMainKey(ChatSdkHostModel.class) + "*";
        redisUtil.deleteLike(key);
        log.info("[清除缓存] - key: {}", key);
    }
}
