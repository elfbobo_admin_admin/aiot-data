package com.ai.service.impl;

import cn.hutool.core.lang.Assert;
import com.ai.chatsdk.common.entity.account.ChatSdkAccount;
import com.ai.chatsdk.common.entity.session.RecordData;
import com.ai.chatsdk.common.service.ChatBusinessService;
import com.ai.entity.ChatModelModel;
import com.ai.entity.ChatSdkModel;
import com.ai.common.support.strategy.StrategyServiceContext;
import com.ai.core.chat.account.service.SdkAccountBuildService;
import com.ai.core.chat.entity.ChatRequestParam;
import com.ai.core.chat.handlers.ChatRequestHandler;
import com.ai.service.ChatService;
import com.ai.service.IChatKeysService;
import com.ai.service.IChatModelService;
import com.ai.service.IChatSdkService;
import com.ai.service.SessionRecordService;
import com.ai.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 聊天会话 业务实现
 */
@Slf4j
@Service
public class ChatServiceImpl implements ChatService {

    @Resource
    private SessionRecordService sessionRecordService;
    @Resource
    private ChatRequestHandler chatRequestHandler;
    @Resource
    private IChatSdkService chatSdkService;
    @Resource
    private IChatModelService chatModelService;
    @Resource
    private IChatKeysService chatKeysService;
    @Resource
    private StrategyServiceContext<ChatBusinessService> chatBusinessContext;
    @Resource
    private StrategyServiceContext<SdkAccountBuildService> accountBuildServiceContext;

    /**
     * 发送消息
     *
     * @param param
     */
    @Transactional
    @Override
    public void sendChatMessage(ChatRequestParam param) {

        List<RecordData> recordList = chatRequestHandler.buildChatSdkParam(param);

        ChatModelModel chatModel = chatModelService.getById(param.getChatModelId());
        Assert.notNull(chatModel, () -> new BusinessException("未找到对话模型，请检查配置"));

        ChatSdkModel chatSdkModel = chatSdkService.getById(chatModel.getChatSdkId());
        Assert.notNull(chatModel, () -> new BusinessException("未找到对话SDK平台信息，请检查配置"));

        // 构建sdk账户参数
        SdkAccountBuildService accountBuildService = accountBuildServiceContext.getService(chatSdkModel.getUniqueKey(), () -> new BusinessException("对话SDK帐号构建参数失败"));
        ChatSdkAccount chatSdkAccount = accountBuildService.buildSdkAccount(chatModel, chatSdkModel, param);
        // 请求sdk对话
        ChatBusinessService service = chatBusinessContext.getService(chatSdkModel.getUniqueKey(), () -> {
            log.error("[ChatBusinessService] 获取对话模型失败");
            throw new BusinessException("系统未找到对应对话模型，请检查配置。");
        });
        service.chatCompletionStream(recordList, chatSdkAccount);
    }


}
