package com.ai.service.impl;

import com.ai.entity.CmjChannelConfigModel;
import com.ai.dao.CmjChannelConfigMapper;
import com.ai.service.ICmjChannelConfigService;

import org.springframework.stereotype.Service;

import net.srt.framework.mybatis.service.impl.BaseServiceImpl;

/**
 * mj频道配置 业务实现类
 */
@Service
public class CmjChannelConfigServiceImpl extends BaseServiceImpl<CmjChannelConfigMapper, CmjChannelConfigModel> implements ICmjChannelConfigService {

}
