package com.ai.service.impl;

import com.ai.entity.DomainModel;
import com.ai.dao.DomainMapper;
import com.ai.service.DomainService;

import net.srt.framework.mybatis.service.impl.BaseServiceImpl;

import org.springframework.stereotype.Service;


@Service
public class DomainServiceImpl extends BaseServiceImpl<DomainMapper, DomainModel> implements DomainService {

    @Override
    public DomainModel getByUniqueKey(String key) {
        DomainModel one = super.lambdaQuery().eq(DomainModel::getUniqueKey, key).one();
        return one;
    }
}
