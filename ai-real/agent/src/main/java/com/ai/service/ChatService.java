package com.ai.service;

import com.ai.core.chat.entity.ChatRequestParam;

/**
 * 聊天会话 业务接口
 */
public interface ChatService {

    /**
     * 发送消息，聊天或文本
     *
     * @param param
     */
    void sendChatMessage(ChatRequestParam param);

}
