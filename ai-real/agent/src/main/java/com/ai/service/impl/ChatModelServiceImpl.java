package com.ai.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;

import com.ai.entity.ChatModelModel;
import com.ai.dao.ChatModelMapper;
import com.ai.service.IChatModelService;

import lombok.extern.slf4j.Slf4j;

import com.ai.common.utils.RedisUtil;
import com.ai.utils.ModelUtil;
import net.srt.framework.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * 对话模型管理 业务实现类
 */
@Slf4j
@Service
public class ChatModelServiceImpl extends BaseServiceImpl<ChatModelMapper, ChatModelModel> implements IChatModelService {

    private final static String MAIN_KEY = ModelUtil.modelMainKey(ChatModelModel.class);

    @Resource
    private RedisUtil redisUtil;

    @Override
    public ChatModelModel getById(Serializable id) {
        String redisKey = MAIN_KEY + "id:" + id;
        ChatModelModel cacheObject = redisUtil.getCacheObject(redisKey);
        if (Objects.isNull(cacheObject)) {
            cacheObject = super.lambdaQuery().eq(ChatModelModel::getId, id).one();
            redisUtil.setCacheObject(redisKey, cacheObject);
        }
        return cacheObject;
    }

    @Override
    public ChatModelModel getByUniqueKey(String key) {
        if (StrUtil.isEmpty(key))
            return null;
        String redisKey = MAIN_KEY + "uniqueKey:" + key;
        ChatModelModel cacheObject = redisUtil.getCacheObject(redisKey);
        if (Objects.isNull(cacheObject)) {
            cacheObject = super.lambdaQuery().eq(ChatModelModel::getUniqueKey, key).one();
            redisUtil.setCacheObject(redisKey, cacheObject);
        }
        return cacheObject;
    }

    @Override
    public List<ChatModelModel> getAllList() {
        String redisKey = MAIN_KEY + "all:";
        List<ChatModelModel> cacheList = redisUtil.getCacheList(redisKey);
        if (CollUtil.isEmpty(cacheList)) {
            cacheList = super.list();
            redisUtil.setCacheList(redisKey, cacheList);
        }
        return cacheList;
    }

    @Override
    public void flushCache() {
        String key = ModelUtil.modelMainKey(ChatModelModel.class) + "*";
        redisUtil.deleteLike(key);
        log.info("[清除缓存] - key: {}", key);
    }
}
