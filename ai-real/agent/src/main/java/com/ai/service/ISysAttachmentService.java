package com.ai.service;


import com.ai.entity.SysAttachmentModel;
import net.srt.framework.mybatis.service.BaseService;


public interface ISysAttachmentService extends BaseService<SysAttachmentModel> {
}
