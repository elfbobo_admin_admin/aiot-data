package com.ai.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import com.ai.convert.CmjAccountConvert;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ai.common.enums.flow.AccountStatus;

import com.ai.entity.CmjAccountModel;
import com.ai.entity.CmjChannelConfigModel;
import com.ai.vo.CmjAccountDetailVO;
import com.ai.core.midjourney.client.DiscordSocketClient;
import com.ai.core.midjourney.common.entity.DiscordAccount;
import com.ai.core.midjourney.pool.DiscordAccountCacheObj;
import com.ai.core.midjourney.pool.DiscordSocketAccountPool;
import com.ai.dao.CmjAccountMapper;
import com.ai.dao.CmjChannelConfigMapper;
import com.ai.service.ICmjAccountService;
import com.ai.common.function.OR;
import com.ai.common.exception.BusinessException;

import net.srt.framework.mybatis.service.impl.BaseServiceImpl;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * mj账户配置 业务实现类
 */
@Service
public class CmjAccountServiceImpl extends BaseServiceImpl<CmjAccountMapper, CmjAccountModel> implements ICmjAccountService {

    public static List<CmjAccountDetailVO> accountDetailList;

    @Resource
    private CmjChannelConfigMapper channelConfigMapper;

    @Override
    public List<CmjAccountDetailVO> getAccountAll() {
        return accountDetailList;
    }

    @Override
    public void resetStopMjSocket() {
        OR.run(this.getAccountAll(), CollUtil::isNotEmpty, accountAll -> {
            for (CmjAccountDetailVO accountDetailVO : accountAll) {
                String userName = accountDetailVO.getUserName();

                DiscordAccountCacheObj discordAccountCacheObj = DiscordSocketAccountPool.get(userName);
                if (Objects.isNull(discordAccountCacheObj) && accountDetailVO.getAccountStatus().equals(AccountStatus.NORMAL.getKey())) {

                    DiscordAccount discordAccount = DiscordSocketClient.buildAccount(accountDetailVO);
                    DiscordSocketClient.connection(discordAccount);

                }
            }
        });
    }

    @Override
    public void flushCache() {
        List<CmjAccountModel> accountModelList = this.list();
        if (CollUtil.isEmpty(accountModelList)) {
            return;
        }
        List<CmjChannelConfigModel> channelConfigModelList = channelConfigMapper.selectList(null);
        accountDetailList = accountModelList.stream().map(accountModel -> {
            CmjAccountDetailVO vo = CmjAccountConvert.INSTANCE.convertDetailVo(accountModel);
            List<CmjChannelConfigModel> channelConfigList = channelConfigModelList.stream().filter(channelConfig -> accountModel.getId().equals(channelConfig.getCmjAccountId())).collect(Collectors.toList());
            vo.setChannelConfigList(channelConfigList);
            vo.setChannelIds(channelConfigList.stream().map(CmjChannelConfigModel::getChannelId).filter(Objects::nonNull).distinct().collect(Collectors.toList()));
            return vo;
        }).collect(Collectors.toList());
    }

    @Override
    public boolean save(CmjAccountModel entity) {
        Assert.isFalse(
                super.lambdaQuery()
                        .eq(CmjAccountModel::getUserName, entity.getUserName())
                        .eq(CmjAccountModel::getUserToken, entity.getUserToken())
                        .count() > 0,
                () -> new BusinessException("账户名称或Token重复")
        );
        entity.setAccountStatus(AccountStatus.NORMAL.getKey());
        return super.save(entity);
    }

    @Override
    public boolean updateById(CmjAccountModel entity) {
        Assert.isFalse(
                super.lambdaQuery()
                        .ne(CmjAccountModel::getId, entity.getId())
                        .eq(CmjAccountModel::getUserName, entity.getUserName())
                        .eq(CmjAccountModel::getUserToken, entity.getUserToken())
                        .count() > 0,
                () -> new BusinessException("账户名称或Token重复")
        );
        return super.updateById(entity);
    }

    @Override
    public boolean removeByIds(Collection<?> list) {
        if (super.removeByIds(list)) {
            channelConfigMapper.delete(Wrappers.<CmjChannelConfigModel>lambdaQuery().in(CmjChannelConfigModel::getCmjAccountId, list));
        }
        return true;
    }
}
