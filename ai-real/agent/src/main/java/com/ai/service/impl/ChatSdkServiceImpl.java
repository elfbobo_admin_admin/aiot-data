package com.ai.service.impl;

import cn.hutool.core.util.StrUtil;
import com.ai.entity.ChatSdkModel;
import com.ai.dao.ChatSdkMapper;
import com.ai.service.IChatSdkService;
import org.springframework.stereotype.Service;

import net.srt.framework.mybatis.service.impl.BaseServiceImpl;

/**
 * 对话第三方平台管理 业务实现类
 */
@Service
public class ChatSdkServiceImpl extends BaseServiceImpl<ChatSdkMapper, ChatSdkModel> implements IChatSdkService {

    @Override
    public ChatSdkModel getByUniqueKey(String key) {
        if (StrUtil.isEmpty(key))
            return null;
        ChatSdkModel one = super.lambdaQuery().eq(ChatSdkModel::getUniqueKey, key).one();
        return one;
    }

}
