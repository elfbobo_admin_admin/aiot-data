package com.ai.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.ai.common.constants.RedisConstant;
import com.ai.entity.SessionRecordDrawModel;
import com.ai.dao.SessionRecordDrawMapper;
import com.ai.service.SessionRecordDrawService;

import com.ai.utils.ModelUtil;
import net.srt.framework.mybatis.service.impl.BaseServiceImpl;
import com.ai.common.utils.RedisUtil;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * 会话详情 业务实现类
 */
@Service
public class SessionRecordDrawServiceImpl extends BaseServiceImpl<SessionRecordDrawMapper, SessionRecordDrawModel> implements SessionRecordDrawService {

    @Resource
    private RedisUtil redisUtil;

    /**
     * 根据会话主键获取列表
     *
     * @param sessionDrawInfoId
     * @return
     */
    @Override
    public List<SessionRecordDrawModel> cacheGetBySessionInfoId(Long sessionDrawInfoId) {
        if (Objects.isNull(sessionDrawInfoId)){
            return null;
        }
        final String KEY = ModelUtil.modelMainKey(SessionRecordDrawModel.class) + RedisConstant.Keyword.LIST_PARAM + ":" + sessionDrawInfoId;

        List<SessionRecordDrawModel> objList = redisUtil.getCacheList(KEY);
        if (CollUtil.isEmpty(objList)){
            List<SessionRecordDrawModel> modelList = this.lambdaQuery().eq(SessionRecordDrawModel::getSessionInfoDrawId, sessionDrawInfoId).list();
            if (CollUtil.isNotEmpty(modelList)){
                redisUtil.setCacheList(KEY,modelList);
                redisUtil.expire(KEY,30L, TimeUnit.MINUTES);
                objList = modelList;
            }
        }
        return objList;
    }
}
