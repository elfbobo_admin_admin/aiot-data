package com.ai.service;

import com.ai.entity.CmjAccountModel;
import com.ai.vo.CmjAccountDetailVO;
import net.srt.framework.mybatis.service.BaseService;

import java.util.List;

/**
 * mj账户配置 业务接口
 */
public interface ICmjAccountService extends BaseService<CmjAccountModel> {

    List<CmjAccountDetailVO> getAccountAll();

    /**
     * 重新连接已断开的websocket连接
     */
    void resetStopMjSocket();

    /**
     * 刷新缓存
     */
    void flushCache();
}
