package com.ai.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.ai.common.constants.Constants;
import com.ai.entity.ChatKeysModel;
import com.ai.dao.ChatKeysMapper;
import com.ai.service.IChatKeysService;
import com.ai.common.function.OR;
import net.srt.framework.mybatis.service.impl.BaseServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import com.ai.common.utils.RedisUtil;
import com.ai.utils.ModelUtil;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * 对话秘钥池 业务实现类
 */
@Slf4j
@Service
public class ChatKeysServiceImpl extends BaseServiceImpl<ChatKeysMapper, ChatKeysModel> implements IChatKeysService {

    private final static String USABLE_KEY = ModelUtil.modelMainKey(ChatKeysModel.class) + "USABLE:";

    @Resource
    private RedisUtil redisUtil;

    @Override
    public List<ChatKeysModel> getAbleByChatSdkId(Long chatSdkId) {
        String key = USABLE_KEY + chatSdkId;
        List<ChatKeysModel> cacheList = redisUtil.getCacheList(key);
        if (CollUtil.isEmpty(cacheList)) {
            cacheList = super.lambdaQuery().eq(ChatKeysModel::getChatSdkId, chatSdkId).eq(ChatKeysModel::getEnableStatus, Constants.EnableStatus.USABLE).list();
            redisUtil.setCacheList(key, cacheList);
        }
        return cacheList;
    }

    @Override
    public void removeByApiToken(String token) {
        if (StrUtil.isEmpty(token))
            return;
        OR.run(super.lambdaQuery().eq(ChatKeysModel::getApiToken, token).one(), Objects::nonNull, model -> {
            super.lambdaUpdate().set(ChatKeysModel::getRemark, "帐号异常")
                    .set(ChatKeysModel::getEnableStatus, Constants.EnableStatus.DISABLE)
                    .eq(ChatKeysModel::getId, model.getId())
                    .update();
            redisUtil.deleteObject(USABLE_KEY + model.getChatSdkId());
        });
    }

    @Override
    public void flushCache() {
        String key = ModelUtil.modelMainKey(ChatKeysModel.class) + "*";
        redisUtil.deleteLike(key);
        log.info("[清除缓存] - key: {}", key);
    }

}
