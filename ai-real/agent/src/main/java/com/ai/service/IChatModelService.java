package com.ai.service;

import com.ai.entity.ChatModelModel;
import net.srt.framework.mybatis.service.BaseService;

import java.util.List;

/**
 * 对话模型管理 业务接口
 */
public interface IChatModelService extends BaseService<ChatModelModel> {

    ChatModelModel getByUniqueKey(String key);

    List<ChatModelModel> getAllList();

    void flushCache();

}
