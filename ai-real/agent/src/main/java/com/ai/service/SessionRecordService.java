package com.ai.service;

import cn.hutool.core.collection.CollUtil;
import com.ai.chatsdk.common.entity.ChatSdkStorageResponse;
import com.ai.chatsdk.common.entity.session.RecordData;
import com.ai.entity.SessionRecordModel;
import com.ai.core.chat.entity.ChatRequestParam;
import com.ai.common.exception.BusinessException;
import net.srt.framework.mybatis.service.BaseService;

import java.util.Collection;
import java.util.List;

/**
 * 会话详情 业务接口
 */
public interface SessionRecordService extends BaseService<SessionRecordModel> {

    String CACHE_KEY_PREFIX = "SESSION_RECORD:";

    /**
     * 聊天响应数据持久化操作
     *
     * @param contextParam
     * @param response
     */
    List<SessionRecordModel> responseInsertHandle(List<RecordData> requestRecordList, ChatRequestParam contextParam, ChatSdkStorageResponse response);

    /**
     * 获取列表根据sessionId
     *
     * @param sessionId
     * @return
     */
    List<SessionRecordModel> cacheGetListBySessionId(Long sessionId);

    /**
     * 添加记录
     *
     * @param param
     */
    default void cachePushRecord(SessionRecordModel param, Long userId) {
        this.cachePushRecord(CollUtil.newArrayList(param), userId);
    }

    /**
     * 添加记录
     *
     * @param param
     */
    void cachePushRecord(List<SessionRecordModel> param, Long userId);

    /**
     * 删除缓存
     *
     * @param sessionIds
     */
    void cacheDeleteRecord(List<Long> sessionIds);

    /**
     * 刷新缓存
     *
     * @param sessionIds
     */
    void cacheFlushRecord(List<Long> sessionIds);

    /**
     * @deprecated
     */
    @Override
    default boolean save(SessionRecordModel entity) {
        throw new BusinessException("无效的方法");
    }

    /**
     * @deprecated
     */
    @Override
    default boolean saveBatch(Collection<SessionRecordModel> entityList) {
        throw new BusinessException("无效的方法");
    }
}
