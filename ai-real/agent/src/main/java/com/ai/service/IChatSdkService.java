package com.ai.service;

import net.srt.framework.mybatis.service.BaseService;
import com.ai.entity.ChatSdkModel;

/**
 * 对话第三方平台管理 业务接口
 */
public interface IChatSdkService extends BaseService<ChatSdkModel> {

    ChatSdkModel getByUniqueKey(String key);

}
