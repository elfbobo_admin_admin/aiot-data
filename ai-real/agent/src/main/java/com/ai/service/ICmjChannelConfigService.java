package com.ai.service;

import com.ai.entity.CmjChannelConfigModel;
import net.srt.framework.mybatis.service.BaseService;

/**
 * mj频道配置 业务接口
 */
public interface ICmjChannelConfigService extends BaseService<CmjChannelConfigModel> {

}
