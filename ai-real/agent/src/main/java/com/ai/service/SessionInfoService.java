package com.ai.service;

import com.ai.entity.SessionInfoModel;
import net.srt.framework.mybatis.service.BaseService;

/**
 * 会话表 业务接口
 */
public interface SessionInfoService extends BaseService<SessionInfoModel> {

    /**
     * 新增领域会话
     *
     * @param sessionType
     */
    SessionInfoModel addSession(String sessionType, String domainUniqueKey, String content);

    /**
     * 获取用户最新会话
     *
     * @param sessionType
     * @return
     */
    SessionInfoModel userLastSession(String sessionType);

    /**
     * 获取用户最新会话（领域会话）
     *
     * @param sessionType
     * @param domainUniqueKey
     * @return
     */
    SessionInfoModel userLastDomainSession(String sessionType, String domainUniqueKey);

    /**
     * 清空会话列表
     *
     * @param sessionId
     */
    void clearSession(Long sessionId);

}
