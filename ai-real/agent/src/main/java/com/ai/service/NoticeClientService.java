package com.ai.service;

import com.ai.entity.NoticeClientModel;
import net.srt.framework.mybatis.service.BaseService;

/**
 * NoticeClient 业务接口
 */
public interface NoticeClientService extends BaseService<NoticeClientModel> {

}
