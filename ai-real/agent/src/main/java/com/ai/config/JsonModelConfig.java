package com.ai.config;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.springframework.context.annotation.Configuration;
@Configuration
public class JsonModelConfig extends SimpleModule {

    /**
     * 将响应数据的Long类型转换为String
     */
    public JsonModelConfig() {
        this.addSerializer(Long.class, ToStringSerializer.instance);
        this.addSerializer(Long.TYPE, ToStringSerializer.instance);
    }

}
