package com.ai.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.ai.entity.SessionRecordDrawModel;

/**
 * 图像会话详情 Model
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SessionRecordDrawDTO extends SessionRecordDrawModel {

    private String userName;

}
