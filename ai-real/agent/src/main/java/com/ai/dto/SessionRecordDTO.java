package com.ai.dto;

import com.ai.entity.SessionRecordModel;
import lombok.*;

/**
 * 会话详情 Dto
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SessionRecordDTO extends SessionRecordModel {

}
