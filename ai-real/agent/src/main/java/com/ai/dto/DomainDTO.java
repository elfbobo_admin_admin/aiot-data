package com.ai.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.ai.entity.DomainModel;

@Data
@EqualsAndHashCode(callSuper = true)
public class DomainDTO extends DomainModel {


}
