package com.ai.dto;


import com.ai.entity.CmjAccountModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * mj账户配置 Dto
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CmjAccountDTO extends CmjAccountModel {

    /**
     * socket连接状态
     * 1 已连接 0 未连接
     */
    private Integer socketStatus;

}
