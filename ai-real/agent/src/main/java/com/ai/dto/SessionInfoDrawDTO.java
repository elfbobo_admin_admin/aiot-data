package com.ai.dto;

import com.ai.entity.SessionInfoDrawModel;
import com.ai.entity.SessionRecordDrawModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class SessionInfoDrawDTO extends SessionInfoDrawModel {

    List<SessionRecordDrawModel> recordList;

}
