package com.ai.dto;

import com.ai.entity.draw.TaskDrawModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class TaskDrawDTO extends TaskDrawModel {

}
