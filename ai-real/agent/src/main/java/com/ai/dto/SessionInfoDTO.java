package com.ai.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.ai.entity.SessionInfoModel;
import com.ai.entity.SessionRecordModel;

import java.util.List;

/**
 * 会话表 Dto
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SessionInfoDTO extends SessionInfoModel {

    private String userName;

    private String ifTourist;

    private String userIpAddress;

    private String email;

    private List<SessionRecordModel> recordList;

}
