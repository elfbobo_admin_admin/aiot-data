package com.ai.common.constants;

/**
 * api请求前缀常量类
 *
 */
public interface ApiPrefixConstant {

    String MODEL_API = "/module";

    interface Auth {

        String SYSTEM = "/auth/system";

    }

    interface Modules {

        String SYSTEM = MODEL_API + "/system";

        String CONFIG = MODEL_API + "/config";

        String SESSION = MODEL_API + "/session";

        String QUARTZ = MODEL_API + "/quartz";

        String USER = MODEL_API + "/user";

        String STATISTICS = MODEL_API + "/statistics";

        String BUSINESS = MODEL_API + "/business";

        String DRAW = MODEL_API + "/draw";

        String ORDER = MODEL_API + "/order";

    }

    interface Common {

        String COMMON = "/common";

    }

}
