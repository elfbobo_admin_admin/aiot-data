package com.ai.common.constants;

/**
 * 系统配置常量类 (变更数据库同时此配置也需要更改)
 *
 */
public interface ResourceConfigConstant {

    String MAIN_KEY = "main";

    String CHAT_CONFIG = "chatConfig";

    String DRAW = "draw";

    String SMS_CONFIG = "smsConfig";

}
