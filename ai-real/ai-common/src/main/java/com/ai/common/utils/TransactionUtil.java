//package com.ai.common.utils;
//
//import com.ai.common.function.SingleFunction;
//import org.springframework.transaction.support.TransactionTemplate;
//
//public class TransactionUtil {
//
//    private static final TransactionTemplate transactionTemplate;
//
//    static {
//        transactionTemplate = SpringUtils.getBean(TransactionTemplate.class);
//    }
//
//    /**
//     * 事务分隔执行
//     *
//     * @param function
//     */
//    public static void execute(SingleFunction function) {
//        transactionTemplate.execute(status -> {
//            try {
//                function.run();
//            } catch (Exception e) {
//                status.setRollbackOnly();
//                throw e;
//            }
//            return true;
//        });
//    }
//}
