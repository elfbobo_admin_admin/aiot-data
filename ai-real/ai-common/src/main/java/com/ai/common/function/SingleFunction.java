package com.ai.common.function;

/**
 * 无惨无返回值的function
 *
 */
public interface SingleFunction {

    void run();

}
