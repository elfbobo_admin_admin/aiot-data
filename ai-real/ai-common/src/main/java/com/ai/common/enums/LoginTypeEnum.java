package com.ai.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LoginTypeEnum {

    // 管理端登陆
    ADMIN("1"),
    // 用户登陆
    CLIENT("2"),
    // 游客登陆
    TOURIST("3");

    private final String type;

}
