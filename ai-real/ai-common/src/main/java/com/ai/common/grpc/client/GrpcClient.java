package com.ai.common.grpc.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;


import java.util.concurrent.TimeUnit;

public class GrpcClient {
    private static ManagedChannel channel;

    public static ManagedChannel create(String host, int port) {
        channel = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext() // 使用明文
//                .useTransportSecurity() // 使用TLS
                .build();
        return channel;
    }

    public static void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

}
