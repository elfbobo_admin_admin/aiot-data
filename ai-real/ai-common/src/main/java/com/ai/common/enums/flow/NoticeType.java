package com.ai.common.enums.flow;

import com.ai.common.enums.FlowBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
@Getter
@AllArgsConstructor
public enum NoticeType implements FlowBaseEnum {

    NOTIFY("NOTIFY", "通知"),

    HELP("HELP", "常见问题/帮助"),

    ;

    private final String key;

    private final String value;
}
