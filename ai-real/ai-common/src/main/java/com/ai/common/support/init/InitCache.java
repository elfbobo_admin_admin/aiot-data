package com.ai.common.support.init;

/**
 * 初始化缓存接口
 * <p>实现该接口会在启动时调用</p>
 */
public interface InitCache {

    /**
     * 初始化缓存
     */
    void runInitCache();

}
