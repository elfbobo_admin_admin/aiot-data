package com.ai.common.enums;

import lombok.Getter;

@Getter
public enum ChannelEnum {

    log_login,

    log_handle,

    mail_send

}
