package com.ai.common.enums;

import java.util.Locale;

/**
 * 流式响应风格枚举
 */
public enum StreamResponseTypeEnum {

    Websocket,

    SSE;

    /**
     * 根据value获取默认的流式响应模式类型
     *
     * @param value
     * @return
     */
    public static StreamResponseTypeEnum getDefaultType(String value) {

        for (StreamResponseTypeEnum streamResponseTypeEnum : StreamResponseTypeEnum.values()) {
            String name = streamResponseTypeEnum.name().toLowerCase(Locale.ROOT);
            if (name.equals(value.toLowerCase(Locale.ROOT))) {
                return streamResponseTypeEnum;
            }
        }
        return SSE;
    }

}
