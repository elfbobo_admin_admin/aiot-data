package com.ai.common.enums.flow;

import com.ai.common.enums.FlowBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 会话状态
 */
@Getter
@AllArgsConstructor
public enum SessionStatus implements FlowBaseEnum {

    PROGRESS("PROGRESS", "进行中"),

    END("END", "结束"),

    ;

    private final String key;

    private final String value;
}
