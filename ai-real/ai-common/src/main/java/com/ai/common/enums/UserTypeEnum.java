package com.ai.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserTypeEnum implements FlowBaseEnum {

    SYS("0", "系统用户"),

    USER("1", "普通用户"),

    ;

    private final String key;

    private final String value;

}
