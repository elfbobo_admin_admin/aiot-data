package com.ai.common.enums;

/**
 * 流程枚举接口
 */
public interface FlowBaseEnum {

    String getKey();

    String getValue();

}
