package com.ai.common.enums.flow;

import com.ai.common.enums.FlowBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ChatSdkType implements FlowBaseEnum {

    openai("openai", "openai模型"),

    spark("spark", "讯飞星火平台"),

    baidu("baidu", "百度千帆大模型"),
    ;

    private final String key;

    private final String value;
}
