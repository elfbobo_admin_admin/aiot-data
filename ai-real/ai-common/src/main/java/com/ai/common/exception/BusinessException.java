package com.ai.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private String message;

    private int code;

    public BusinessException() {
    }

    public BusinessException(String message) {
        this.message = message;
        setMessage(message);
    }

    public BusinessException(String message,int code) {
        this.message = message;
        this.code = code;
        setMessage(message);
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public <T extends RuntimeException> T error(String message){
        this.message = message;
        return (T) this;
    }
}
