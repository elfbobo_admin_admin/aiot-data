package com.ai.common.enums.flow;

import com.ai.common.enums.FlowBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccountStatus implements FlowBaseEnum {

    NORMAL("NORMAL", "正常"),
    FREEZE("FREEZE", "冻结"),
    ;

    private final String key;

    private final String value;

}
