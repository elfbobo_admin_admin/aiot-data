package com.ai.common.constants;

/**
 * 通用常量
 *
 */
public interface Constants {

    String MAIN_ID = "id";

    String DEFAULT_IN_PARAM = "-1";

    /**
     * 默认父节点id
     */
    Long DEFAULT_PARENT_ID = 0L;

    /**
     * UTF-8 字符集
     */
    String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    String GBK = "GBK";

    /**
     * http请求
     */
    String HTTP = "http://";

    /**
     * https请求
     */
    String HTTPS = "https://";

    String BASE_RANDOM_CHAR = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    String BASE_RANDOM_CHAR_LC = "abcdefghijklmnopqrstuvwxyz0123456789";

    String BASE_RANDOM_CHAR_GC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    interface DelFlag {
        /**
         * 未删除
         */
        String NORMAL = "0";
        /**
         * 已删除
         */
        String DEL = "1";
    }

    interface Disable {
        /**
         * 正常
         */
        String NORMAL = "0";
        /**
         * 停用
         */
        String DISABLE = "1";
    }

    interface Status {
        /**
         * 正常
         */
        String NORMAL = "0";
        /**
         * 不正常
         */
        String NO_NORMAL = "1";
    }

    interface BOOLEAN {
        /**
         * 否
         */
        String FALSE = "0";
        /**
         * 是
         */
        String TRUE = "1";
    }

    interface OPEN_STATUS {
        /**
         * 关闭
         */
        String CLOSE = "0";
        /**
         * 开启
         */
        String OPEN = "1";
    }

    interface VISIBLE {
        /**
         * 显示
         */
        String SHOW = "0";
        /**
         * 隐藏
         */
        String HIDE = "1";
    }

    /**
     * 可使用状态
     */
    interface EnableStatus {
        /**
         * 可用
         */
        String USABLE = "0";
        /**
         * 停用
         */
        String DISABLE = "1";
    }

    /**
     * 菜单类型（M目录 C菜单 F按钮）
     */
    interface MENU_TYPE {
        /**
         * 目录
         */
        String M = "M";
        /**
         * 菜单
         */
        String C = "C";
    }

    interface MENU_INFO {
        /**
         * 是否菜单外链（否）
         */
        String NO_FRAME = "1";

        /**
         * Layout组件标识
         */
        String LAYOUT = "Layout";

        /**
         * InnerLink组件标识
         */
        String INNER_LINK = "InnerLink";

        /**
         * ParentView组件标识
         */
        String PARENT_VIEW = "ParentView";
    }

}
