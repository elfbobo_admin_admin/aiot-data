package com.ai.common.websocket.constants;

public interface EndpointConstant {

    String chat = "/socket/chat";

    String user = "/socket/user/{userId}";

}
