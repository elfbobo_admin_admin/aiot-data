package com.ai.common.grpc.server;

import io.grpc.BindableService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;

import java.io.IOException;
import java.util.List;

/**
 * 明文传输，如果不想使用明文传输，可以使用tls进行加密
 */
public class GrpcServer {

    private static Server server;
    private static ServerBuilder serverBuilder;

    /**
     * 注意，gRPC不支持动态添加 service，每次新的 service添加需要重启
     * @param port
     * @throws IOException
     */
    public static void create(int port) throws IOException {
        serverBuilder = ServerBuilder.forPort(port);
    }

    /**
     * 注意，gRPC不支持动态添加 service，每次新的 service添加需要重启。
     * 系统必须在启动的时候把所有的 service 都添加进来。
     * @param service
     * @throws IOException
     */
    public static void addService(BindableService service) throws IOException {
        serverBuilder.addService(service);
    }

    public static void start() throws IOException {
        server = serverBuilder.build()
                .start();

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                GrpcServer.stop();
                System.err.println("*** gRPC server shut down");
            }
        });
    }

    private static void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    /**
     * 在主线程上等待终止，因为grpc库使用守护线程。
     * 在调用了start 方法后调用。
     * @throws InterruptedException
     */
    public static void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

}
