package com.ai.common.enums;

public enum MjParamUnique {

    blend,

    describe,

    imagine,

    message,

    remix,

    reroll,

    upscale,

    variation,

}
