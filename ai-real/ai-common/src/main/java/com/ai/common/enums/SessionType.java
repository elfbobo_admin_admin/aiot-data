package com.ai.common.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

public enum SessionType {

    chat,

    domain,

    custom;

    public static SessionType get(String name) {

        if (StringUtils.isEmpty(name)) {
            return null;
        }
        for (SessionType value : values()) {
            if (value.name().toUpperCase(Locale.ROOT).equals(name.toUpperCase())) {
                return value;
            }
        }
        return null;
    }

}
