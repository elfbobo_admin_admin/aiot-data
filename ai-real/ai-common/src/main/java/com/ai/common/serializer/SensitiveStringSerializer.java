package com.ai.common.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.util.StringUtils;

import java.io.IOException;

/**
 * 字符串加敏
 */
public class SensitiveStringSerializer extends JsonSerializer<String> {

    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
//        try {
//            if (SecurityConstant.SYSTEM_ID.equals(SecurityBaseUtil.getUserId())) {
//                gen.writeString(value);
//                return;
//            }
//        } catch (Exception ignored) {
//        }
        if (StringUtils.hasText(value)) {
            int length = value.length();
            int maskLength = length / 2;
            int start = (length - maskLength) / 2;
            int end = start + maskLength;
//            String maskedValue = value.substring(0, start) + "*".repeat(maskLength) + value.substring(end);
            String maskedValue = value.substring(0, start) + sensitive(maskLength) + value.substring(end);
            gen.writeString(maskedValue);
        }
    }

    private String sensitive(int maskLength) {
        String result = "";
        for(int i = 0; i < maskLength; i++) {
            result += "*";
        }
        return result;
    }
}

