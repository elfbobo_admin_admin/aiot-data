package com.ai.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OpenConfigUnique {

    qiniu("qiniu");

    private final String uniqueKey;

}
