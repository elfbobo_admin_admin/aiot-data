package com.ai.common.enums.permission;

import com.ai.common.enums.FlowBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RoleDefaultKeyEnum implements FlowBaseEnum {

    system("system","系统管理员"),

    admin("admin","系统人员"),

    tourist("tourist","游客"),
    ;

    private final String key;

    private final String value;
}
