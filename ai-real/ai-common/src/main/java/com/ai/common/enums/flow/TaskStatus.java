package com.ai.common.enums.flow;

import com.ai.common.enums.FlowBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
@Getter
@AllArgsConstructor
public enum TaskStatus implements FlowBaseEnum {

    WAIT("WAIT", "待执行"),
    RUNNING("RUNNING", "进行中"),
    SUCCESS("SUCCESS", "已完成"),
    FAIL("FAIL", "失败"),
    ;

    private final String key;

    private final String value;

}
