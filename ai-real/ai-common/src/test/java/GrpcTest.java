import com.ai.common.grpc.client.GrpcClient;
import com.ai.common.grpc.server.GrpcServer;
import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import com.ai.grpc.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.atomic.AtomicInteger;

public class GrpcTest {

    static class GreeterImpl extends GreeterGrpc.GreeterImplBase {
        /**
         * 原子Integer
         */
        public AtomicInteger count = new AtomicInteger(0);

        @Override
        public void sayHello(HelloRequest req, StreamObserver<HelloReply> responseObserver) {
            System.out.println("====call server sayHello====");
            HelloReply reply = HelloReply.newBuilder().setMessage("Hello " + req.getName() + " age=" + req.getAge()).build();

            responseObserver.onNext(reply);
            responseObserver.onCompleted();

            System.out.println("server Atomic ==== " + count.incrementAndGet() + Thread.currentThread().getName());
        }
    }

    public void startServer() {
        try {
            GrpcServer.create(5000);
            GrpcServer.addService(new GreeterImpl()); // 如果有多种消息使用grpc的多路复用在这里添加
//            GrpcServer.addService(new GreeterImpl());
            GrpcServer.start();
            GrpcServer.blockUntilShutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void testClient() {
        try {
            ManagedChannel channel = GrpcClient.create("127.0.0.1", 5000);
            GreeterGrpc.GreeterBlockingStub blockingStub = GreeterGrpc.newBlockingStub(channel);

            HelloRequest request = HelloRequest.newBuilder().setName(" world").setAge(111).build();
            HelloReply response = blockingStub.sayHello(request);

            System.out.println("Client Greeting: " + response.getMessage());

            GrpcClient.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        GrpcTest test = new GrpcTest();
        new Thread(() -> {

                test.startServer();

        }).start();

        test.testClient();

//        System.exit(0); // 正常退出
//        System.exit(1); // 异常退出
    }
}
