package cc.iotkit.data.service;

import cc.iotkit.data.model.TbNotifyMessage;
import com.baomidou.mybatisplus.extension.service.IService;

public interface NotifyMessageService extends IService<TbNotifyMessage> {
}
