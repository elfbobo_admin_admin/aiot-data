package cc.iotkit.data.service;

import cc.iotkit.data.model.TbAlertConfig;
import com.baomidou.mybatisplus.extension.service.IService;

public interface AlertConfigService extends IService<TbAlertConfig> {
}
