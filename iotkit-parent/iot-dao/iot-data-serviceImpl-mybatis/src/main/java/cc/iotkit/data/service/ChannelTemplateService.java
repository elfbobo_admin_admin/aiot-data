package cc.iotkit.data.service;

import cc.iotkit.data.model.TbChannelTemplate;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ChannelTemplateService extends IService<TbChannelTemplate> {
}
