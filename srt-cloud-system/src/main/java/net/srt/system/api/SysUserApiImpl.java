package net.srt.system.api;

import lombok.AllArgsConstructor;
import net.srt.api.module.system.StorageApi;
import net.srt.api.module.system.SysUserApi;
import net.srt.api.module.system.dto.StorageDTO;
import net.srt.api.module.system.dto.SysUserDTO;
import net.srt.framework.common.utils.Result;
import net.srt.storage.service.StorageService;
import net.srt.system.convert.SysUserConvert;
import net.srt.system.entity.SysUserEntity;
import net.srt.system.service.SysUserService;
import net.srt.system.vo.SysUserVO;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@AllArgsConstructor
public class SysUserApiImpl implements SysUserApi {
    private final SysUserService sysUserService;

    @Override
    public List<SysUserDTO> listByIds(List ids) throws IOException {
        List<SysUserDTO> users = SysUserConvert.INSTANCE.convertListDto(sysUserService.listByIds(ids));
        return users;
    }
}
