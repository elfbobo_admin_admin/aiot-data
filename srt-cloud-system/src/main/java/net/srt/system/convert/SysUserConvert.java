package net.srt.system.convert;

import net.srt.api.module.system.dto.SysUserDTO;
import net.srt.framework.security.user.UserDetail;
import net.srt.system.entity.SysUserEntity;
import net.srt.system.vo.SysUserVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;


@Mapper
public interface SysUserConvert {
    SysUserConvert INSTANCE = Mappers.getMapper(SysUserConvert.class);

    SysUserVO convert(SysUserEntity entity);

    SysUserEntity convert(SysUserVO vo);

    SysUserVO convert(UserDetail userDetail);



    UserDetail convertDetail(SysUserEntity entity);

    List<SysUserVO> convertList(List<SysUserEntity> list);

    List<SysUserDTO> convertListDto(List<SysUserEntity> list);

}
