package net.srt.api.module.system;

import net.srt.api.ServerNames;

import net.srt.api.module.system.dto.SysUserDTO;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.IOException;
import java.util.List;

@FeignClient(name = ServerNames.SYSTEM_SERVER_NAME, contextId = "sys-user-api")
public interface SysUserApi {

    @PostMapping(value = "api/sys/getUsers")
    public List<SysUserDTO> listByIds(@RequestBody List ids) throws IOException;

}
